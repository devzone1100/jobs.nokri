<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppliedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applied_users', function (Blueprint $table) {
            $table->id();
            $table->string('job_id');
            $table->string('company_id');
            $table->string('company_email');
            $table->string('job_title');
            $table->string('applied_useremail');
            $table->string('applied_userid');
            $table->string('applied_firstname');
            $table->string('applied_lastname');
            $table->string('applied_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applied_users');
    }
}
