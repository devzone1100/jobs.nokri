<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_payments', function (Blueprint $table) {
            $table->id();
            $table->string('USERID');
            $table->string('USEREMAIL');
            $table->string('ORDERID');
            $table->string('MID');
            $table->string('TXNID');
            $table->string('TXNAMOUNT');
            $table->string('PAYMENTMODE');
            $table->string('CURRENCY');
            $table->string('TXNDATE');
            $table->string('STATUS');
            $table->string('RESPCODE');
            $table->string('RESPMSG')->nullable();
            $table->string('GATEWAYNAME')->nullable();
            $table->string('BANKTXNID');
            $table->string('BANKNAME');
            $table->string('CHECKSUMHASH');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_payments');
    }
}
