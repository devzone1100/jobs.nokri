<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_models', function (Blueprint $table) {
            $table->id();
            $table->string('job_id');
            $table->string('company_email');
            $table->string('company_id');
            $table->string('Job_Category');
            $table->string('Job_Title');
            $table->string('Job_Type');
            $table->string('Working_Hours');
            $table->string('Salary');
            $table->string('Experience');
            $table->string('company_logo')->nullable();
            $table->string('job_letter')->nullable();
            $table->string('about_job');
            $table->string('trending_key');
            $table->string('country');
            $table->string('city');
            $table->string('full_address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('timezone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
