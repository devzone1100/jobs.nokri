<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackEndCode;
use App\Http\Controllers\Forgot_password;

// register***************
use App\Http\Controllers\MyController;
use App\Http\Controllers\NewViewContoller;

// job-post****************
use App\Http\Controllers\PostJobBackEnd;


// admin---------------------
use App\Http\Controllers\AdminController;
use App\Http\Controllers\paymentController;



Route::get('/', function () {
    return view('static.index');
});




// Static-page --------------------------
Route::view('home','static.index');
Route::view('signup','static.signup');
Route::view('login','static.login');
Route::view('contact','static.contact');
Route::view('service','static.service');
Route::view('aboutus','static.aboutus');
Route::view('blog-single','static.blog-single');
Route::view('blog-grid','static.blog-grid');
Route::view('paytm-merchant-form','company.paytm-merchant-form');
// Route::get('paytm-merchant-form',[paymentContoller::class,'make_payment']);


// User-Page-------------------------------------
Route::view('job-listing-grid','user.job-listing-grid');
Route::view('user-dashboard','user.user-dashboard');
// Route::get('user-dashboard',[NewViewContoller::class,'userdashboardView']);
// Route::view('edit-user-profile','user.edit-user-profile');
Route::get('edit-user-profile',[NewViewContoller::class,'edit_profile_Data']);

Route::get('user-applied-job',[NewViewContoller::class,'userAppliedJob']);



// User-Page-End-------------------------------------



// Company-Page--------------------------------------------
Route::view('company-application','company.company-application');

// Route::view('transaction-invoice','transactionInvoice');
Route::get('transaction-invoice',[NewViewContoller::class,'transactionInvoice']);

Route::get('edit-company-profile',[NewViewContoller::class,'editCompanyProfile']);

Route::get('company-page',[PostJobBackEnd::class,'companypageview']);

Route::view('company-dashboard','company.company-dashboard');
Route::view('manage-job','company.manage-job');

Route::get('manage-application',[PostJobBackEnd::class,'manageapplieduser']);
Route::get('edit_manage_application/{id}',[PostJobBackEnd::class,'editmanageapplication']);
Route::get('delete_applied_user/{id}',[PostJobBackEnd::class,'deleteDetailsAppliedUser']);
Route::post('updateAppliedUser',[PostJobBackEnd::class,'updateFormAppliedUser']);


Route::get('manage-job',[PostJobBackEnd::class,'manageJobs']);
Route::get('edit_manage_jobs/{id}',[PostJobBackEnd::class,'editDetailsManageJobs']);
Route::post('updateFormManageJobs',[PostJobBackEnd::class,'updateFormManageJobs']);
Route::get('delete_manage_jobs/{id}',[PostJobBackEnd::class,'deleteMangeJobs']);





// Company-Page-End--------------------------------------------



// Adnim-page*************************************************************************************

Route::post('Adminregisterinsret',[AdminController::class,'Adminregistersave']);

Route::Post('AdminloginCheck',[AdminController::class, 'AdminLogin_']);
Route::get('Admin-logout',[AdminController::class,'Adminlogout']);


Route::view('add-plans','Admin.add-plans');
Route::post('addplaninsert',[AdminController::class,'AdminAddPlansInsert']);
Route::get('addplan_Data',[AdminController::class,'addplanData']);
Route::get('select_plans',[AdminController::class,'SelectPlans']);

Route::get('add_cart/{id}',[AdminController::class,'addCartView']);

Route::get('applied-user-data',[AdminController::class,'AppliedUserData']);
Route::get('edit_applied_user_data/{id}',[AdminController::class,'editAppliedUserData']);
Route::get('delete_applied_user_data/{id}',[AdminController::class,'deleteDetailsAppliedUserData']);
Route::post('updateAppliedUserData',[AdminController::class,'updateFormAppliedUserData']);





Route::view('Adminsignup','Admin.Adminsignup');
Route::view('Admin-Login','Admin.Admin-Login');
Route::view('Admin-dashboard','Admin.Admin-dashboard');
Route::view('Admin-edit-profile','Admin.Admin-edit-profile');


// Adnim-page-End*************************************************************************************




Route::view('payment','payment');

Route::view('navbar','layout.navbar');
Route::view('footer','layout.footer');


// Register***********************
Route::post('registerinsret',[NewViewContoller::class,'registersave']);
Route::post('updateFormregister',[NewViewContoller::class,'updateFormregister']);


Route::get('edit_page_register/{id}',[NewViewContoller::class,'editDetailsregister']);
Route::get('delete_page_register/{id}',[NewViewContoller::class,'deleteDetailsregister']);


// *******************login,dashboard,profile *************************************************************

Route::get('Company-dashboard',[NewViewContoller::class, 'index']);
Route::get('newGetData',[NewViewContoller::class, 'newGetData']);
Route::get('registerData',[NewViewContoller::class, 'newGetData']);


Route::post('newSignUp',[NewViewContoller::class, 'viewPage']);
Route::Post('loginCheck',[NewViewContoller::class, 'Login_']);

Route::get('logout',[NewViewContoller::class,'logout']);
Route::get('verify_email/{token}',[NewViewContoller::class,'verify_email']);


// *****************Admin-middleware*************************************
Route::group(['middleware'=>'myCustomAdmin'],function(){
    
    Route::get('Admin-dashboard',[AdminController::class,'AdmindashboardView']);
    Route::get('vp',[AdminController::class,'viewProfile']);
});
// *****************Admin-middleware-End*************************************



Route::group(['middleware'=>'myCustom'],function(){

    Route::get('job-post',[NewViewContoller::class,'jobpostformView']);

    
    
    Route::get('company-dashboard',[NewViewContoller::class,'dashboardView']);
    Route::get('user-dashboard',[NewViewContoller::class,'userdashboardView']);
    Route::post('profile_image_update_User',[NewViewContoller::class,'profile_image_update_User']);
    Route::post('profile_image_update',[NewViewContoller::class,'profile_image_update']);
    Route::get('vp',[NewViewContoller::class,'viewProfile']);
});


// login,dashboard,profile End *************************************************************************




// post job***************************
Route::post('jobpostinsret',[PostJobBackEnd::class,'jobpostsave']);
Route::post('updateFormjobpost',[PostJobBackEnd::class,'updateFormjobpost']);

Route::get('jobpostData',[PostJobBackEnd::class,'jobpostFormShow']);
Route::get('edit_page_jobpost/{id}',[PostJobBackEnd::class,'editDetailsjobpost']);
Route::get('delete_page_jobpost/{id}',[PostJobBackEnd::class,'deleteDetailsjobpost']);


Route::get('job-listing-grid',[PostJobBackEnd::class,'jobpostlistShow']);

Route::get('home',[PostJobBackEnd::class,'LetestjobpostlistShow']);
Route::get('',[PostJobBackEnd::class,'LetestjobpostlistShow']);
Route::get('index',[PostJobBackEnd::class,'LetestjobpostlistShow']);

Route::get('details_job_post/{id}',[PostJobBackEnd::class,'detailsjobpost']);

// post-job-end*****************************



// applied-job-user*****************************

Route::post('applieduserinsret',[PostJobBackEnd::class,'applieduser']);




// applied-job-user*****************************



// forget-password****************

Route::view('forgetpassword','forgetpassword');
Route::get('forgetpassword/{token}',[NewViewContoller::class,'reset_password']);

Route::post('forget',[NewViewContoller::class,'forget']);
Route::post('forget_password',[NewViewContoller::class,'forget_pass']);


// payment************************

// Route::get('paymentData',[Forgot_password::class,'paymentFormShow']);
Route::get('edit_page/{id}',[BackEndCode::class,'editDetails']);
Route::get('delete_page/{id}',[BackEndCode::class,'deleteDetails']);




Route::get('paymentData',[paymentController::class,'paymentFormShow']);
Route::get('delete_payment/{id}',[paymentController::class,'deletePaymentData']);

Route::post('paymentinsret',[paymentController::class,'make_payment']);
Route::post('payment-success',[paymentController::class,'successPage']);
Route::post('updateForm',[BackEndCode::class,'updateForm']);











