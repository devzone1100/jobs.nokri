<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <!-- <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" type="text/css" href="{{asset('font/flaticon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

    <style>
        
        ul.socialIcons {
    padding: 0;
    text-align: center;
    }

    .socialIcons li {
    background-color: yellow;
    list-style: none;
    display: inline-block;
    margin: 0px;
    border-radius: 2em;
    overflow: hidden;
    }

    .socialIcons li a {
    display: block;
    padding: 0.8em;
        min-width: 2.805714em;
        max-width: 1.28571429em;
        height: 2.805714em;
        
    white-space: nowrap;
    line-height: 1.5em; /*it's working only when you write text with icon*/
    transition: 0.5s;
    text-decoration: none;
    font-family: arial;
    color: #fff;
    }

    .socialIcons li i {
    margin-right: 0.5em;
    }

    .socialIcons li:hover a {
    max-width: 200px;
    padding-right: 1em;
    }

    .socialIcons .facebook {
    background-color: #3b5998;
    box-shadow: 0 0 16px #3b5998;
    }

    .socialIcons .twitter {
    background-color: #00aced;
    box-shadow: 0 0 16px #00aced;
    }

    .socialIcons .instagram {
    background-color: #cd486b;
    box-shadow: 0 0 16px #cd486b;
    }

    .socialIcons .pinterest {
    background-color: #c92228;
    box-shadow: 0 0 16px #c92228;
    }

    .socialIcons .steam {
    background-color: #666666;
    box-shadow: 0 0 16px #666666;
    }
    </style>
</head>
<body>
    
<ul class="socialIcons">
    <li class="facebook"><a href=""><i class="fa fa-fw fa-facebook " style="padding-top:-40px !important;"></i>Facebook</a></li>
    <li class="twitter"><a href=""><i class="fa fa-fw fa-twitter"></i>Twitter</a></li>
    <li class="instagram"><a href=""><i class="fa fa-fw fa-instagram"></i>Instagram</a></li>
    <li class="pinterest"><a href=""><i class="fa fa-fw fa-pinterest-p"></i>Pinterest</a></li>
    <li class="steam"><a href=""><i class="fa fa-fw fa-steam"></i>Steam</a></li>
</ul>

</body>
</html>