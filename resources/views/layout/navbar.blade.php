<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" type="text/css" href="{{asset('font/flaticon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i');

html, body { font-family: 'Roboto', sans-serif !important; }

nav.navbar.bg-dark .dropdown-menu { background-color: #343a40 !important; }
nav.navbar.bg-dark .dropdown-menu .dropdown-item { color: rgba(255, 255, 255, 0.5); }
nav.navbar.bg-dark .dropdown-menu .dropdown-item:hover { color: rgba(255, 255, 255, 0.75); background: transparent; }
nav.navbar.bg-dark .dropdown-menu .dropdown-item.active { color: #fff; background: transparent; }
nav.navbar.bg-dark .dropdown-menu .dropdown-item.disabled { color: rgba(255, 255, 255, 0.25); }

.nic-menu { padding: 0 1rem .5rem 1rem;    background: linear-gradient(to right, #147fa3 10%, #F26721 90%); }
.nic-menu .nav-item strong { text-align:center; font-weight: 400; display: block; margin: .5rem 0; color: rgba(255, 255, 255, 0.75); border-bottom: 1px solid rgba(255, 255, 255, 0.75); }
.nic-menu .nav-item .nav-link { padding: .3rem 0; text-align:center; }

.dropdown-menu{ background: linear-gradient(to right, #147fa3 10%, #F26721 90%);}
.dropdown-item {color:white !important;}
.dropdown-item:hover {background: #eee !important; color:black !important;}

.nic-menu .nav-item .nav-link:hover { background: #eee !important;  color:black !important; }



.nic-menu-sm { width: 540px; }
.nic-menu-md { width: 720px; }
.nic-menu-lg { width: 960px; }
.nic-menu-xl { width: 1140px; }
.nic-menu-mega { width: 1400px; }

@media (max-width: 960px) {
	.nic-menu-sm,
	.nic-menu-md,
	.nic-menu-lg,
	.nic-menu-xl,
	.nic-menu-mega { width: 100% !important; }
}
.nav-item{
    padding-right:15px;
}
.nav-link{
    color:#fff !important;
}

    /************************ search area box *******************************/
    .wb-search-form {
    width: 100%;
    position: fixed;
    z-index: 99999;
    height: 80px;
    background: linear-gradient(to right, #147fa3 10%, #F26721 90%);
    top: -200px;
    left: 0;
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
    overflow-x: hidden;
    }

    .search-form-on .wb-search-form {
    top: 0;
    }

    .sticky .wb-search-form {
    height: 80px;
    }

    .wb-search-form form {
    position: relative;
    z-index: 1;
    }

    .wb-search-form form input {
    width: 100%;
    border: 1px solid rgba(161, 230, 227, 0.05);
    background-color: #ffffffbb;
    height: 50px;
    color: rgb(7, 7, 7);
    padding: 0 20px;
    font-size: 16px;
    }

    /* close button */
    #closeBtn {
    width: 30px;
    height: 30px;
    position: absolute;
    z-index: 9;
    background-color: transparent;
    margin-top: -15px;
    top: 50%;
    border-radius: 50%;
    right: 30px;
    color: #fff;
    cursor: pointer;
    text-align: center;
    opacity: 0.5;
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
    }

    #closeBtn:hover {
    opacity: 1;
    }

    #closeBtn > i {
    color: black;
    line-height: 30px;
    font-size: 28px;
    }
    

    /* Search box end */

    .navbar.scrolled{
        background: linear-gradient(to right, #147fa3 10%, #F26721 90%);
    }
 


    </style>

</head>
<body>


  <!-- for Search -->
  <div class="wb-search-form d-flex align-items-center">
      <div class="container">
      <div class="row">
          <div class="col-12">
          <div class="search-close-btn" id="closeBtn">
              <i class="fa fa-times-circle" aria-hidden="true"></i></i>
          </div>
          <!-- search form  -->
          <form action="search.php" method="GET">
              <input type="search" name="caviarSearch" id="search" placeholder="Search Your Desire Destinations">
              <input type="submit" class="d-none form-control" value="submit">
          </form>
          </div>
      </div>
      </div>
  </div>
  <!-- for search end -->

  <nav class="navbar fixed-top navbar-expand-xl navbar-dark ">
    <a href="{{url('home')}}" class="navbar-brand" style="display: inline-flex"><img src="images/log.png" alt="" height="50px"><span><h5 class="my-3" style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: #fff; letter-spacing: 3px; font-weight: 600;">&nbsp;Jobs<span style="color: #F26721;">.</span>Nokri</h5></span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item active"><a class="nav-link"  href="{{url('home')}}" >Home</a></li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Candidate</a>
          <div class="dropdown-menu nic-menu nic-menu-md">
          <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <ul class="nav flex-column">
                <li class="nav-item"><strong><i class="flaticon-briefcase"></i></strong></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Html5 & Css3</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Wordpress</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Javascript</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Photoshop</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Designer</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Construction</a></li>
              </ul>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <ul class="nav flex-column">
                <li class="nav-item"><strong><i class="flaticon-settings-1"></i></strong></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Html5 & Css3</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Wordpress</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Javascript</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Photoshop</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Designer</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Construction</a></li>
              </ul>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <ul class="nav flex-column">
                <li class="nav-item"><strong><i class="flaticon-placeholder"></i></strong></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Html5 & Css3</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Wordpress</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Javascript</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Photoshop</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Designer</a></li>
                <li class="nav-item"><a class="nav-link py-2" href="">Construction</a></li>
              </ul>
            </div>
          </div>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Jobs</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item py-2" href="{{url('job-listing-grid')}}">job listing grid</a>
            <a class="dropdown-item py-2" href="{{url('job-post')}}">job post</a>
            <a class="dropdown-item py-2" href="{{url('job-single')}}">job single</a>
            <a class="dropdown-item py-2" href="{{url('manage-job')}}">Manage Job</a>
            <a class="dropdown-item py-2" href="{{url('Edit-Profile')}}">Edit Profile</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Company</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item py-2" href="{{url('Admin-Login')}}">Admin</a>
              <a class="dropdown-item py-2" href="{{url('company-page')}}">company Page</a>
              <a class="dropdown-item py-2" href="{{url('applied-candi')}}">Applied Company</a>
              <a class="dropdown-item py-2" href="{{url('company-dashboard')}}">Company Dashboard</a>
              <a class="dropdown-item py-2" href="{{url('user-dashboard')}}">User Dashboard</a>
              <a class="dropdown-item py-2" href="{{url('Admin-dashboard')}}">Admin Dashboard</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item py-2" href="{{url('blog-grid')}}">Blogs-Grid</a>
            <a class="dropdown-item py-2" href="{{url('blog-single')}}">Blog</a>
            <a class="dropdown-item py-2" href="">News</a>
            <a class="dropdown-item py-2" href="">Science</a>
          </div>
        </li>

        <li class="nav-item ">
            <a class="nav-link" href="{{url('contact')}}">Contact</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{url ('service')}}">Service</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{url ('aboutus')}}">About</a>
        </li>

        <li class="nav-item ">
            <div class="wb-search-btn">
                <a href="#" class="nav-link" id="search-btn"><i class="fa fa-search text-light" aria-hidden="true"></i></a>
            </div>
        </li>
      </ul>
    
      <ul class="navbar-nav">
        @if( Session::has('useremail') || Cookie::has('useremail') )
        <li class="nav-item">
            <div class="nav-link" > 
                <a href="{{url ('logout')}}" class="float-left  px-2  text-light" ><i class="fa fa-power-off" style="font-size: 19px;">&nbsp;&nbsp;Logout</i></a>
            </div>
        </li>
        @else
        <li class="nav-item">
            <div class="nav-link" > 
                <a href="{{url ('login')}}" class="float-left px-2  text-light"><i class="fa fa-sign-in" style="font-size: 19px;">&nbsp;&nbsp;Login</i></a>
            </div>
        </li>
        @endif
      </ul>
    </div>
  </nav>


<script>
  $(document).ready(function(){
      $(".navbar-toggler-icon").on({

          click: function(){
              $(".navbar").css("background", "linear-gradient(to right, #147fa3 10%, #F26721 90%)");
  
          }  
      });
  });
</script>


<script>

  (function($) {
    "use strict";

    // search active code
    $("#search-btn, #closeBtn").on("click", function() {
    $("body").toggleClass("search-form-on");
    });
    // sticky active
    var $window = $(window);
    $window.on("scroll", function() {
    if ($window.scrollTop() > 0) {
        $("body").addClass("sticky");
    } else {
        $("body").removeClass("sticky");
    }
    });
  })(jQuery);

</script>

<script>
  $(window).scroll(function(){
    $('nav').toggleClass('scrolled', $(this).scrollTop() > 150);
  });
</script>

    
</body>
</html>