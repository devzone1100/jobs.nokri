<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" type="text/css" href="{{asset('font/flaticon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css'>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,500" rel="stylesheet"/>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
  



    <style>

        * {
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }
        
        ::-webkit-input-placeholder {
          color: #c3c3c3;
        }
        
        h1 {
          font-size: 24px;
        }
        
        h2 {
          font-size: 20px;
        }
        
        h3 {
          font-size: 15px;
        }
        
        .u-list {
          margin: 0;
          padding: 0;
          list-style: none;
        }
        
        .u-input {
          outline: 0;
          border: 1px solid #d0d0d0;
          padding: 5px 10px;
          height: 35px;
          font-size: 12px;
          -webkit-border-radius: 10px;
          border-radius: 10px;
          background-clip: padding-box;
        }
        
        .c-badge {
          font-size: 10px;
          font-weight: 700;
          min-width: 17px;
          padding: 5px 4px;
          border-radius: 100px;
          display: block;
          line-height: 0.7;
          color: #fff;
          text-align: center;
          white-space: nowrap;
          background-color: #f91605;
        }
        .c-badge--header-icon {
          position: absolute;
          bottom: -9px;
        }
        
        .tooltip {
          width: 120px;
        }
        .tooltip-inner {
          padding: 8px 10px;
          color: #fff;
          text-align: center;
          background-color: #147fa3;
          font-size: 12px;
          border-radius: 3px;
        }
        .tooltip-arrow {
          border-right-color: #147fa3 !important;
        }
        
        .hamburger-toggle {
          position: relative;
          padding: 0;
          background: transparent;
          border: 1px solid transparent;
          cursor: pointer;
          order: 1;
        }
        .hamburger-toggle [class*='bar-'] {
          display: block;
          background: #f6f9fa;
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
          -webkit-transition: .2s ease all;
          transition: .2s ease all;
          border-radius: 2px;
          height: 2px;
          width: 24px;
          margin-bottom: 4px;
        }
        .hamburger-toggle [class*='bar-']:nth-child(2) {
          width: 18px;
        }
        .hamburger-toggle [class*='bar-']:last-child {
          margin-bottom: 0;
          width: 12px;
        }
        .hamburger-toggle.is-opened {
          left: 3px;
        }
        .hamburger-toggle.is-opened [class*='bar-'] {
          background: #ffff;
        }
        .hamburger-toggle.is-opened .bar-top {
          -webkit-transform: rotate(45deg);
          transform: rotate(45deg);
          -webkit-transform-origin: 15% 15%;
          transform-origin: 15% 15%;
        }
        .hamburger-toggle.is-opened .bar-mid {
          opacity: 0;
        }
        .hamburger-toggle.is-opened .bar-bot {
          -webkit-transform: rotate(45deg);
          transform: rotate(-45deg);
          -webkit-transform-origin: 15% 95%;
          transform-origin: 15% 95%;
          width: 24px;
        }
        .hamburger-toggle:focus {
          outline-width: 0;
        }
        .hamburger-toggle:hover [class*='bar-'] {
          background: #ffff;
        }
        
        .header-icons-group {
          background: #F26721;
          display: flex;
          order: 3;
          margin-left: auto;
          height: 100%;
          border-left: 1px solid #cccccc;
        }
        .header-icons-group .c-header-icon:last-child {
          border-right: 0;
        }
        
        .c-header-icon {
          position: relative;
          display: flex;
          float: left;
          width: 70px;
          height: 100%;
          align-items: center;
          justify-content: center;
          line-height: 1;
          cursor: pointer;
          border-right: 1px solid #cccccc;
        }
        .c-header-icon i {
          color: #fff !important;
          font-size: 18px;
          line-height: 40px;
        }
        .c-header-icon--in-circle {
          border: 1px solid #d0d0d0;
          border-radius: 100%;
        }
        .c-header-icon:hover i {
          color: #f5642d;
        }
        
        .l-header {
          padding-left: 70px;
          position: fixed;
          top: 0;
          right: 0;
          z-index: 10;
          width: 100%;
          background: #ffffff;
          -webkit-transition: padding 0.5s ease-in-out;
          -moz-transition: padding 0.5s ease-in-out;
          -ms-transition: padding 0.5s ease-in-out;
          -o-transition: padding 0.5s ease-in-out;
          transition: padding 0.5s ease-in-out;
        }
        .l-header__inner {
          height: 100%;
          width: 100%;
          display: flex;
          height: 70px;
          align-items: center;
          justify-content: stretch;
          border-bottom: 1px solid;
          border-color: #cccccc;
        }
        .sidebar-is-expanded .l-header {
          padding-left: 220px;
        }
        
        .c-link {
          display: flex;
          height: 100%;
          width: 100%;
        }
        
        
        .c-dropdown {
          opacity: 0;
          text-align: left;
          position: absolute;
          flex-direction: column;
          display: none;
          width: 300px;
          top: 30px;
          right: -40px;
          background-color: #fff;
          overflow: hidden;
          min-height: 300px;
          border: 1px solid #d0d0d0;
          -webkit-border-radius: 10px;
          border-radius: 10px;
          background-clip: padding-box;
          -webkit-box-shadow: 0px 5px 14px -1px #cecece;
          -moz-box-shadow: 0px 5px 14px -1px #cecece;
          box-shadow: 0px 5px 14px -1px #cecece;
          -webkit-transition: all 0.3s ease-in-out;
          -moz-transition: all 0.3s ease-in-out;
          -ms-transition: all 0.3s ease-in-out;
          -o-transition: all 0.3s ease-in-out;
          transition: all 0.3s ease-in-out;
        }
        
        .l-sidebar {
          width: 70px;
          position: absolute;
          z-index: 10;
          left: 0;
          top: 0;
          bottom: 0;
          background: #147fa3;
          -webkit-transition: width 0.5s ease-in-out;
          -moz-transition: width 0.5s ease-in-out;
          -ms-transition: width 0.5s ease-in-out;
          -o-transition: width 0.5s ease-in-out;
          transition: width 0.5s ease-in-out;
        }
        .l-sidebar .logo {
          width: 100%;
          height: 70px;
          display: flex;
          align-items: center;
          justify-content: center;
          background-color: #f5642d;
        }
        .l-sidebar .logo .logo__txt {
          font-size: 26px;
          line-height: 1;
          color: #fff;
          text-align: center;
          font-weight: 700;
        }
        .logo__txt img {
          width: 40px;
          height: 40px;
          border-radius: 50%;
        }
        .l-sidebar__content {
          height: 100%;
          position: relative;
        }
        .sidebar-is-expanded .l-sidebar {
          width: 220px;
        }
        
        .c-menu > ul {
          display: flex;
          flex-direction: column;
        }
        .c-menu > ul .c-menu__item {
          color: #fff;
          max-width: 100%;
          overflow: hidden;
        }
        .c-menu > ul .c-menu__item__inner {
          display: flex;
          flex-direction: row;
          align-items: center;
          min-height: 60px;
          position: relative;
          cursor: pointer;
          -webkit-transition: all 0.5s ease-in-out;
          -moz-transition: all 0.5s ease-in-out;
          -ms-transition: all 0.5s ease-in-out;
          -o-transition: all 0.5s ease-in-out;
          transition: all 0.5s ease-in-out;
          text-decoration: none !important;
          color: #ffff !important;
        }
        .c-menu > ul .c-menu__item__inner:before {
          position: absolute;
          content: " ";
          height: 0;
          width: 2px;
          left: 0;
          top: 50%;
          margin-top: -18px;
          background-color: #f5642d;
          opacity: 0;
          -webkit-transition: all 0.5s ease-in-out;
          -moz-transition: all 0.5s ease-in-out;
          -ms-transition: all 0.5s ease-in-out;
          -o-transition: all 0.5s ease-in-out;
          transition: all 0.5s ease-in-out;
        }
        .c-menu > ul .c-menu__item.is-active .c-menu__item__inner {
          border-left-color: #5f9cfd;
          background-color: #147fa3;
        }
        .c-menu > ul .c-menu__item.is-active .c-menu__item__inner i {
          color: #ffff !important;
        }
        .c-menu > ul .c-menu__item.is-active .c-menu__item__inner .c-menu-item__title span {
          color: #ffff !important;
        }
        .c-menu > ul .c-menu__item.is-active .c-menu__item__inner:before {
          height: 36px;
          opacity: 1;
        }
        .c-menu > ul .c-menu__item:not(.is-active):hover .c-menu__item__inner {
          background-color: #f5642d;
          border-left-color: #f5642d;
        }
        .c-menu > ul .c-menu__item i {
          flex: 0 0 70px;
          font-size: 18px;
          font-weight: normal;
          text-align: center;
          -webkit-transition: all 0.5s ease-in-out;
          -moz-transition: all 0.5s ease-in-out;
          -ms-transition: all 0.5s ease-in-out;
          -o-transition: all 0.5s ease-in-out;
          transition: all 0.5s ease-in-out;
        }
        .c-menu > ul .c-menu__item .c-menu-item__expand {
          position: relative;
          left: 100px;
          padding-right: 20px;
          margin-left: auto;
          -webkit-transition: all 1s ease-in-out;
          -moz-transition: all 1s ease-in-out;
          -ms-transition: all 1s ease-in-out;
          -o-transition: all 1s ease-in-out;
          transition: all 1s ease-in-out;
        }
        .sidebar-is-expanded .c-menu > ul .c-menu__item .c-menu-item__expand {
          left: 0px;
        }
        .c-menu > ul .c-menu__item .c-menu-item__title {
          flex-basis: 100%;
          padding-right: 10px;
          position: relative;
          left: 220px;
          opacity: 0;
          -webkit-transition: all 0.7s ease-in-out;
          -moz-transition: all 0.7s ease-in-out;
          -ms-transition: all 0.7s ease-in-out;
          -o-transition: all 0.7s ease-in-out;
          transition: all 0.7s ease-in-out;
        }
        .c-menu > ul .c-menu__item .c-menu-item__title span {
          font-weight: 400;
          font-size: 14px;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          -webkit-transition: all 0.5s ease-in-out;
          -moz-transition: all 0.5s ease-in-out;
          -ms-transition: all 0.5s ease-in-out;
          -o-transition: all 0.5s ease-in-out;
          transition: all 0.5s ease-in-out;
        }
        .sidebar-is-expanded .c-menu > ul .c-menu__item .c-menu-item__title {
          left: 0px;
          opacity: 1;
        }
        .c-menu > ul .c-menu__item .c-menu__submenu {
          background-color: #147fa3;
          padding: 15px;
          font-size: 12px;
          display: none;
        }
        .c-menu > ul .c-menu__item .c-menu__submenu li {
          padding-bottom: 15px;
          margin-bottom: 15px;
          border-bottom: 1px solid;
          border-color: #147fa3;
          color: #5f9cfd;
        }
        .c-menu > ul .c-menu__item .c-menu__submenu li:last-child {
          margin: 0;
          padding: 0;
          border: 0;
        }
        
        main.l-main {
          width: 100%;
          height: 100%;
          padding: 70px 0 0 70px;
          -webkit-transition: padding 0.5s ease-in-out;
          -moz-transition: padding 0.5s ease-in-out;
          -ms-transition: padding 0.5s ease-in-out;
          -o-transition: padding 0.5s ease-in-out;
          transition: padding 0.5s ease-in-out;
        }
        main.l-main .content-wrapper {
          padding: 25px;
          height: 100%;
        }
        main.l-main .content-wrapper .page-content {
          border-top: 1px solid #d0d0d0;
          padding-top: 25px;
        }
        main.l-main .content-wrapper--with-bg .page-content {
          background: #fff;
          border-radius: 3px;
          border: 1px solid #d0d0d0;
          padding: 25px;
        }
        main.l-main .page-title {
          font-weight: 400;
          margin-top: 0;
          margin-bottom: 25px;
        }
        .sidebar-is-expanded main.l-main {
          padding-left: 220px;
        }
        
        /* dashboard end************* */

        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn_1 {
            border: 1px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 1px solid black;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .list-group a{
            text-decoration: none;
        }
        .card-body a {
            text-decoration: none;
        }

        /* grid */

        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .list-group a {
            text-decoration: none;
        }
        .form-control{
            border-radius: 0px;
            height: 45px;
        }

        .card-grid {
        flex-direction: row;
        align-items: center;
        border: 1px solid #a7a4a5;
        }
        .card-title {
        font-weight: bold;
        color: #F26721;
        }
        .card-grid a {
            text-decoration: none;
        }
        .card img {
        width: 30%;
        border-top-right-radius: 0;
        border-bottom-left-radius: calc(0.25rem - 1px);
        }
        
        .btn a {
            text-decoration: none;
        }
        .btn_3 {
            border: 0px solid #F26721; 
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_3:hover {
        background-position: left bottom;
        }
        .list-group a{
            text-decoration: none;
        }
        .btn_2 {
            border: 0px solid #147fa3;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 17px; 
            color: #ffff !important;
            transition: all .6s ease-out;
            border-radius: 0px;
            
            
        }
        .btn:focus{
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .btn_2 a {
            text-decoration: none;
            color: #fff !important;
        }

        /* drop */
        .parent {
        text-align: justify;
        padding-left: 10px;
        width: 250px;
        overflow: hidden;
        transition: .2s;
        }
        .parent a{
            text-decoration: none;
            color: #ffff !important;
        }
        

        .child {
        width: auto;
        height: 35px;
        background:#F26721;
        padding-left: 20px;
        display: flex;
        justify-content: flex-start;
        align-items: center;
        color: white;
        transition: 0.2s;
        cursor: pointer;
        }
        .child:hover {
        background-color: #727f83 !important;
        }

        .parent .c-menu__item__inner{
        background-color: #F26721 !important;
      }
    

        .button {
        padding: 0 20px;
        display: flex;
        justify-content: flex-start;
        align-items: center;

        width: 250px;
        height: 40px;
        background-color: #2a2a3d;
        cursor: pointer;

        color: white;
        border: none;
        transition: 0.2s;
        }
        .button:hover {
        background-color: #373752;
      }

    

      /* dropdown------------------ */

      .dropdown {
        float: right;
        position: relative;
        display: block;
        right: 0 !important;
      }

      .dropdown-content {
        margin:0px !important;
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        overflow: auto;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        right: 0;
        z-index: 1;
      }

      .dropdown-content a {
        color: black;
        padding: 0px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown:hover .dropdown-content {display: block;}
      .dropdown a:hover {background-color: #ddd;}

      .show {display: block;}

      /* dropdown End------------------ */

      /* bell-notification-------------------- */
      .notification {
  position: relative;
   
}

.notification i {
    cursor: pointer;
    color:red; 
}
 
.notification ul.dropdown {
  display: none;
  position: absolute;
  top: 150%;
  left:-128px;
  min-width: 240px;
  padding: 0;
  border: 2px solid #F26721; 
  border-radius: 0 0 .2em .2em;
  box-shadow: 0 1px 1px rgba(50,50,50,0.1); 
  
   
}
 
/* up arrow*/
.notification ul.dropdown:before {
    content: "";
    width: 0;
    height: 0;
    position: absolute;
    bottom: 100%;
    right: 87px;
    border-width: 0 10px 10px 10px;
    border-style: solid;
    border-color: #F26721 transparent; 
}
.notification ul.dropdown:after {
    content: "";
    width: 0;
    height: 0;
    position: absolute;
    bottom: 100%;
    right: 89px;
    border-width: 0 8px 7px 7px;
    border-style: solid;
    border-color: #fff transparent;    
}    
.notification ul.dropdown li {
  height: 75px;
  list-style-type: none;
  border-top: 1px solid lightgrey;
  background-color: #147fa3;
}   
 
.notification ul.dropdown li:hover{
  background-color: #eee;
}
    
.notification ul.dropdown li:first-child {
  list-style-type: none;
  border-top: none;    
}
    
.notification ul.dropdown .fa-circle{
    font-size: 15px;
    color: rgba(115, 187, 22, 1);  
} 
    
.notification ul.dropdown li a {
  text-decoration: none;
  padding: .5em 1em;
  display: block;
  color:#ffffff;
  font-size: 1.2em; 
}
 
/*View All Notification*/
.notification ul.dropdown .fa-list{
    color: rgba(115, 187, 22, 1); 
  
   
}

  /*---------- user_setting-------------------- */
      .user_setting {
  position: relative;
   
}

.user_setting i {
    cursor: pointer;
    color:red; 
}
 
.user_setting ul.dropdown {
  display: none;
  position: absolute;
  top: 150%;
  left:-128px;
  min-width: 170px;
  padding: 0;
  /* border: 2px solid #F26721; 
  border-radius: 0 0 .2em .2em; */
  box-shadow: 0 1px 1px rgba(50,50,50,0.1); 
  
   
}
 
/* up arrow*/
.user_setting ul.dropdown:before {
    content: "";
    width: 0;
    height: 0;
    position: absolute;
    bottom: 100%;
    right: 22px;
    border-width: 0 10px 10px 10px;
    border-style: solid;
    border-color: #F26721 transparent; 
}
.user_setting ul.dropdown:after {
    content: "";
    width: 0;
    height: 0;
    position: absolute;
    bottom: 100%;
    right: 24px;
    border-width: 0 8px 7px 7px;
    border-style: solid;
    border-color: #fff transparent;    
}    
.user_setting ul.dropdown li {
  list-style-type: none;
  border-top: 1px solid lightgrey;
  background-color: #147fa3;
}   
 
.user_setting ul.dropdown li:hover{
  background-color: #eee;
}
    
.user_setting ul.dropdown li:first-child {
  list-style-type: none;
  border-top: none;    
}
    
  
.user_setting ul.dropdown li a {
  text-decoration: none;
  padding-left:10px;
 
  display: block;
  color:#ffffff;
  font-size: 1em; 
}

.user_setting ul.dropdown li a:hover{
      text-decoration: none;
      padding-left:10px;
      color: black;
      display: block;
      /* color:#ffffff; */
      font-size: 1em; 
    }
 

    


    



  



            
        
        
  </style>
        
</head>
<body>
 
  <body class="sidebar-is-reduced">
    <header class="l-header">

      <div class="l-header__inner clearfix">
        <div class="c-header-icon js-hamburger" style="background: #147fa3;">
          <div class="hamburger-toggle"><span class="bar-top"></span><span class="bar-mid"></span><span class="bar-bot"></span></div>
        </div>
       
        
        <div class="c-link" style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
            <h3 class="px-4 py-2 text-light">Admin-Dashboard</h3>

        </div>

        
        <div class="header-icons-group">

          <div  class="c-header-icon basket">
            <nav class="notification"> <i class="fa fa-bell dropdown-toggle"></i> 
              <ul class="dropdown text-light">
                <li><a href="#">Message 1</a></li>
                <li><a href="#">More text, More text</a></li>
                <li><a href="#">Message 3</a></li>
                <li><a href="#" style="color: #F26721 "><i class="fa fa-list"></i> View All Notifications</a></li>     
              </ul>
            </nav>
          </div>  

          <div class="c-header-icon basket dropbtn">
            <nav class="user_setting"> <i class="fa fa-user dropdown-toggle"></i> 
              <ul class="dropdown text-light">
                <li><a href="{{url('Admin-edit-profile')}}"><i class="fa fa-user px-3" style="color:#F26721 !important;"></i> &nbsp;&nbsp; Profile</a></li>
                <li><a href=""><i class="fa fa-cogs px-3" style="color:#F26721 !important;"></i> &nbsp;&nbsp; Setting</a></li>
                <li><a href="{{url ('Admin-logout')}}"><i class="fa fa-power-off px-3" style="color:#F26721 !important;"></i> &nbsp;&nbsp; Logout</a></li>
                   
              </ul>
            </nav>
          </div>  
  
        </div>

      </div>
    </header>
    <div class="l-sidebar position-fixed">
      <div class="logo">
        <div class="logo__txt"><img src="images/riban_30.png" class="bg-light"></div>
      </div>
      <div class="l-sidebar__content">
        <nav class="c-menu js-menu">
          <ul class="u-list">
            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Admin dashboard">
              <a href="{{url('Admin-dashboard')}}" class="c-menu__item__inner"><i class="fa fa-tachometer"></i>
                <div class="c-menu-item__title"><span>Admin dashboard</span></div>
              </a>
            </li>

            <!-- <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Data Table">
              <a  onClick='DropdownFunction()' class="c-menu__item__inner"><i class="fa fa-table"></i>
                <div class="c-menu-item__title"><span>Data Table &nbsp;&nbsp; &nbsp;</span></div>
                <i class="fa fa-caret-down pr-2"></i>
              </a>
              <div id='parent_1' class="parent">
                <a href="{{url('registerData')}}" class="c-menu__item__inner"><i class="fa fa-user "></i>  <div class="c-menu-item__title"> <span class="px-3">Register Table</span> </div></a>
                <a href="{{url('jobpostData')}}" class="c-menu__item__inner"><i class="fa fa-suitcase "></i>  <div class="c-menu-item__title"> <span class="px-3">Job-Post Table</span> </div></a>
                <a href="{{url('paymentData')}}" class="c-menu__item__inner"><i class="fa fa-money "></i>  <div class="c-menu-item__title"> <span class="px-3">Payment Table</span> </div></a>
              </div>
            </li>

            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Manage">
              <a  onClick='DropdownFunction_2()' class="c-menu__item__inner"><i class="fa fa-suitcase"></i>
                <div class="c-menu-item__title"><span>Manage &nbsp;&nbsp;  &nbsp;&nbsp; &nbsp;</span></div>
                <i class="fa fa-caret-down"></i>
              </a>
              <div id='parent_2' class="parent">
                <a href="{{url('#')}}" class="c-menu__item__inner"><i class="fa fa-suitcase "></i>  <div class="c-menu-item__title"> <span class="px-3">Manage Job </span> </div></a>
                <a href="{{url('#')}}" class="c-menu__item__inner"><i class="fa fa-filter "></i>  <div class="c-menu-item__title"> <span class="px-3">Jobs Category</span> </div></a>
                <a href="{{url('#')}}" class="c-menu__item__inner"><i class="fa fa-file "></i>  <div class="c-menu-item__title"> <span class="px-3">Manage Company</span> </div></a>
                <a href="{{url('#')}}" class="c-menu__item__inner"><i class="fa fa-money "></i>  <div class="c-menu-item__title"> <span class="px-3">Manage Application</span> </div></a>
              </div>
                
            </li> -->
           
            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="manage User">
              <a href="{{url('registerData')}}" class="c-menu__item__inner"><i class="fa fa-user"></i>
                <div class="c-menu-item__title"><span>Manage User Data</span></div>
              </a>
            </li>
            
            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="manage Job">
              <a href="{{url('jobpostData')}}" class="c-menu__item__inner"><i class="fa fa-suitcase"></i>
                <div class="c-menu-item__title"><span>Manage Jobs Data</span></div>
              </a>
            </li>

            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Manage Payment">
              <a href="{{url('paymentData')}}" class="c-menu__item__inner"><i class="fa fa-money"></i>
                <div class="c-menu-item__title"><span>Manage Payment Data</span></div>
              </a>
            </li>

            
            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Manage Applied User">
              <a href="{{url('applied-user-data')}}" class="c-menu__item__inner"><i class="fa fa-share"></i>
                <div class="c-menu-item__title"><span>Applied User Data</span></div>
              </a>
            </li>

            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Add Plan">
              <a href="{{url('add-plans')}}" class="c-menu__item__inner"><i class="fa fa-plus"></i>
                <div class="c-menu-item__title"><span>Add Plan</span></div>
              </a>
            </li>

            <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Settings">
              <a  onClick='DropdownFunction_4()' class="c-menu__item__inner"><i class="fa fa-cogs"></i>
                <div class="c-menu-item__title"><span>Settings&nbsp;&nbsp; &nbsp;  &nbsp;&nbsp; &nbsp;</span></div>
                <i class="fa fa-caret-down pr-2"></i>
              </a>
              <div id='parent_4' class="parent">
                  <a href="{{url('#')}}" class="c-menu__item__inner"><i class="fa fa-cogs"></i>  <div class="c-menu-item__title"> <span class="px-3">Session Time </span> </div></a>
                  <a href="{{url('#')}}" class="c-menu__item__inner"><i class="fa fa-money "></i>  <div class="c-menu-item__title"> <span class="px-3">Charges Amount</span> </div></a>
                <!-- <a href="registerData" class='child c-menu-item__title'>Session Time</a>
                <a href="jobpostData" class='child c-menu-item__title'>Charges Amount</a> -->
              </div>
              
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </body>



  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.0/moment.min.js'></script>
  
<script>
  /* When the user clicks on the button, 
  toggle between hiding and showing the dropdown content */
  function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }

  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }
</script>
  
        
  <script >"use strict";
  
    var Dashboard = function () {
        var global = {
            tooltipOptions: {
                placement: "right"
            },
            menuClass: ".c-menu"
        };
    
        var menuChangeActive = function menuChangeActive(el) {
            var hasSubmenu = $(el).hasClass("has-submenu");
            $(global.menuClass + " .is-active").removeClass("is-active");
            $(el).addClass("is-active");
    
            // if (hasSubmenu) {
            // 	$(el).find("ul").slideDown();
            // }
        };
    
        var sidebarChangeWidth = function sidebarChangeWidth() {
            var $menuItemsTitle = $("li .menu-item__title");
    
            $("body").toggleClass("sidebar-is-reduced sidebar-is-expanded");
            $(".hamburger-toggle").toggleClass("is-opened");
    
          
        };
    
        return {
            init: function init() {
                $(".js-hamburger").on("click", sidebarChangeWidth);
    
                $(".js-menu li").on("click", function (e) {
                    menuChangeActive(e.currentTarget);
                });
    
            }
        };
    }();
    
    Dashboard.init();

  </script>

  <script>
    const parent = document.querySelector("#parent_1");
        const children = parent.children;

        let parentHeight = 0;
        for (const key in children) {
        if (typeof children[key].offsetHeight == "number")
            parentHeight += children[key].clientHeight;
        }
        parent.style.height = `0px`;

        const DropdownFunction = () => {
        if (parent.offsetHeight == 0){
            parent.style.height = `${parentHeight}px`;
        }
        else parent.style.height = "0";
    };

 

        
  </script>



  <script>
       const parent_2 = document.querySelector("#parent_2");
        const children_2 = parent_2.children_2;
        let parentHeight_2 = 0;
        for (const key in children_2) {
        if (typeof children_2[key].offsetHeight == "number")
            parentHeight_2 += children_2[key].clientHeight_2;
        }
        parent_2.style.height = `0px`;

        const DropdownFunction_2 = () => {
        if (parent_2.offsetHeight == 0){
            parent_2.style.height = `${parentHeight}px`;
        }
        else parent_2.style.height = "0";
    };
  </script>



<script>
    
    const parent_4 = document.querySelector("#parent_4");
        const children_4 = parent_4.children_4;

        let parentHeight_4 = 0;
        for (const key in children_4) {
        if (typeof children_4[key].offsetHeight == "number")
            parentHeight_4 += children_4[key].clientHeight;
        }
        parent_4.style.height = `0px`;

        const DropdownFunction_4 = () => {
        if (parent_4.offsetHeight == 0){
            parent_4.style.height = `${parentHeight}px`;
        }
        else parent_4.style.height = "0";
    };
  </script>


<script>
  $(document).ready(function(){




var down = false;

$('#bell').click(function(e){

var color = $(this).text();
if(down){

$('#box').css('height','0px');
$('#box').css('opacity','0');
down = false;
}else{

$('#box').css('height','auto');
$('#box').css('opacity','1');
down = true;

}

});

});
</script>

<!-- *****************bell-notification---------- -->
<script>
  $(function() {

  // Dropdown toggle
  $('.dropdown-toggle').click(function() {
    $(this).next('.dropdown').toggle( 400 );
  });

  $(document).click(function(e) {
    var target = e.target;
    if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle')) {
      $('.dropdown').hide() ;
    }
  });

  });
</script>  
<!-- *****************bell-notification =End ---------->






    
</body>
</html>