<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
         /* footer------------------- */
         .footer ul li {
            padding-top: 20px;
        }
        .footer ul li a {
            color: #fff;
            
        }
        .footer ul li a:hover {
            color: #ffff;
            
        }
        .footer .d-flex a {
            margin-left: 10px;
        }
        .footer .underline li a {
            text-decoration: none;
        }

        /* footer end----------------- */

         /* button up scroll */
         #button_up {
        border: 1px solid #ffff;
        display: inline-block;
        background: linear-gradient(to right, #147fa3 10%, #F26721 90%);
        width: 50px;
        height: 50px;
        text-align: center;
        border-radius: 50px;
        position: fixed;
        bottom: 30px;
        right: 30px;
        transition: background-color .3s, 
            opacity .5s, visibility .5s;
        opacity: 0;
        visibility: hidden;
        z-index: 1000;
        }
        #button_up::after {
        content: "\f102";
        font-family: FontAwesome;
        font-weight: normal;
        font-style: normal;
        font-size: 2em;
        line-height: 50px;
        color: #fff;
        }
        #button_up:hover {
        cursor: pointer;
        background-color: #333;
        }
        #button_up:active {
        background-color: #555;
        }
        #button_up.show {
        opacity: 1;
        visibility: visible;
        }
         /* button up scroll End */
    </style>
</head>
<body>

  <!--------------------------------- footer-------------------------->

        <div class="container-fluid mt-5 footer" style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
            <div class="row text-white px-5 ">
                <div class="col-md-3 col-sm-3 col-12 px-5 py-4">
                    <ul style="list-style-type: none;">
                        <a href="index.html" style="text-decoration: none;"><h5 class="pt-4" style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: #fff; letter-spacing: 3px; font-weight: 600;">Jobs<span style="color: #F26721;">.</span>Nokri</h5></a>
                        <div>
                            <li><i class="fa fa-map-marker" aria-hidden="true"> &nbsp;&nbsp;&nbsp;</i>City Avenue, Floor 1 Malbourne New India.</li>
                            <li><i class="fa fa-phone" aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;</i>1 -234 -456 -7890</li>
                            <li><a href=""><i class="fa fa-envelope" aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;</i>@jbdesks.com</a></li>
                            <li><a href=""><i class="fa fa-google" aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;</i>@google.com</a></li>
                            <li><a href=""><i class="fa fa-linkedin" aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;</i>@linkedin.com</a></li>
                            <div class="row d-flex py-4"> 
                                <a href="" class="btn btn-success"> <i class="fa fa-user"></i></a> 
                                <a href="" class="btn btn-primary"> <i class="fa fa-envelope-o"></i></a> 
                                <a href="" class="btn btn-danger"> <i class="fa fa-google"></i></a> 
                                <a href="" class="btn btn-info"> <i class="fa fa-linkedin"></i></a> 

                            </div>
                        </div>
                    </ul> 
                </div>
                
                <div class="col-md-3 col-sm-3 col-12 p-5">
                    <h5 class="px-4">FEATURES</h5>
                    <ul style="list-style-type: square;" class="underline">
                        <li><a href="">Job management&biling</a></li>
                        <li><a href="">Time& material tracking</a></li>
                        <li><a href="">Standard compliance</a></li>
                        <li><a href="">Realtime GPS tracking</a></li>
                        <li><a href="">Client portal</a></li>
                        <li><a href="">Powerful Workful</a></li>
                    </ul>
                </div>
                
                <div class="col-md-3 col-sm-3 col-12 p-5">
                  <h5 class="px-4">BROWSE</h5>
                    <ul style="list-style-type: square;" class="underline">
                      <li><a href="">Freelancers By Category</a></li>
                      <li><a href="">Freelancers In USA</a></li>
                      <li><a href="">Freelancers In UK</a></li>
                      <li><a href="">Freelancers In Canada</a></li>
                      <li><a href="">Freelancers In india</a></li>
                      <li><a href="">Find jobs</a></li>
                    </ul>
                </div>
                
                <div class="col-md-3 col-sm-3 col-12 p-5">
                    <h5 class="px-4">APP & INTEGRATION</h5>
                    <ul style="list-style-type: none;" class="underline">
                        <li><a href="">Xero</a></li>
                        <li><a href="">Reckon</a></li>
                        <li><a href="">Flexidocs</a></li>
                        <li><a href="">Microsoft Excahange</a></li>
                        <li><a href="">Mailchimp</a></li>
                        <li><a href="">MYOB</a></li>
                    </ul>
                </div>
            </div>
            <div class="row py-5">
                <div class="col-md-12 text-center text-light">
                    <p><i class="fa fa-copyright"></i> 2019 Jobs.Nokri. All Rights Reserved.</p>
                </div>
            </div>
        </div>

        <a id="button_up" style="text-decoration: none;"></a>

        <!-- end footer-------------------------------------------------- -->


        <script>
               // button up scroll******************

            var btn = $('#button_up');

            $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
            });

            btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
            });
        </script>
     
    
</body>
</html>