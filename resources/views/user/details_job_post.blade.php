<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    
    <style>
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            color: #fff;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 1px solid black;
            background: linear-gradient(to right, #147fa3 50%,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            height: 50px;
            width: 50px;
            color: #fff;
            font-size: 25px;
            transition: all .6s ease-out;
            border-radius: 60%;
            
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .btn_2 a {
            text-decoration: none;
        }
        .list-design li {
            padding-top: 15px;
        }
        .map-container-6{
            overflow:hidden;
            padding-bottom:56.25%;
            position:relative;
            height:0;
        }
        .map-container-6 iframe{
            bottom: 90px;
            height:70%;
            width:70%;
            position:absolute;
            border-radius: 35px;
            margin-left: 180px;
        }
        .d-flex a {
            margin-left: 10px;
        }
        .card-text {
            text-align:justify;
        }
        button{
            border:0px solid red !important;
            outline-color:white !important;
        }
        .btn{
            border:0px solid red !important;
            outline-color:white !important;
        }
        a {
            text-decoration:none !important;
        }

        /* loader--------------------- */
        .loader {
            display: none;
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
            }

            .loading {
            border: 3px solid #ccc;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            border-top-color: #F26721;
            border-left-color:  #F26721;
            animation: spin 1s infinite ease-in;
            }

            @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        /* loader--------------------- */

        
    </style>
</head>
<body>

@include('layout.User_sidemenu')

    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg">
        <!-- <h5 class="page-title">Dashboard</h5> -->
            <div class="container-fluid px-5">

                <?php
                // print_r($register_data);
                ?>

                @if(Session::has('useremail') || Cookie::has('useremail'))
                   
                    <div class="row mt-5 px-5">
                        <div class="col-md-3 col-sm-12 col-12 ">
                            <div class="card" style="">
                                <div class="card-header" style=" background: linear-gradient(to right, #13bef7 10%, #147fa3 90%);">Jobs Overview</div>
                                    <div class="pt-4">
                                        <img class="card mx-auto" src="{{asset('company_logos/'.$data->company_logo)}} ">
                                        <!-- <img src="images/riban_21.jpg" class="card mx-auto" alt="..."> -->
                                    </div>
                                <div class="card-body text-center px-4">
                                <p class="card-title" style="color: #147fa3;" > <span style="text-transform: uppercase !important; color: #F26721;"> {{$data["Job_Title"]}} </span> ( {{$data["Experience"]}} )</p>
                                <p class="card-text py-2 text-center">Webstrot Technology Pvt. Ltd.</p>
                                <a href="#" class="py-1 px-2 text-danger border border-danger"><i class="fa fa-heart"></i></a>
                                <a href="#" class="btn_1 ml-3 px-3 py-2 text-light" style="text-decoration:none;">Part Time</a>
                                </div>
                                <ul class="list-group list-group-flush mb-0">
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-calendar pt-2 px-2"></i>&nbsp;Date Posted:<ul><li style="list-style: none; color: #F26721;">{{$data->created_at->format('d-M-Y') }} </li></ul>
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-map-marker pt-2 px-2"></i>&nbsp;&nbsp;Location:<ul><li style="list-style: none; color: #147fa3; "> <span style="list-style: none; color: #F26721;">{{$data["city"]}}</span>  ({{$data["country"]}})</li></ul>
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-info-circle pt-2 px-2"></i>&nbsp;Job Title:<ul><li style="list-style: none; color: #F26721; text-transform: capitalize !important;">{{$data["Job_Title"]}}</li></ul>
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-user pt-2 px-2"></i>&nbsp;Hours:<ul><li style="list-style: none; color: #F26721;">{{$data["Working_Hours"]}}</li></ul>
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-money pt-2 px-2"></i>&nbsp;Salary:<ul><li style="list-style: none; color: #F26721;">{{$data["Salary"]}}</li></ul>
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-th-large pt-2 px-2"></i>&nbsp;Category:<ul><li style="list-style: none; color: #F26721;">{{$data["Job_Category"]}}</li></ul>
                                <li class="list-group-item  text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-star pt-2 px-2"></i>&nbsp;Experience:<ul><li style="list-style: none; color: #F26721;">{{$data["Experience"]}}</li></ul>
                                </ul>
                                <div class="card-body text-center">

                                    <form method="post" action="{{url('applieduserinsret')}}">
                                        @csrf 
                                        <input type="hidden" name="job_id" value="{{$data['job_id']}}">
                                        <input type="hidden" name="company_id" value="{{$data['company_id']}}">
                                        <input type="hidden" name="company_email" value="{{$data['company_email']}}">
                                        <input type="hidden" name="job_title" value="{{$data['Job_Title']}}">
                                        <input type="hidden" name="applied_useremail" value="{{Session::get('useremail')}}">
                                        <input type="hidden" name="applied_userid" value="{{Session::get('userid')}}">
                                        <input type="hidden" name="applied_firstname" value="{{$register_data->Firstname}}">
                                        <input type="hidden" name="applied_lastname" value="{{$register_data->Lastname}}">
                                        <input type="hidden" name="applied_phone" value="{{$register_data->phone_no}}">

                                        <div class="row">
                                            <div class="col-md-10">
                                                 <input name="applieduser" type="submit" id="loader" class="btn btn-primary text-light btn-outline-light form-control" value="Apply Now !"> 
                                            </div>
                                            <div class="col-md-1 mt-3 pl-0 ">
                                                <span><div class='spinner-displayer '></div></span>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-12  mx-auto">
                            <div class="card" style="border: 1px solid #F26721;">
                                <div class="card py-2 px-3">
                                    <h5>Job Description</h5>
                                </div>
                                <p class="card-text px-3 pt-4">
                                    {{$data["about_job"]}}
                                </p>
                                <div class="card-body">
                                    <a href="#" class="card-link"><i class="fa fa-globe"></i>&nbsp;&nbsp; www.google.com</a>
                                    <a href="#" class="card-link"><i class="fa fa-download"></i>&nbsp;&nbsp;Download info</a>
                                </div>
                                
                                <div class="card py-2 px-3">
                                    <h5>Responsibilities</h5>
                                </div>
                                <p class="card-text px-3 pt-4">
                                    Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit.
                                    <ul style="list-style-type:square;" class="list-design">
                                        <li>Build next-generation web applications with a focus on the client side.</li>
                                        <li>Redesign UI's, implement new UI's, and pick up Java as necessary</li>
                                        <li>Explore and design dynamic and compelling consumer experiences.</li>
                                        <li>Design and build scalable framework for web applications.</li>
                                    </ul>
                                </p>

                                <div class="card py-2 px-3">
                                    <h5>Minimum Qualifications</h5>
                                </div>
                                <ul style="list-style-type:square;" class="list-design">
                                    <li>Build next-generation web applications with a focus on the client side.</li>
                                    <li>Redesign UI's, implement new UI's, and pick up Java as necessary</li>
                                    <li>Explore and design dynamic and compelling consumer experiences.</li>
                                    <li>Design and build scalable framework for web applications.</li>
                                </ul>
                                
                                <div class="card py-2 px-3">
                                    <h5>How To Apply</h5>
                                </div>
                                <p class="card-text px-3 py-4 pr-5">
                                    Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit.
                                </p>

                                <div class="card py-2 px-3">
                                    <div class="row d-flex py-4 mx-auto">
                                        <span style="font-size: 20px;" class="pt-2">Share : </span>
                                        <a href="" class="btn btn-success"> <i class="fa fa-user"></i></a> 
                                        <a href="" class="btn btn-primary"> <i class="fa fa-envelope-o"></i></a> 
                                        <a href="" class="btn btn-danger"> <i class="fa fa-google"></i></a> 
                                        <a href="" class="btn btn-info"> <i class="fa fa-linkedin"></i></a> 
                                    </div>
                                </div>
                            
                                
                            </div>
                        </div>
                    </div>
                @endif       
            </div>
            @include('layout.footer')
        </div>
    </main>


    <script>
        function spinner() {
        const spinnerDisplayer = document.querySelector('.spinner-displayer');
        const loader = document.getElementById('loader');

        loader.addEventListener('click', () => {
        spinnerDisplayer.classList.add('loading');
    })
    }

    spinner();
    </script>
 
        

    
</body>
</html>