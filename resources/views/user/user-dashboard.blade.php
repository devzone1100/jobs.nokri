<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>


        /* verifcation document model */

      .form-control:focus {
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
      }
      .btn:focus{
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
      }

      /* data total use detail */
      .card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5em;
    opacity: 0.2;
    
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }

    </style>
        
</head>


<body>
        

 

  @include('layout.User_sidemenu')

 
  
  <main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg">   

      <!-- <h1 class="page-title">User Dashboard</h1> -->

        <div class="container-fluid pb-5">
            <div class="row">
            
                <div class="col-md-3">
                    <a href="{{url('user-applied-job')}}">
                      <div class="card-counter danger">
                      <i class="fa fa-share"></i>
                      <span class="count-numbers">{{$apply_jobs_data}}</span>
                      <span class="count-name">Applied Jobs</span>
                      </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers">0</span>
                    <span class="count-name">Sortlist Application</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter primary">
                    <i class="fa fa-code-fork"></i>
                    <span class="count-numbers">0</span>
                    <span class="count-name">Rejected</span>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card-counter success">
                    <i class="fa fa-check"></i>
                    <span class="count-numbers">0</span>
                    <span class="count-name">Selected</span>
                    </div>
                </div>

            </div>
        </div>
    
        <div class="page-content p-0 py-1" style="background: #eee;">
          <!-- content-here -->

         

        </div>

    </div>


  </main>









    
</body>
</html>