<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job-listing-grid</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


    <style>
        .nav{
            /* background-color: #336683; */
            padding: 25px;
        }

        .nav-tabs {
            border-bottom: none !important;
        }

        #tabs .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            color: #eee !important;
            background-color: #a4d6ef !important;
            border-color: transparent transparent #f3f3f3;
        }
        #tabs .nav-tabs .nav-link {
            border: 1px solid transparent;
            color: #eee;
            font-size: 20px;
            background-color: #F26721;
            border: 1px solid #a4d6ef;
            color: #fff;
        }

        .nav-item {
            border-radius: 0 !important;
        }

        .nav-item:last-child {
            border-top-right-radius: .5rem !important;
            border-bottom-right-radius: .5rem !important;
        }

        .nav-item:first-child {
            border-top-left-radius: .5rem !important;
            border-bottom-left-radius: .5rem !important;
        }

        .nav-tabs .nav-link.active{
        background-color:  #147fa3 !important; 
        color: #fff;

        }

        a {
            text-decoration: none !important;
        }

        .no-gutters img {
            height: 75px !important;
            width: 150px !important;
        }


/* viewmore*************************/
    .viewmore .readmore {
    display:none;
    /* padding: 10px; */
    /* border-width: 0 1px 1px 0; */
    /* border-style: solid; */
    /* border-color: #fff; */
    /* box-shadow: 0 1px 1px #ccc; */
    /* margin-bottom: 5px; */
    /* background-color: #f1f1f1; */
    }
    .totop {
        position: fixed;
        bottom: 10px;
        right: 20px;
    }
    .totop a {
        display: none;
    }
    a, a:visited {
        color: #33739E;
        text-decoration: none;
        display: block;
        /* margin: 10px 0; */
    }
    a:hover {
        text-decoration: none;
    }
    #loadMore {
        margin-top:20px;
        padding: 10px;
        text-align: center;
        background-color: #33739E;
        color: #fff;
        border-width: 0 1px 1px 0;
        border-style: solid;
        border-color: #fff;
        box-shadow: 0 1px 1px #ccc;
        transition: all 600ms ease-in-out;
        -webkit-transition: all 600ms ease-in-out;
        -moz-transition: all 600ms ease-in-out;
        -o-transition: all 600ms ease-in-out;
    }
    #loadMore:hover {
        background-color: #F26721;
        color: #fff;
    }

/* viewmore-end************************ */

main.l-main .content-wrapper {
    padding: 0 !important;
}


        

    </style>
</head>
<body>

@include('layout.User_sidemenu')


<main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg">

        <div class="page-content p-0 py-1" style="background: #eee;">

            <!-- Tabs -->
            <section id="tabs">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="row mt-4">
                                        <div class="col-12 col-md-10 col-lg-8">
                                            <form class="">
                                                <div class="card-body row no-gutters align-items-center">
                                                
                                                    <!--end of col-->
                                                    <div class="col-md-9">
                                                        <input class="form-control" id='search' type="text"  placeholder="Search topics or keywords">
                                                    </div>
                                                    <!--end of col-->
                                                    <div class="col-md-3 pl-3">
                                                        <button class="btn  py-3 px-5 test-light job-search-btn" type="submit" style="background:#F26721; color:#fff;">Search</button>
                                                    </div>
                                                    <!--end of col-->
                                                </div>
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                </div>
                                <div class="col-md-3 pr-0">
                                    <nav class="">
                                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"> <i class="fa fa-list"></i> List</a>
                                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"> <i class="fa fa-th"></i> Grid</a>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            
                            <div class="row mt-5">
                            
                                <div class="col-md-3">
                                    <div class="card">
                                        
                                        <aside class="">
                                            <div class="card">
                                                <article class="card-group-item">
                                                    <header class="card-header">
                                                        <h6 class="title" style="font-size:15px;"> <i class="fa fa-building" style="font-size:15px;"></i> Company By</h6>
                                                    </header>
                                                    <div class="filter-content">
                                                        <div class="card-body">
                                                            <div>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox" >&nbsp;&nbsp; linux <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </article>
                                            </div> 
                                        </aside>

                                        <aside class="">
                                            <div class="card">
                                                <article class="card-group-item">
                                                    <header class="card-header">
                                                        <h6 class="title" style="font-size:15px;"> <i class="fa fa-filter" style="font-size:20px;"></i>Job Type By</h6>
                                                    </header>
                                                    <div class="filter-content">
                                                        <div class="card-body">
                                                            <div>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </article>
                                            </div> 
                                        </aside>

                                        <aside class="">
                                            <div class="card">
                                                <article class="card-group-item">
                                                    <header class="card-header">
                                                        <h6 class="title" style="font-size:15px;"> <i class="fa fa-street-view" style="font-size:20px;"></i> Location By</h6>
                                                    </header>
                                                    <div class="filter-content">
                                                        <div class="card-body">
                                                            <div>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                                <li href="#" class='child c-menu-item__title'><input class="mb-2" type="checkbox">&nbsp;&nbsp; Graphic Designer <span class="badge badge-success ml-2">21</span></li>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </article>
                                            </div> 
                                        </aside>

                                        <aside class="">
                                            <div class="card">
                                                <article class="card-group-item">
                                                    <header class="card-header">
                                                        <h6 class="title">Range input </h6>
                                                    </header>
                                                    <div class="filter-content">
                                                        <div class="card-body">
                                                        <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                        <label>Min</label>
                                                        <input type="number" class="form-control" id="inputEmail4" placeholder="$0">
                                                        </div>
                                                        <div class="form-group col-md-6 text-right">
                                                        <label>Max</label>
                                                        <input type="number" class="form-control" placeholder="$1,0000">
                                                        </div>
                                                        </div>
                                                        </div> 
                                                    </div>
                                                </article>
                                            </div> 
                                        </aside>

                                    </div> 

                                </div>  
                            
                                <div class="col-md-9">
                                    <div class="tab-content  px-3 px-sm-0" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <div class="row viewmore">
                                                @php  
                                                $i=0;
                                                @endphp
                                                @foreach($data as $d) 
                                                <div class="col-md-12 col-sm-12 col-12 readmore "  >
                                                    <div class="card card-body job-list">
                                                        <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                                            <div class="mr-2 mb-3 mb-lg-0 no-gutters"><img class="card-img px-5" src="{{asset('company_logos/'.$d['company_logo'])}}"> </div>
                                                            <div class="media-body media-search">
                                                                <p class="media-title font-weight-semibold" style="color: #147fa3;"> <span href="#" data-abc="true" style="text-transform: uppercase !important; color: #F26721;">{{$data[$i]["Job_Title"]}} </span> ( {{$data[$i]["Experience"]}} )</p>
                                                                <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                                                                    <li class="list-inline-item"><a href="#" class="text-dark" data-abc="true">{{$data[$i++]["Salary"]}}</a></li>
                                                                </ul>
                                        
                                                                <p class="mb-3">Company Hire Developer </p>
                                                                <ul class="list-inline list-inline-dotted mb-0">
                                                                    <li class="list-inline-item">All items from</li>
                                                                    <li class="list-inline-item"><a  href= "{{url('details_job_post' ,$d['id'])}}" class=""  data-abc="true" style="color:#F26721;"> <i class="fa fa-share"></i> Details</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="mt-3 mt-lg-3 ml-lg-3 text-center">
                                                                <div class="text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                                                <div class="text-muted">1985 reviews</div> <a href="{{url('details_job_post' ,$d['id'])}}" class="btn mt-4 text-white" style="background:#147fa3;"><i class="fa fa-share mr-2"></i>Apply</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                @endforeach    
                                                
                                            </div>

                                            <!-- loadmore----------------------------------------------------------------------->
                                            <a href="#" id="loadMore">Load More...</a>
                                            <!-- <p class="totop">  -->
                                                <a href="#top" class="text-center">Back to top <i class="fa fa-arrow-up"></i> </a> 
                                            <!-- </p> -->
                                            <!-- loadmore-end----------------------------------------------------------------------->


                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <div class="row">
                                                @php  
                                                $i=0;
                                                @endphp
                                                @foreach($data as $d) 

                                                    <div class="col-md-4 col-sm-12 col-12 mt-1 px-1">
                                                        <div class="card card-body job-list">
                                                            <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                                                <div class="mr-2 mb-3 mb-lg-0 no-gutters"><img class="card-img px-5" src="{{asset('company_logos/'.$d['company_logo'])}} "> </div>
                                                            
                                                                <div class="mt-3 mt-lg-3 ml-lg-3 text-center">
                                                                    <div class="text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                                                    <div class="text-muted">1985 reviews</div>
                                                                </div>
                                                            </div>
                                                            <div class="media-footer mt-4 media-search">
                                                                <p class="media-title font-weight-semibold" style="color: #147fa3;"> <span href="#" data-abc="true" style="text-transform: uppercase !important; color: #F26721;">{{$data[$i]["Job_Title"]}} </span> ( {{$data[$i]["Experience"]}} )</p>
                                                                <ul class="list-inline list-inline-dotted mb-3 mb-lg-2">
                                                                    <li class="list-inline-item"><a href="#" class="text-dark" data-abc="true">{{$data[$i++]["Salary"]}}</a></li>
                                                                </ul>
                                                                <p class="mb-3">Company Hire Developer</p>
                                                                <ul class="list-inline list-inline-dotted mb-0">
                                                                    <li class="list-inline-item">All items from</li>
                                                                    <li class="list-inline-item"><a href= "{{url('details_job_post' ,$d['id'])}}" class=""  data-abc="true" style="color:#F26721;"> <i class="fa fa-share"></i> Details</a></li>
                                                                    <li class="list-inline-item text-light"><a href="{{url('details_job_post' ,$d['id'])}}" class="btn text-light"  data-abc="true" style="background:#147fa3;"> <i class="fa fa-share text-light"></i> Apply</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    
                                                    </div>

                                                @endforeach     
                                            </div>

                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            
                        
                        </div>
                    </div>
                </div>
            </section>
            <!-- ./Tabs -->
             
        </div>
    </div>
</main> 

<!-- search-job------------------------>
<script>
        $(document).ready(function(){
    // $('#search').keyup(function(){
    $('#search').keyup(function(){
    
    // Search text
    var text = $(this).val().toLowerCase();
    
    // Hide all content class element
    $('.job-list').hide();

    // Search 
    $('.job-list .media-search ').each(function(){
    
        if($(this).text().toLowerCase().indexOf(""+text+"") != -1 ){
        $(this).closest('.job-list').show("slow");
        }
    });
    });
    });
</script>

<!-- search-job-end------------------------------------->

<script>

    $(function () {
        $(".readmore").slice(0, 6).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            $(".readmore:hidden").slice(0, 6).slideDown();
            if ($(".readmore:hidden").length == 0) {
                $("#load").fadeOut('slow');
            }
            $('html,body').animate({
                scrollTop: $(this).offset().top
            }, 1500);
        });
    });

    $(document).on("keyup","#search", function(){
        var search_val=$("#search").val();
        console.log(search_val);

        if(search_val)
        {
            $(".readmore").slice(0, 15).show();
            
        }
        else{
            
            $(".readmore").each((i,e)=>{
                console.log(i,e);
                if(i<6){
                $(e).show();
                }else{
                $(e).hide();
            }
            
            })

        }
        
    });

    // search_input_end*******************************************************************

    $('a[href=#top]').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.totop a').fadeIn();
        } else {
            $('.totop a').fadeOut();
        }
    });
   
</script>

<!-- job-filter---------------------------------------->







    
</body>
</html>