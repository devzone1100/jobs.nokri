<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
          .btn_1 {
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 20px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px;
            border:0px solid white !important;
            
        }
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }






    </style>
</head>
<body>



<div class="container-fluid" style="margin-top:85px; padding-left:85px;">
    <form action="">
        <div class="row my-3 px-5">
        
            <div class="col-md-12 px-5">
                <div class="card">
                    <div class="card-header px-5 py-4 text-light" style="background: #147fa3;">
                        Your Profile
                    </div>
                    <div class="row  px-5 pt-3">
                        <div class="col-md-1">
                            <img class="card-img"  id="blah"  style="border-radius: 20px; border: 2px solid #d4c9cf; height: 70px; width: 70px;">
                        </div>
                        <div class="col-md-2">
                            <div class="card-text pt-4"> 
                                <a href="#" type="image" class="btn btn-info text-light">Browse Image</a>
                                <input type="file" id="my_file" style="display: none;" onchange="readURL(this);" />  
                            </div> 
                        </div>
                    </div>
                    <div class="row  px-5 pt-3">
                        <div class="col-md-6">
                        
                            <div class="form-group">
                                <label for="">First Name</label>
                                <input type="text" class="form-control"  placeholder="Your first name">
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control"  placeholder="Your last name">
                            </div>
                            
                        </div>
                    </div>
                    <div class="row px-5 py-1">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" class="form-control"  placeholder="Your Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Phone No</label>
                                <input type="text" class="form-control"  placeholder="Your Phone no">
                            </div>
                        </div>
                    </div>
                    <div class="row px-5 py-3">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn_1 py-1 px-5">Save</button>
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </form>    

</div>

@include('layout.sidemenu')


		










<script>
    $("a[type='image']").click(function() {
          $("input[id='my_file']").click();
      });

      // show image after upload

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah')
                      .attr('src', e.target.result)
                      .width(70)
                      .height(70);
                      
              };

              reader.readAsDataURL(input.files[0]);
          }
      }
  </script>
    
</body>
</html>