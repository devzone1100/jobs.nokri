<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <script src="{{asset('libs/conts.js')}}"></script>

   
    
    <style>

        /* toaster message show */
        #toast-container{position:fixed;z-index:999999;pointer-events:none}
        #toast-container>div{position:relative;pointer-events:auto;overflow:hidden;margin:0 800px 6px;padding:15px 15px 15px 50px;width:100%;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;background-position:15px center;background-repeat:no-repeat;-moz-box-shadow:0 0 12px #999;-webkit-box-shadow:0 0 12px #999;box-shadow:0 0 12px #999;color:#FFF;opacity:.8;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=80);filter:alpha(opacity=80)}

        /* toaster message show end */
       
        .login_class{
            float: left;
            width:100%;
            background-image: url(images/riban_27.jpg);
            height: 100%;
            background-position: center 0;
            background-size: cover;
            position: relative;
            min-height: 610px;
            text-align: center;
            padding-top: 60px;

        }
        .login_form h4 {
            font-size: 26px;
            text-transform: capitalize;
            padding-bottom: 30px;
        }
        .form-group {
            padding-top: 20px;
        }
        .form-control{
            border-radius: 0px;
            height: 40px;
        }
        .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 0px solid black;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .btn_eye {
            cursor: context-menu !important;
            border: 0px solid black;
            background: linear-gradient(to right, #fff 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .form-group a {
            text-decoration: none;
        }
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }

        /* tab */
        .nav-tabs{
            background: linear-gradient(to right, #61b1cc 30%, #F26721 70%) ;
        }
        .nav-link {
            color:#15695e !important;
        }

        /* validation password */

        input.error, textarea.error {
            border: 1px dashed red;
            font-weight: 300;
            color: red;
        }

        label.error.fail-alert {
        /* border: 1px dashed red;
        border-radius: 4px; */
        line-height: 1;
        padding: 2px 0 6px 6px;
        background: #ffe6eb;
        }
        input.valid.success-alert {
        border: 2px solid #4CAF50;
        color: green;
        }

        /* validation password end */

    


   /* data inser loader */
        
        .loader {
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5) !important;
            display: none;
            position: absolute;
            z-index: 1000;
            }

        .loading {
        top: 45%;
        left: 48.5%;
        position: absolute;
        border: 5px solid #ccc;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        border-top-color: #147fa3;
        border-left-color: #F26721;
        border-bottom-color: #147fa3;
        border-right-color: #F26721;
        animation: spin 1s infinite ease-in;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    /* data inser loader End */   





   
  
  
       
    </style>
</head>
<body>
    
        <div class="loader">
            <p class="text-light" style="margin-top:26% !important; margin-left:48% !important">Please wait....</p>
            <div class="loading">
            </div>
        </div>
        
        
    
    <div class="container-fluid text-light" style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
        <div class="row pt-2">
            <div class="col-md-6 col-sm-6 col-6 mx-auto pr-5">
                 <h5 class="text-center pr-5">Admin Sign Up</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-6 pl-5">
                <h6 class="text-center pl-5"><a href="{{url('home')}}" class="text-light" style="text-decoration: none;">Home</a>   /  <a href="signup.html" class="text-light" style="text-decoration: none;">Sign Up</a></h6>  
           </div>
        </div>
    </div> 

    <div class="container py-4">
        <div class="row" style="border: 2px solid rgb(179, 177, 177);">
            <div class="col-md-6 login_class">
                <div>
                    <img src="images/log.png" alt=""><span class="m-3 " style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: black; letter-spacing: 3px; font-weight: 600; font-size: 20px;">Jobs<span style="color: #F26721;">.</span>Nokri</span>
                    <div class="p-5">
                        <div class="form-group px-5">
                            <a href="" class="btn_1 form-control text-light">login with facebook <i class="fa fa-facebook p-2"></i></a>
                        </div>
                        <div class="form-group px-5">
                            <a href="" class="btn_2 form-control text-light ">login with Google <i class="fa fa-google p-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mx-auto">
                <h4 class="text-center py-4">Sign Up</h4>
              
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form method="post" action="{{url('Adminregisterinsret')}}" class="text-dark" id="signup_form">
                           
                            <hr>
                            @csrf
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" placeholder="Enter Email*" name="email" class="form-control">
                                    </div>
                                </div>
                               
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group pt-3 password_parent">
                                        <input type="password" placeholder="Enter Password*" name="password" id="passwordUser"  class="form-control">
                                        <span class="input-group-append bg-light px-2">
                                            <i class="fa fa-eye mt-3" id="togglePasswordUser" style="cursor: pointer;" ></i>
                                        </span>
                                    </div>
                                    <p class="error-text3"></p>
                                </div>
                             
                            </div>
                           
                           
                            <div class="row">
                                <div class="col-md-12 px-5">
                                    <div class="form-group">
                                        <input name="register"   type="submit" class="btn_2 form-control text-light">
                                    
                                    </div>
                                    <div class="account text-center">
                                        <p>You have an acount ?<a href="{{url('Admin-Login')}}" style="text-decoration: none;">&nbsp; Login <i class="fa fa-sign-in"></i></a></p>
                                    </div>
                                </div>
                            </div>
                          
                        </form>   
                    </div>
                    
                </div>
            </div>
           
        </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>

<script>

    @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch(type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;

            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
    @endif
</script>









<script>




    
// password eye-show User-Form**************************

    const togglePasswordUser = document.querySelector('#togglePasswordUser');
    const passwordUser = document.querySelector('#passwordUser');

    togglePasswordUser.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = passwordUser.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordUser.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-low-vision');
    });


    // validater form----------------------
$("#signup_form").validate({
    errorClass: "error fail-alert",
    validClass: "valid success-alert",
    rules:{
    "first_name":{
        required:true,
    },
    "last_name":{
        required:true,
    },
    "email":{
        required:true,
    },
    "password":{
        required:true,
        minlength: 8,
    },
    "user_type":{
        required:true,
        
    },
    
    "confirm":{
        required:true,
        // minlength: 8,
        equalTo: "#passwordUser"
    },
    "phone":{
        required:true,
        number: true,
        minlength: 10,
        maxlength:10
    },
    "gender":{
        required:true,
    }
   
},
errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
        error.appendTo($(element).parent().siblings('.error-text'));
    }else if(element.attr("type") == "radio"){
        error.appendTo($(element).parent().parent(".gender-parent").siblings('.error-text2'));  
    } 
    else if(element.attr("name") == "password"){
        error.appendTo($(element).parent(".password_parent").siblings('.error-text3'));  
    }
    else if(element.attr("name") == "confirm"){
        error.appendTo($(element).parent(".confirm_parent").siblings('.error-text4'));  
    }
     else {
        error.insertAfter(element);
    }
},
messages: {
        
        confirm: {
            equalTo: "Please enter the same Password."
        },

        user_type:{
            required: "Reqiure Checkbox"

        },
        gender: {
            required: "Choose gender"

        }

    },
 
    
    submitHandler: function(form) {
    document.getElementsByClassName("loader")[0].style.display = "block";
    SubmittingForm();
       
  
  }

});

// confirm_parent

$("#signup_form_company").validate({
    errorClass: "error fail-alert",
    validClass: "valid success-alert",
    rules:{
    "first_name":{
        required:true,
    },
    "last_name":{
        required:true,
    },
    "email":{
        required:true,
    },
    "password":{
        required:true,
        minlength: 8,
    },
    "user_type":{
        required:true,
    },
    
    "confirm":{
        required:true,
        minlength: 8,
        equalTo: "#password"
    },
    "phone":{
        required:true,
        number: true,
        minlength: 10,
        maxlength:10
    }
    
   
},
errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
        error.appendTo($(element).parent().siblings('.error-text'));
    } 
    else if(element.attr("name") == "password"){
        error.appendTo($(element).parent(".password_parent").siblings('.error-text3'));  
    }
    else if(element.attr("name") == "confirm"){
        error.appendTo($(element).parent(".confirm_parent").siblings('.error-text4'));  
    }
     else {
        error.insertAfter(element);
    }
},

submitHandler: function(form) {
    document.getElementsByClassName("loader")[0].style.display = "block";
    SubmittingForm();
       
  
  },

    messages: {
        
        confirm: {
            equalTo: "Please enter the same Password."
        },
        user_type:{
            required: "Reqiure Checkbox"

        },

    }
    
    

});

</script>


    
        
    
   

    
    
</body>
</html>