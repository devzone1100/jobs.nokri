
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <style>

         /* toaster message show */
         #toast-container{position:fixed;z-index:999999;pointer-events:none}
        /* #toast-container>div{position:relative;pointer-events:auto;overflow:hidden;margin: auto;  padding:15px 15px 15px 50px;width:100%;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;background-position:15px center;background-repeat:no-repeat;-moz-box-shadow:0 0 12px #999;-webkit-box-shadow:0 0 12px #999;box-shadow:0 0 12px #999;color:#FFF;opacity:.8;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=80);filter:alpha(opacity=80)} */
        #toast-container>div{position: fixed;
        z-index: 999;
        height: 4em;
        width: 100%;
        overflow: show;
        margin: 0 auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        }
        /* toaster message show end */
  

        #loginbox {
            margin-top: 30px;
        }

        #loginbox > div:first-child {        
            padding-bottom: 10px;    
        }

        .iconmelon {
            display: block;
            margin: auto;
        }

        #form > div {
            margin-bottom: 25px;
        }

        #form > div:last-child {
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .panel {    
            background-color: #ffffff;
        }

        .panel-body {
            padding-top: 30px;
            background-color: rgba(255,255,255,.3);
        }

        #particles {
            width: 100%;
            height: 100%;
            overflow: hidden;
            top: 0;                        
            bottom: 0;
            left: 0;
            right: 0;
            position: absolute;
            z-index: -2;
        }

        .iconmelon,
        .im {
        position: relative;
        width: 150px;
        height: 150px;
        display: block;
        fill: #4A575D;
        }

        .iconmelon:after,
        .im:after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        }


        @import url('https://fonts.googleapis.com/css?family=Numans');

        .container{
        height: 100%;
        align-content: center;
        }

        .card{
        height: 345px;
        margin-top: auto;
        margin-bottom: auto;
        width: 400px;
        background-color: rgba(0,0,0,0.5) !important;
        }

        .input-group-prepend span{
        width: 50px;
        background-color: #F26721;
        color: black;
        border:0 !important;
        }

        input:focus{
        outline: 0 0 0 0  !important;
        box-shadow: 0 0 0 0 !important;

        }
        .form-control{
            height:40px;
        }

        .remember{
        color: white;
        }

        .remember input
        {
        width: 20px;
        height: 20px;
        margin-left: 15px;
        margin-right: 5px;
        }

        .login_btn{
        color: black;
        background-color: #F26721;

        }

        .login_btn:hover{
        color: black;
        background-color: #4A575D;
        }

        .links{
        color: white;
        }
        .links:hover{
            color: #CF1F46;
            }

        .links a{
        margin-left: 4px;
        }
        .times:hover{
            background-color: #CF1F46;
        }
       


         /* data inser loader ------------------------*/
         .loader {
        width: 100%;
        height: 500%;
        background: rgba(0, 0, 0, 0.5) !important;
        display: none;
        position: absolute;
        z-index: 1000;
        }
        .loader-text {
        position: fixed;
        z-index: 999;
        height: 0em;
        width: 15em;
        overflow: show;
        margin: auto;
        top: 5%;
        left: 0;
        bottom: 0;
        right: 0;

        }

        .loading {    
        border-radius: 50%;
        z-index: 999;
        height: 2px;
        width: 2px;
        position: fixed;
        margin: auto;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        box-shadow: 10px 0 0 2px #F26721, 7px 7px 0 2px #999, 0 10px 0 2px #999, -7px 7px 0 2px #999, -10px 0 0 2px #999, -7px -7px 0 2px #999, 0 -10px 0 2px #F26721, 7px -7px 0 2px #F26721;
        -webkit-animation: rotate 0.7s steps(8) infinite;
        -o-animation: rotate 0.7s steps(8) infinite;
        animation: rotate 0.7s steps(8) infinite;
  
        }

        @keyframes rotate {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    /* data inser loader End */    


    </style>

</head>
<body  style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">

        <div class="loader">
            <p class="text-light loader-text"> Login Successfully Please wait....</p>
            <div class="loading"> 
            </div>
        </div>

   

        <div class="row"> 
            <div class="col-md-12 mx-auto">
                @if(Session::has('error'))
                    <div class="alert alert-danger text-center">{{Session::get('error')}}</div>
                @endif
                @if(Session::has('message2'))
                    <div class="text-center">
                        <div class="alert alert-success text-center">{{Session::get('message2')}}</div>
                    </div>
                @endif
            </div>
               
        </div>
    
    <div class="container">
        <div style="text-align:center"><img class="mt-2"src="https://i.ibb.co/rxKLQ4S/19-1-User-512.png" alt="photo" style="width:20%"></div>
            <div class="d-flex justify-content-center h-100">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center text-light">Admin Login</h3>
                    </div>
                    <div class="card-body">
                            <p id="loginMsg" class="text-center"></p>
                            @csrf
                            <div class="row px-4">
                                <div class="col-md-12 pr-0">
                                    <div class="input-group form-group ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class='fas fa-user-graduate ml-2' style='font-size:23px'></i></span>
                                            <input type="text" class="form-control form-control-lg" placeholder="Email" id="email" required>
                                        </div>         
                                    </div>
                                </div>
                               
                            </div>
                            
                            <div class="row px-4">
                                <div class="col-md-12 pr-0">
                                    <div class="input-group form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class='fa fa-key ml-2' style='font-size:22px'></i></span>
                                            <input type="password" class="form-control form-control-lg" placeholder="Password" id="password" required>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                     
                            <div class="row align-items-center remember px-4">
                                <input type="checkbox" id="remember_me">Remember me
                            </div>
                            <div class="form-group text-center my-5">
                                <input type="submit" onclick="AdminloginCheck()" value="Login" class="btn px-5 login_btn text-light" id="btnGiris">    
                            </div>
                    
                    </div>
                   
                </div>
            </div>
        </div>
    <div>   


    <script>

        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;
                
                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script> 

    <script src="{{asset('libs/admin_login.js')}}"></script>
        
    
  


</body>
</html>