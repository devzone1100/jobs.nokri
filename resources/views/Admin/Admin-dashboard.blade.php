<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin-Dashboard</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
            .card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5em;
    opacity: 0.2;
    
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
    </style>
</head>
<body>
@include('layout.sidemenu')



<main class="l-main">

      <div class="container-fluid" style="">
      <!-- <h1 class="page-title my-4">Admin Dashboard</h1> -->
          <div class="row">
            <a href="{{url('paymentData')}}">
              <div class="col-md-2">
                <div class="card-counter danger">
                  <i class="fa fa-money"></i>
                  <span class="count-numbers">{{$paymentData}}</span>
                  <span class="count-name">Money</span>
                </div>
              </a>  
          </div>
          

          <div class="col-md-2">
            <a href="{{url('applied-user-data')}}">
              <div class="card-counter primary">
                <i class="fa fa-share"></i>
                <span class="count-numbers">{{$applied_user_data}}</span>
                <span class="count-name">Applied Jobs</span>
              </div>
            </a>  
          </div>

          <div class="col-md-2">
            <a href="{{url('jobpostData')}}">
              <div class="card-counter success">
                <i class="fa fa-user-plus"></i>
                <span class="count-numbers">{{$job_post_data}}</span>
                <span class="count-name">Jobs Post</span>
              </div>
            </a>  
          </div>


          <div class="col-md-2">
            <div class="card-counter info">
              <i class="fa fa-users"></i>
              <span class="count-numbers">{{$Company_register_data}}</span>
              <span class="count-name">Company Registed</span>
            </div>
          </div>

          <div class="col-md-2">
            <div class="card-counter primary">
              <i class="fa fa-users"></i>
              <span class="count-numbers">{{$User_register_data}}</span>
              <span class="count-name">User Registed</span>
            </div>
          </div>

          <div class="col-md-2">
            <a href="{{url('jobpostData')}}">
              <div class="card-counter danger">
                <i class="fa fa-database"></i>
                <span class="count-numbers">{{$register_data}}</span>
                <span class="count-name">Total Register Data</span>
              </div>
            </a> 
          </div>
        </div>
      </div>



    <div class="content-wrapper content-wrapper--with-bg">
      <div class="page-content">
          <!-- content-here -->
            <div class="container-fluid" style="">
                <form action=""  method="" enctype="multipart/form-data">
                    <div class="row my-3 px-0">
                    
                        <div class="col-md-12 px-0">
                            <div class="card">
                                <div class="card-header px-5 py-4 text-light" style="background: #147fa3;">
                                    Your Profile
                                    <i class="fa fa-edit float-right" style="font-size: 18px;" id="btn_enable" type="button"></i>
                                </div>
                                <div class="row px-5 pt-3">
                                @if(Session::has('Adminemail') || Cookie::has('Adminemail'))
                                    <div class="col-md-2">
                                    <img class="card-img "  id="blah" src="{{asset('/profile_images/'.$data->profile_image)}}"   style="border-radius: 20px; height: 70px; width: 70px;">
                                    </div>
                                 @csrf    
                                    
                                    <div class="col-md-2">
                                        <div class="card-text pt-3"> 
                                           <a href="#" type="image" class="btn btn-info text-light">Browse Image</a>
                                            <input type="file" name='pro_img' id="my_file" style="display: none;" onchange="readURL(this);" />  
                                        </div> 
                                    </div>
                                </div>
                                <div class="row  px-5 pt-3">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control" id="first_name" disabled  name="first_name"   placeholder="Your first name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="">Last Name</label>
                                          <input type="text" class="form-control" id="last_name"  disabled name="last_name"   placeholder="Your last name">
                                      </div>  
                                    </div>
                                </div>
                                <div class="row px-5 py-1">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="text" class="form-control"  name="email" disabled  value="{{$data['email']}}"  placeholder="Your Email">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Phone No</label>
                                            <input type="text" class="form-control" id="phone" disabled name="phone" maxlength="10"   placeholder="Your Phone no">
                                        </div>
                                    </div>
                                </div>
                                <div class="row px-5 py-3">
                                    <div class="col-md-4">
                                        <button type="submit" id="save" disabled class="btn btn-primary py-3 px-5">Save Change</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>   
                @endif 

            </div>
      </div>
    </div>
  </main>


  <script> 
    $(document).ready(function() { 
      $("#btn_enable").click(function(){ 
      $("#first_name").prop("disabled", false);
      $("#last_name").prop("disabled", false);
      $("#phone").prop("disabled", false);
      $("#save").prop("disabled", false);
      }); 
    }); 
  </script>





<script>
    $("a[type='image']").click(function() {
          $("input[id='my_file']").click();
      });

      // show image after upload

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah')
                      .attr('src', e.target.result)
                      .width(70)
                      .height(70);
                      
              };

              reader.readAsDataURL(input.files[0]);
          }
      }
  </script>





    
</body>
</html>