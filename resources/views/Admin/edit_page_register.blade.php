<!-- {{$editDetails}} -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UpdateRegisterData</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    

</head>
<body class="bg-secondary">

<div class="conatiner-fluid">
    <div class="">
        <div class="col-md-4 mx-auto bg-light my-4">
        
            <form action="{{url('updateFormregister')}}" method="post">
            <h5 class="card-title text-center pt-2">UpdateForm</h5>
            <hr>
                @csrf
                <input class="form-control" type="hidden" name="id_" value="{{$editDetails['id']}}">
                <label class="form-group">First Name</label>
                <input class="form-control" type="text" name="first_name" value="{{$editDetails['Firstname']}}">
                <label class="form-group">Last Name</label>
                <input class="form-control" type="text" name="last_name" value="{{$editDetails['Lastname']}}">
                <label class="form-group">Email</label>
                <input class="form-control" type="email" name="email" value="{{$editDetails['email']}}">
                <!-- <label class="form-group">Phone</label>
                <input class="form-control" type="text" name="phone" value="{{$editDetails['phone_no']}}"> -->
                <label class="form-group">Password</label>
                <input class="form-control" type="password" name="password" value="{{$editDetails['password']}}">
                <label class="form-group">User Type</label>
                <input class="form-control" type="text" name="user_type" value="{{$editDetails['UserType']}}">
                <!-- <label class="form-group mt-2">Gender</label>
                <input class="form-control" type="text" name="gender" value="{{$editDetails['gender']}}"> -->
                <label class="form-group">Status</label>
                <input class="form-control" type="number" name="status" value="{{$editDetails['status']}}"><br>
                <input class="btn btn-warning my-2" type="submit">
            </form>
        </div>
    </div>
</div>

    
</body>
</html>
  