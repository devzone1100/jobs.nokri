<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage-Applied-User</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,500" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>


  
    <style>
        
        .parent a {
            font-size:15px !important;
        }
        
        .row {
            /* margin-right: -15px;
            margin-left: -15px; */
            margin-top: 20px;
            width:100% !important;
           
        }
        .row .col-sm-6 {
            color:#ffffff !important;
           
        }
        .form-control{
            height:30px !important;
              
        }
        .dataTables_info {
            color:#ffffff !important;
        }
       
 
        table {
        width: 100%;
        border-collapse: collapse;
        }

        tr:nth-of-type(odd) {
        background: #f4f4f4;
        }

        tr:nth-of-type(even) {
        background: #fff;
        }

     

        th {
        /* background: #5cb85c; */
        color: #fff;
        font-weight: 500;
        }

        td,
        th {
        padding: 10px;
        border: 1px solid #ccc;
        text-align: left;
        }

        td:nth-of-type(1) {
        font-weight: 500 !important;
        }

        td {
        font-family: 'Roboto', sans-serif !important;
        font-weight: 300;
        line-height: 20px;
        }

        /* span {
        font-style: italic
        } */

        @media only screen and (max-width: 760px),
        (min-device-width: 768px) and (max-device-width: 1024px) {

        table.responsive,
        .responsive thead,
        .responsive tbody,
        .responsive th,
        .responsive td,
        .responsive tr {
            display: block !important;
        }

        .responsive thead tr {
            position: absolute !important;
            top: -9999px;
            left: -9999px;
        }

        .responsive tr {
            border: 1px solid #ccc;
        }

        .responsive td {
            border: none;
            border-bottom: 1px solid #eee !important;
            position: relative !important;
            padding-left: 25% !important;
        }

        .responsive td:before {
            position: absolute !important;
            top: 6px;
            left: 6px;
            width: 45%;
            padding-right: 10px;
            white-space: nowrap !important;
            font-weight: 500 !important;
        }

        
        .responsive td:before {
            content: attr(data-table-header) !important;
        }
    }
    #sort_info,.dataTables_filter,.dataTables_length{
        color: black !important;
    }
    </style>

  </head>

<body class="bg-secondary">


<div class="container-fluid pr-0" style="margin-top: 60px; padding-left:85px;">
    <div class="row">
        <div class="col-md-12"  style="background:#f8f9fa; border-radius:20px;">
        <!-- <h4 class=" text-center text-light">Register Data </h4> -->
            <!-- <div class="card-header bg-primary text-center" style="font-size:18px ">Register Data </div> -->
                @if(Session::has('error'))
                <div class="alert alert-danger">{{Session::get('error')}}</div>
                @endif
                @if(Session::has('message2'))
                    <div class="alert alert-success text-center">{{Session::get('message2')}}</div>
                @endif
            <table class="table responsive" id="sort">
                <thead>
                <tr class="bg-info" style="font-size: 17px; font-weight:600;">
                    <th>S no.</th>
                    <th>company_email</th>
                    <th>job_id</th>
                    <th>job_title</th>
                    <th>applied_useremail</th>
                    <th>applied_firstname </th>
                    <th>applied_lastname</th>
                    <th>applied_phone</th>
                    <th>Action</th>
                </tr> 
                </thead>
                <tbody>
                    @foreach($data as $d)
                    <tr class="text-dark" style="font-size: 15px;">
                        <td>{{$d->id}}</td>
                        <td>{{$d->company_email}}</td>
                        <td>{{$d->job_id}}</td>
                        <td>{{$d->job_title}}</td>
                        <td>{{$d->applied_useremail}}</td>
                        <td>{{$d->applied_firstname}}</td>
                        <td>{{$d->applied_lastname}}</td>
                        <td>{{$d->applied_phone}}</td>
                        <td class="text-center">
                            <a href= "{{url('edit_applied_user_data' ,$d->id)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                            <a onclick="deleteConfirmation({{$d->id}})" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        </td>
                       
                    </tr>
                    @endforeach           
                    </tbody>
            </table>
        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.0/moment.min.js'></script>


<script>
// Code By Webdevtrick ( https://webdevtrick.com )
    $(document).ready(function() {
    $("#sort").DataTable({
        pageLength : 7,
        lengthMenu: [[7, 10, 20, -1], [7, 10, 20, 50]],
        columnDefs : [
        { type : 'date', targets : [3] }
    ],  
    });
    });
</script>

<script type="text/javascript">
    function deleteConfirmation(id) {
        swal({
            title: "Delete?",
            text: "Please ensure and then confirm!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {

            if (e.value === true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    // type: 'POST',
                    url: "{{url('delete_applied_user_data')}}/" + id,
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    success: function (results) {

                        if (results.success === true) {
                            location.reload();
                            swal("Done!", results.message, "success");
                        } else {
                            swal("Error!", results.message, "error");
                        }
                    }
                });

            } else {
                e.dismiss;
            }

        }, function (dismiss) {
            return false;
        })
    }
</script>







</body>
@include('layout.sidemenu')
</html>