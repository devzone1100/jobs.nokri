{{$editDetails}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job-Post-Update</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body class="bg-secondary">

<div class="conatiner">
    <div class="row">
        <div class="col-md-4 mx-auto bg-light my-4">
        
            <form action="{{url('updateFormjobpost')}}" method="post">
            <h5 class="card-title text-center pt-2">UpdateForm</h5>
            <hr>
                @csrf
                <input class="form-control" type="hidden" name="id_" value="{{$editDetails['id']}}">
                <label class="form-group">Job Category</label>
                <div class="form-group">
                    <select class="form-control" name="job_category" value="{{$editDetails['Job_Category']}}" aria-placeholder="UI/UX Designer"  required>
                        <option>select location</option>
                        <option>california</option>
                        <option>los velas</option>
                        <option>noida</option>
                        <option>chicago</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-group">Job Title</label>
                    <input class="form-control" name="job_title" value="{{$editDetails['Job_Title']}}" placeholder="ex-Designer,Developer" required>
                </div>
                <label class="form-group">Job Type</label>
                <div class="form-group">
                    <select class="form-control" name="job_type" value="{{$editDetails['Job_Type']}}" required>
                        <option>Full Time</option>
                        <option>california</option>
                        <option>los velas</option>
                        <option>noida</option>
                        <option>chicago</option>
                    </select>
                </div>
                <label class="form-group">Working Hours</label>
                <div class="form-group">
                <select class="form-control" name="working_hours" value="{{$editDetails['Working_Hours']}}" required>
                    <option>40/h Week</option>
                    <option>california</option>
                    <option>los velas</option>
                    <option>noida</option>
                    <option>chicago</option>
                </select>
                </div>
                <label class="form-group">Salary</label>
                <div class="form-group">
                    <select class="form-control" name="salary" value="{{$editDetails['Salary']}}" required>
                        <option>15k-20k</option>
                        <option>california</option>
                        <option>los velas</option>
                        <option>noida</option>
                        <option>chicago</option>
                    </select>
                </div>
                <label class="form-group">Experience</label>
                <div class="form-group">
                    <select class="form-control" name="experience" value="{{$editDetails['Experience']}}" required>
                        <option>1 Year Experience</option>
                        <option>california</option>
                        <option>los velas</option>
                        <option>noida</option>
                        <option>chicago</option>
                    </select>
                </div>
                <label class="form-group mt-2">Job Letter</label>
                <input type="text" name="job_letter" value="{{$editDetails['Job_Letter']}}"><br>
                <input class="btn btn-warning my-2" type="submit">
            </form>
        </div>
    </div>
</div>

    
</body>
</html>
  

   