<!-- {{$editDetails}} -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <style>
           /* toaster message show */
           #toast-container{position:fixed;z-index:999999;pointer-events:none}
        /* #toast-container>div{position:relative;pointer-events:auto;overflow:hidden;margin: auto;  padding:15px 15px 15px 50px;width:100%;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;background-position:15px center;background-repeat:no-repeat;-moz-box-shadow:0 0 12px #999;-webkit-box-shadow:0 0 12px #999;box-shadow:0 0 12px #999;color:#FFF;opacity:.8;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=80);filter:alpha(opacity=80)} */
        #toast-container>div{position: fixed;
        z-index: 999;
        height: 4em;
        width: 100%;
        overflow: show;
        margin: 0 auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        }
        /* toaster message show end */
        
        .parent a {
            font-size:15px !important;
        }
    </style>

</head>
<body>

<div class="conatiner">
    <div class="row">
        <div class="col-md-4 mx-auto bg-info my-4">
        
            <form action="{{url('updateForm')}}" method="post">
            <h3 class="text-center">UpdateForm</h3>
                @csrf
                <input class="form-control" type="hidden" name="id_" value="{{$editDetails['id']}}">
                <label class="form-group">Email</label>
                <input class="form-control" type="text" name="email" value="{{$editDetails['email']}}">
                <label class="form-group">Transation ID</label>
                <input class="form-control" type="text" name="transation" value="{{$editDetails['Transation_ID']}}">
                <label class="form-group">User ID</label>
                <input class="form-control" type="text" name="user_id" value="{{$editDetails['User_ID']}}">
                <label class="form-group">Prize</label>
                <input class="form-control" type="text" name="prize" value="{{$editDetails['Prize']}}">
                <label class="form-group">Job ID</label>
                <input class="form-control" type="text" name="job_id" value="{{$editDetails['Job_ID']}}">
                <input class="btn btn-warning my-2" type="submit">
            </form>
        </div>
    </div>
</div>


<script>

@if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
@endif
</script> 

    
</body>
</html>
  