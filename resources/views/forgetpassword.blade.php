<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ResetPassword</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">

    <style>
        body {
            padding: 40px 0;
            background: #F26721 !important;
        }
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
          /* validation password */

          input.error, textarea.error {
            border: 1px dashed red;
            font-weight: 300;
            color: red;
        }

        label.error.fail-alert {
        /* border: 1px dashed red;
        border-radius: 4px; */
        line-height: 1;
        padding: 2px 0 6px 6px;
        background: #ffe6eb;
        }
        input.valid.success-alert {
        border: 2px solid #4CAF50;
        color: green;
        }
  
    </style>    
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card login-form">
                    <div class="card-body">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{Session::get('error')}}</div>
                        @endif
                        @if(Session::has('message2'))
                            <div class="alert alert-success text-center">{{Session::get('message2')}}</div>
                        @endif
                     
                            <h6 class="card-title">Reset Password</h6>
                            <hr>
                  
                        <div class="card-text">
                            <form method="post" action="{{url('forget_password')}}" id="resetpassword_form">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" value="{{$token}}" name="passwordToken" id="passwordToken">
                                        <div class="input-group password_parent">
                                            <label class="input-group">New password.</label>
                                            <input type="password" class="form-control" name="password" id="passwordUser" onChange="onChangeReset()"  placeholder="Enter your new password">
                                            <span class="input-group-append bg-light px-2">
                                                <i class="fa fa-eye mt-2" id="togglePasswordUser" style="cursor: pointer;" ></i>
                                            </span>
                                        </div>
                                        <p class="error-text3"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group confirm_parent">
                                            <label class="input-group">Confirm password.</label>
                                            <input type="password" name="confirmpassword" id="passconfirmUser" onChange="onChangeReset()" class="form-control" placeholder="Re-Eneter password">
                                            <span class="input-group-append bg-light px-2">
                                                <i class="fa fa-eye mt-2" id="togglePasswordUserConfirm" style="cursor: pointer;" ></i>
                                            </span>
                                        </div>
                                        <p class="error-text4"></p>
                                    </div>
                                </div>
                                <button type="submit" name="recoverypassword" class="btn btn-primary btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>



   

    <script>
        // password eye-show reset-password-Form**************************

        const togglePasswordUser = document.querySelector('#togglePasswordUser');
        const password = document.querySelector('#passwordUser');

        togglePasswordUser.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-low-vision');
        });

        const togglePasswordUserConfirm = document.querySelector('#togglePasswordUserConfirm');
        const confirmpassword = document.querySelector('#passconfirmUser');

        togglePasswordUserConfirm.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = confirmpassword.getAttribute('type') === 'password' ? 'text' : 'password';
        confirmpassword.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-low-vision');
        });



     
        // validater form----------------------
        
        $("#resetpassword_form").validate({
            errorClass: "error fail-alert",
            validClass: "valid success-alert",
            rules:{
        
            "password":{
                required:true,
                minlength: 8,
            },
        
            "confirmpassword":{
                required:true,
                minlength: 8,
                equalTo: "#passwordUser"
            },
        
        
        },
            errorPlacement: function (error, element) {
                   
                    if(element.attr("name") == "password"){
                        error.appendTo($(element).parent(".password_parent").siblings('.error-text3'));  
                    }
                    else if(element.attr("name") == "confirmpassword"){
                        error.appendTo($(element).parent(".confirm_parent").siblings('.error-text4'));  
                    }
                    else {
                        error.insertAfter(element);
                    }
                },
                messages: {
                
                confirmpassword: {
                    equalTo: "Please enter the same Password."
                }

            },
        });

    </script>
    
</body>
</html>