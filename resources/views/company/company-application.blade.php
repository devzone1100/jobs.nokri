<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Company-Application</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        
      
        .list-group a{
            text-decoration: none;
        }
        .card-body a {
            text-decoration: none;
        }
        .no-gutters img {
          height:80px !important;
          width:100% !important;
        }

       
        
        
        </style>
        
</head>
<body>
        
@include('layout.Company_sidemenu')   
  
  <main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg">
      <!-- <h5 class="page-title">Dashboard</h5> -->
        <div class="">
            <div class="container-fluid pl-0">
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-12">
                        <div class="card">
                            <div class="card p-3 text-light" style="background: #147fa3;">
                                Recent Applications
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 p-3  col-sm-12 col-12 mx-auto">
                                <div class="card" style="border: 1px solid #d4c9cf;">
                                    <div class="row no-gutters">
                                        <div class="col-md-2 py-3">
                                            <img class="card-img px-5" src="images/riban_27.jpg" alt="Suresh Dasari Card">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-12 text-center my-auto mx-auto">
                                            <div class="card-body py-3">
                                                <h4 class="card-title" href="" style="color: #F26721;">Sohan Sharma</h4>
                                                    <div class="card-text ">
                                                        <p><i class="fa fa-suitcase text-danger"></i>&nbsp;&nbsp;&nbsp;Web Designer<i class="fa fa-map-marker text-danger ml-4"></i> &nbsp;&nbsp;&nbsp;New Delhi</p> 
                                                    </div>     
                                                </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-12 text-center my-auto">
                                            <a href="#" class="btn_1 btn ml-2 px-4 text-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download Info</a>
                                            <a href="#" class="btn btn_2 px-5 my-3 text-light  ml-2"><i class="fa fa-envelope">&nbsp;&nbsp;Send</i></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-md-12 p-3  col-sm-12 col-12 mx-auto">
                                <div class="card" style="border: 1px solid #d4c9cf;">
                                    <div class="row no-gutters">
                                        <div class="col-md-2 py-3">
                                            <img class="card-img px-5" src="images/riban_27.jpg" alt="Suresh Dasari Card">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-12 text-center my-auto mx-auto">
                                            <div class="card-body py-3">
                                                <h4 class="card-title" href="" style="color: #F26721;">Sohan Sharma</h4>
                                                    <div class="card-text ">
                                                        <p><i class="fa fa-suitcase text-danger"></i>&nbsp;&nbsp;&nbsp;Web Designer<i class="fa fa-map-marker text-danger ml-4"></i> &nbsp;&nbsp;&nbsp;New Delhi</p> 
                                                    </div>     
                                                </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-12 text-center my-auto">
                                            <a href="#" class="btn btn_1 ml-2 px-4 text-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download Info</a>
                                            <a href="#" class="btn btn_2 px-5 my-3 text-light  ml-2"><i class="fa fa-envelope">&nbsp;&nbsp;Send</i></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-md-12 p-3  col-sm-12 col-12 mx-auto">
                                <div class="card" style="border: 1px solid #d4c9cf;">
                                    <div class="row no-gutters">
                                        <div class="col-md-2 py-3">
                                            <img class="card-img px-5" src="images/riban_27.jpg" alt="Suresh Dasari Card">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-12 text-center my-auto mx-auto">
                                            <div class="card-body py-3">
                                                <h4 class="card-title" href="" style="color: #F26721;">Sohan Sharma</h4>
                                                    <div class="card-text ">
                                                        <p><i class="fa fa-suitcase text-danger"></i>&nbsp;&nbsp;&nbsp;Web Designer<i class="fa fa-map-marker text-danger ml-4"></i> &nbsp;&nbsp;&nbsp;New Delhi</p> 
                                                    </div>     
                                                </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-12 text-center my-auto">
                                            <a href="#" class="btn btn_1 ml-2 px-4 text-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download Info</a>
                                            <a href="#" class="btn btn_2 px-5 my-3 text-light  ml-2"><i class="fa fa-envelope">&nbsp;&nbsp;Send</i></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-md-12 p-3  col-sm-12 col-12 mx-auto">
                                <div class="card" style="border: 1px solid #d4c9cf;">
                                    <div class="row no-gutters">
                                        <div class="col-md-2 py-3">
                                            <img class="card-img px-5" src="images/riban_27.jpg" alt="Suresh Dasari Card">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-12 text-center my-auto mx-auto">
                                            <div class="card-body py-3">
                                                <h4 class="card-title" href="" style="color: #F26721;">Sohan Sharma</h4>
                                                    <div class="card-text ">
                                                        <p><i class="fa fa-suitcase text-danger"></i>&nbsp;&nbsp;&nbsp;Web Designer<i class="fa fa-map-marker text-danger ml-4"></i> &nbsp;&nbsp;&nbsp;New Delhi</p> 
                                                    </div>     
                                                </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-12 text-center my-auto">
                                            <a href="#" class="btn btn_1 ml-2 px-4 text-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download Info</a>
                                            <a href="#" class="btn btn_2 px-5 my-3 text-light  ml-2"><i class="fa fa-envelope">&nbsp;&nbsp;Send</i></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div class="col-md-12 p-3  col-sm-12 col-12 mx-auto">
                                <div class="card" style="border: 1px solid #d4c9cf;">
                                    <div class="row no-gutters">
                                        <div class="col-md-2 py-3">
                                            <img class="card-img px-5" src="images/riban_27.jpg" alt="Suresh Dasari Card">
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-12 text-center my-auto mx-auto">
                                            <div class="card-body py-3">
                                                <h4 class="card-title" href="" style="color: #F26721;">Sohan Sharma</h4>
                                                    <div class="card-text ">
                                                        <p><i class="fa fa-suitcase text-danger"></i>&nbsp;&nbsp;&nbsp;Web Designer<i class="fa fa-map-marker text-danger ml-4"></i> &nbsp;&nbsp;&nbsp;New Delhi</p> 
                                                    </div>     
                                                </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-12 text-center my-auto">
                                            <a href="#" class="btn btn_1  ml-2 px-4 text-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download Info</a>
                                            <a href="#" class="btn btn_2 px-5 my-3 text-light  ml-2"><i class="fa fa-envelope">&nbsp;&nbsp;Send</i></a> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layout.footer')
    </div>
  </main>


    
</body>
</html>