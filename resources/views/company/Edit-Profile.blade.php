<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit-Profile</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">


    <style>

        .form-control:focus {
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
            }
            .btn:focus{
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
            }
            .form-control{
                border-radius:0;
            }
            .form-group{
                padding-top: 15px;
            }
         
        .btn_2 {
            border: 0px solid black;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .list-group a{
            text-decoration: none;
        }
        .card-body a {
            text-decoration: none;
        }
        
        
        </style>
        
</head>
<body>
        
@include('layout.Company_sidemenu')
  <main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg">
      <!-- <h5 class="page-title">Dashboard</h5> -->
        <div class="page-content p-0 py-1" style="background: #eee;">
            <div class="">
                <div class="container-fluid py-4">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12 mx-auto">
                                    <div class="card" style="border: 1px solid #d4c9cf;">
                                        <div class="row no-gutters">
                                            <div class="col-md-5 p-5">
                                               <div class="pl-5"><img class="card-img"  id="blah"  style="border-radius: 20px; border: 2px solid #d4c9cf; height: 80px; width: 80px;"></div>
                                            </div>
                                            <div class="col-md-4 col-sm-12 col-12">
                                                <div class="card-body">
                                                    <h4 class="card-title text-center" style="color: #F26721;">JPEG Or PNG 500x500px Thumbnail</h4>
                                                        <div class="card-text pt-4"> 
                                                            <a href="#" type="image" class="btn btn_8  text-light form-control">Browse Image</a>
                                                            <input type="file" id="my_file" style="display: none;" onchange="readURL(this);" />  
                                                        </div>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card px-5 py-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">@Username</label>
                                            <input type="text" class="form-control"  placeholder="Luca Wallace">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" class="form-control"  placeholder="luca@gmail.com">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Phone</label>
                                            <input type="number" class="form-control"  placeholder="+91 9569070818">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Website</label>
                                            <input type="email" class="form-control"  placeholder="www.webstrot.com">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Job Description</label>
                                            <select class="form-control">
                                                <option >It & Computer</option>
                                                <option>Marketing</option>
                                                <option>Mechanical</option>
                                                <option>Doctor</option>
                                            </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Address</label>
                                            <input type="text" class="form-control"  placeholder="124/A kalani Bagh">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Country</label>
                                            <input type="text" class="form-control"  placeholder="India">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">State</label>
                                            <select class="form-control">
                                                <option >Madhya Pradesh</option>
                                                <option>Uttar Pradesh</option>
                                                <option>Himachal Pradesh</option>
                                                <option>Punjab</option>
                                            </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">City</label>
                                            <input type="text" class="form-control"  placeholder="Dewas">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form>
                                            <div class="form-group">
                                            <label for="">Zip Code</label>
                                            <select class="form-control">
                                                <option >455001</option>
                                                <option>422501</option>
                                                <option>45310</option>
                                                <option></option>
                                            </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3 ">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header px-5 text-light" style="background: #147fa3;">
                                            Social Networks
                                        </div>
                                        <div class="row  px-5 pt-3">
                                            <div class="col-md-6">
                                                <form>
                                                    <div class="form-group">
                                                    <label for="">Google</label>
                                                    <input type="text" class="form-control"  placeholder="https://google.com/webstrot">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                                <form>
                                                    <div class="form-group">
                                                    <label for="">Facebook</label>
                                                    <input type="text" class="form-control"  placeholder="https://facebook.com/webstrot">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row px-5 py-1">
                                            <div class="col-md-6">
                                                <form>
                                                    <div class="form-group">
                                                    <label for="">Twitter</label>
                                                    <input type="text" class="form-control"  placeholder="https://twitter.com/webstrot">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                                <form>
                                                    <div class="form-group">
                                                    <label for="">Linkedin</label>
                                                    <input type="text" class="form-control"  placeholder="https://linkedin.com/webstrot">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div id="show-more-content_2">
                                        <div class="row px-5 py-1">
                                            <div class="col-md-6">
                                                <form>
                                                    <div class="form-group">
                                                    <label for="">Instagram</label>
                                                    <input type="text" class="form-control"  placeholder="https://instagram.com/webstrot">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-6">
                                                <form>
                                                    <div class="form-group">
                                                    <label for="">YouTube</label>
                                                    <input type="text" class="form-control"  placeholder="https://youtube.com/webstrot">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row px-5 pb-4">
                                            <div id="show-more" class="col-md-3">
                                                <a href="javascript:void(0)" class="btn btn_2 btn-md btn-success form-control ml-1">Add More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row px-5">
                                <div class="col-md-12">
                                    <form>
                                        <div class="form-group ">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <label class="form-check-label mx-5" for="">
                                            Enable Two Step Verification By Email
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row px-5 pb-5">
                                <div class="col-md-3">
                                    <button class="btn btn_2 form-control ml-1">Save Change</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layout.footer')
            </div>
        </div>        
    </div>
   

  </main>


  

  <script>
    $("a[type='image']").click(function() {
          $("input[id='my_file']").click();
      });

      // show image after upload

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah')
                      .attr('src', e.target.result)
                      .width(80)
                      .height(80);
                      
              };

              reader.readAsDataURL(input.files[0]);
          }
      }
  </script>

  <script>
     // readmore
     $('#show-more-content_2').hide();

$('#show-more').click(function(){
    $('#show-more-content_2').show(300);
    $('#show-less').show();
    $('#show-more').hide();
});

$('#show-less').click(function(){
    $('#show-more-content_2').hide(150);
    $('#show-more').show();
    $(this).hide();
});
  </script>

  
    
</body>
</html>