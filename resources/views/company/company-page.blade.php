<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Company-Page</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
 


    <style>
        
        .form-control:focus {
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
            }
            .btn:focus{
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
            }
            .modal-body label{
                padding-top: 8px;
            }
            .form-control{
                border-radius:0;
            }
            .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px; 
            color: #ffff !important;
            transition: all .6s ease-out;
            border-radius: 0px;
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 0px solid black;
            background: linear-gradient(to right,#147fa3 50%,#F26721 50% );
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            color: #ffff !important;
            transition: all .6s ease-out;
            border-radius: 0px;
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .btn_2 a {
            text-decoration: none;
        }
        .list-group a {
            text-decoration: none;
        }
        .form-control{
            border-radius: 0px;
            height: 45px;
        }
        .modal-title {
            padding-left: 10px;
        }
        </style>
        
</head>
<body>
        
 @include('layout.Company_sidemenu')
  <main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg">
      <!-- <h5 class="page-title">Dashboard</h5> -->
        <div class="page-content p-0 py-1" style="background: #eee;">
            <div class="">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header text-light" style="background:#147fa3">
                                            Basic Information
                                            <i class="fa fa-edit float-right" type="button" data-toggle="modal" data-target="#myModal_2"></i>
                                        </div>
                                        <div class="row my-3">
                                            <div class="col-md-6">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-calendar pt-2 px-2"></i>&nbsp;Category:<ul><li style="list-style: none; color: rgb(113, 114, 116);">Design & Creative</li></ul></li>
                                                </ul>    
                                            </div>
                                            <div class="col-md-6">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item text-dark text-left pl-4" style="font-size:18px;"><i class="fa fa-envelope pt-2 px-2"></i>&nbsp;Email:<ul><li style="list-style: none; color: rgb(113, 114, 116);">webstrot@example.com</li></ul></li>
                                                </ul>    
                                            </div>
                                        </div>
                                        <div class="row my-3">
                                            <div class="col-md-6">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-map-marker pt-2 px-2"></i>&nbsp;Location:<ul><li style="list-style: none; color: rgb(113, 114, 116);">Los Angeles Califonia PO</li></ul></li>
                                                </ul>    
                                            </div>
                                            <div class="col-md-6">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-user pt-2 px-2"></i>&nbsp;Compant Size:<ul><li style="list-style: none; color: rgb(113, 114, 116);">20-50</li></ul></li>
                                                </ul>    
                                            </div>
                                        </div>
                                        <div class="row my-3">
                                            <div class="col-md-6">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-info-circle pt-2 px-2"></i>&nbsp;Hotline:<ul><li style="list-style: none; color: rgb(113, 114, 116);">0145636941</li></ul></li>
                                                </ul>    
                                            </div>
                                            <div class="col-md-6">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item text-dark text-left pl-4" style="font-size:18px"><i class="fa fa-globe pt-2 px-2"></i>&nbsp;Website:<ul><li style="list-style: none; color: rgb(113, 114, 116);">www.webstrot.com</li></ul></li>
                                                </ul>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header text-light" style="background:#147fa3">
                                            About-Us
                                            <i class="fa fa-edit float-right" type="button" data-toggle="modal" data-target="#myModal_2"></i>
                                        </div>
                                        <div class="row my-3 p-4">
                                            <div class="col-md-12">
                                            <p>Google is and always will be an engineering company. We hire people with a broad set of icalskills who are ready to tackle some of technology's greatest challenges and make an impact on milions, if not billions, of users. At Google, engineers not only revolutionize search, they routinely work on massive scalability and storage solutions,</p>
                                            <p>large-scale applications and rely new platforms for developers around the world. From AdWords to rome, Android to Ye, Social to Local, Google engineers are changing the world. From AdWords to Chrome, Aoid to Ye, Social to.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header text-light" style="background:#147fa3">
                                            Intro Video
                                            <i class="fa fa-edit float-right" type="button" data-toggle="modal" data-target="#myModal_3"></i>
                                        </div>
                                        <div class="row my-3 p-4">
                                            <div class="col-md-8">
                                                <form>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control"  placeholder="Youtube url or Browse video">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-3">
                                                <form>
                                                    <div class="form-group">
                                                        <button type="video" class=" btn_2 form-control ml-1">Browse Video</button>
                                                        <input type="file" id="my_file_mp4" style="display: none;" />
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header text-light" style="background:#147fa3">
                                            Image Gallery
                                            <i class="fa fa-edit float-right" type="button" data-toggle="modal" data-target="#myModal_4"></i>
                                        </div>
                                        <div class="row my-3 p-3">
                                            <div class="col-md-2">
                                                <img src='images/gallery1.jpg' alt=''>
                                            </div>
                                            <div class="col-md-2">
                                                <img src='images/gallery2.jpg' alt=''>
                                            </div>
                                            <div class="col-md-2">
                                                <img src='images/gallery3.jpg' alt=''>
                                            </div>
                                            <div class="col-md-6">
                                                <img src='images/gallery2.jpg' alt=''>
                                            </div>
                                        </div>
                                        <div class="row my-3">
                                            <div class="col-md-3 px-5">
                                                <div class="mb-2">
                                                    <img id="blah">
                                                </div>
                                                
                                                <button type="image" class=" btn_2 form-control ml-1">Add Image</button>
                                                <input type="file" id="my_file" style="display: none;" onchange="readURL(this);" />
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 px-5 ml-2">
                                    <button class="btn_1 form-control mr-2">Save Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('layout.footer')
                </div>
            
                <!-- modal open for editing -->
            
                <div class="modal fade" id="myModal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fa fa-edit"></i>Basic Information</h5>
                                    <button type="button" class="close text-danger" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="row px-2">
                                        <div class="col-md-12">
                                            <form class="form">
                                                <label for="">Category:</label>
                                                <input type="text" class="form-control" placeholder="Design & Creative">
                                                <label for="pwd">Email:</label>
                                                <input type="email" class="form-control" placeholder="Email">
                                                <label for="text">Location:</label>
                                                <input type="text" class="form-control" placeholder="Los Angeles Califonia PO" >
                                                <label for="">Compant Size:</label>
                                                <input type="number" class="form-control" placeholder="20-50">
                                                <label for="">Hotline:</label>
                                                <input type="number" class="form-control" placeholder="0145636941">
                                                <label for=""> Website:</label>
                                                <input type="text" class="form-control" placeholder="www.webstrot.com" id="pwd">
                                                
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
            
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn_2">Save Update</button>
                                    <button type="button" class="btn btn_1 px-3" data-dismiss="modal">Close</button>
                                </div>
                        </div>
                    </div>
                </div>
            
                <div class="modal fade" id="myModal_2">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fa fa-edit"></i>About Us</h5>
                                    <button type="button" class="close text-danger" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="row px-2">
                                        <div class="col-md-12">
                                            <form class="form">
                                                <label for="">Write Yourself:</label>
                                                <textarea type="text" class="form-control"></textarea>
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
            
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn_2">Save Update</button>
                                    <button type="button" class="btn btn_1 px-3" data-dismiss="modal">Close</button>
                                </div>
                        </div>
                    </div>
                </div>
            
                <div class="modal fade" id="myModal_3">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fa fa-edit"></i>Intro Video</h5>
                                    <button type="button" class="close text-danger" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="row px-2">
                                        <div class="col-md-12">
                                            <form class="form">
                                                <label for="">Video Link:</label>
                                                <input type="text" class="form-control" placeholder="youtube url or browse video">
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
            
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn_2">Save Update</button>
                                    <button type="button" class="btn btn_1 px-3" data-dismiss="modal">Close</button>
                                </div>
                        </div>
                    </div>
                </div>
            
                <div class="modal fade" id="myModal_4">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="fa fa-edit"></i>Gallery</h5>
                                    <button type="button" class="close text-danger" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="row px-2">
                                        <div class="col-md-12">
                                            <form class="form">
                                                <label for="">Gallery:</label>
                                                <input type="text" class="form-control" placeholder="Title">
                                                <input type="text" class="form-control mt-3" placeholder="Link">
                                                <label for="">Image</label>
                                                <input type="file" class="form-control">
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
            
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn_2">Save Update</button>
                                    <button type="button" class="btn btn_1 px-3" data-dismiss="modal">Close</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
  </main>


        
 

<script>
    // Browse file button

  $("button[type='image']").click(function() {
          $("input[id='my_file']").click();
      });
      $("button[type='video']").click(function() {
          $("input[id='my_file_mp4']").click();
      });

      // show image after upload

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah')
                      .attr('src', e.target.result)
                      .width(145)
                      .height(150);
                      
              };

              reader.readAsDataURL(input.files[0]);
          }
      }
</script>

</body>
</html>