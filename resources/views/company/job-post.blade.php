<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Job-Post</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">

    

    <style>

       

        .job_title{
            text-transform: capitalize;
        }
        
        /* .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        } */
        .btn:focus{
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .form-control{
            border-radius: 0px;
            height: 45px;
        }
        option {
            font-size:17px;
        }
        
        .list-group a {
            text-decoration: none;
        }
        .list-group a li i {
            color: #1288af;
        }
        .map-container-6{
            overflow:hidden;
            padding-bottom:56.25%;
            position:relative;
            height:0;
        }
        .map-container-6 iframe{
            left:0;
            top:0;
            height:100%;
            width:100%;
            position:absolute;
        }

         /* data inser loader ------------------------*/
            .loader {
        width: 100%;
        height: 500%;
        /* background: rgba(0, 0, 0, 0.5) !important; */
        display: none;
        position: absolute;
        z-index: 1000;
        }
        .loader-text {
        position: fixed;
        z-index: 999;
        height: 0em;
        width: 7em;
        overflow: show;
        margin: auto;
        top: 5%;
        left: 0;
        bottom: 0;
        right: 0;

        }

        .loading {

            border-radius: 50%;
            z-index: 999;
            height: 2px;
            width: 2px;
            position: fixed;
            margin: auto;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            box-shadow: 10px 0 0 2px #F26721, 7px 7px 0 2px #999, 0 10px 0 2px #999, -7px 7px 0 2px #999, -10px 0 0 2px #999, -7px -7px 0 2px #999, 0 -10px 0 2px #F26721, 7px -7px 0 2px #F26721;
            -webkit-animation: rotate 0.7s steps(8) infinite;
            -o-animation: rotate 0.7s steps(8) infinite;
            animation: rotate 0.7s steps(8) infinite;
  
        }

        @keyframes rotate {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
        /* data inser loader End */ 

       /* validation password */

        input.error, textarea.error {
            border: 1px dashed red;
            font-weight: 300;
            color: red;
        }

        select.error, textarea.error {
            border: 1px dashed red;
            
        }
        select.valid.success-alert {
        border: 2px solid #4CAF50;
        color: green;
        }

        label.error.fail-alert {
        /* border: 1px dashed red;
        border-radius: 4px; */
        line-height: 1;
        padding: 2px 0 6px 6px;
        color:red;
        /* background: #ffe6eb; */
        }
        input.valid.success-alert {
        border: 2px solid #4CAF50;
        color: green;
        }

        /* validation password end */
     
        
        
    </style>
        
</head>
<body>
        <?php
        // print_r($location_data); die;
        ?>

        <div class="loader">
        <p class="text-info loader-text">&nbsp; Please wait....</p>
        <div class="loading"> 
        </div>
        </div>

@include('layout.Company_sidemenu')      
  
  <main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg">
      <!-- <h5 class="page-title">Dashboard</h5> -->
        <div class="page-content p-0 py-1" style="background: #eee;">
            <!-- {{Session::get('useremail')}} -->
                <div class="">

                   

                    <div class="container-fluid py-4">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12">

                                @if(Session::has('useremail') || Cookie::has('useremail'))


                                <!-- {{Session::get('userid')}} -->
                                
                                @php
                                    $email= !empty(Session::has('useremail'))? Session::get('useremail'): Cookie::get('useremail');
                                    $company_id= !empty(Session::has('userid'))? Session::get('userid'): Cookie::get('userid'); 

                                @endphp
                            
                                <form method="post" action="{{url('jobpostinsret')}}" enctype="multipart/form-data" id="jobpost_form" name="generate_jobID" >
                                   @csrf 
                                    <div class="card  border-info">
                                        

                                        <input class="form-control" type="hidden" name="company_email" value="{{$email}}">
                                        <input class="form-control" type="hidden" name="company_id" value="{{$company_id}}">


                                        <div class="form-group py-4 px-4 text-light" style="background:#147fa3">Post New Job</div>
                                        <div class="p-4 pb-5">
                                        
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-group">Job Category <span class="text-danger">*</span></label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="job_category" >
                                                            <option selected  value="">Choose..</option>
                                                            <option>Software Engineer</option>
                                                            <option>Manager</option>
                                                            <option>Hardware</option>
                                                            <option>Electrical</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-group">Job Title <span class="text-danger">*</span> </label>
                                                        <input class="form-control" name="job_title" placeholder="ex-Designer,Developer" style='text-transform:uppercase' >
                                                    </div>
                                                </div>
                                            </div>
                                            
                        
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-group">Job Type <span class="text-danger">*</span></label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="job_type" >
                                                            <option selected value="">Choose...</option>
                                                            <option>Full Time</option>
                                                            <option>Part Time</option>
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-group">Working Hours <span class="text-danger">*</span></label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="working_hours">
                                                            <option selected value="">Choose...</option>
                                                            <option>40/h Week</option>
                                                            <option>60/h Week</option>
                                                            <option>70/h Week</option>
                                                            <option>120/h Week</option>
                                                            <option>140/h Week</option>
                                                            <option>150/h Week</option>
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                        
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-group">Salary</label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="salary">
                                                            <option selected value="">Choose...</option>
                                                            <option>15k-20k</option>
                                                            <option>20k-25k</option>
                                                            <option>25k-30k</option>
                                                            <option>30k-35k</option>
                                                            <option>35k-40k</option>
                                                            <option>45k-50k</option>
                                                           
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-group">Experience <span class="text-danger">*</span></label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="experience" >
                                                            <option selected value="">Choose...</option>
                                                            <option>0-1 Year</option>
                                                            <option>1-2 Year</option>
                                                            <option>2-3 Year</option>
                                                            <option>4-5 Year</option>
                                                            <option>5-6 Year</option>
                                                            <option>6+  Year</option>
                                
                                                         
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                        
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-group" for="company_logo" >Company Logo <span class="text-danger">*</span></label>
                                                        <div class="row">
                                                            <div class="col-md-10 pr-0">
                                                                <input class="form-control" accept="image/*" name="company_logo" type='file' id="profile_img"/>&nbsp;
                                                            </div>
                                                            <div class="pl-2 mt-2">
                                                                <span><button class="btn" style="border-radius: 35px;" data-toggle="modal" data-target="#myModal_img" type="button" value="Preview" onclick="PreviewImage();"><i class="fa fa-eye"></i>&nbsp; Preview</button></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    

                                                </div>
                                            
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label class="form-group" for="upload_file" >Job Letter <span class="text-danger">*</span></label>
                                                        <div class="row">
                                                            <div class="col-md-10 pr-0">
                                                                <input class="form-control" name="job_letter" id="uploadPDF" type="file" name="file"/>
                                                            </div>
                                                            <div class="pl-2 mt-2">
                                                                <span><button class="btn" style="border-radius: 35px;" data-toggle="modal" data-target="#myModal" type="button" value="Preview" onclick="PreviewFile();"><i class="fa fa-eye"></i>&nbsp; Preview</button></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>

                                            <!--****************** preview image****************************** -->
                
                                            <div class="modal" id="myModal_img">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                    
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Preview Logo</h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        <div style="clear:both">
                                                            <div class="text-center" ><img id="blah" src="#" alt="your image" height="100px" width="100px" style="border: 3px solid #eee;" /></div>
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                    
                                                    
                                                    </div>
                                                </div>
                                            </div>
                
                                            <!--****************** preview image end****************************** -->

                
                                            <!--****************** preview file****************************** -->
                
                                            <div class="modal" id="myModal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                    
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Preview Document</h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        <div style="clear:both">
                                                            <iframe id="viewer"   frameborder="0" width="100%" height="100%"></iframe>
                                                        </div>
                                                    </div>
                                                    
                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                
                                            <!--****************** preview file end****************************** -->


                                        </div>
                                        
                                    </div>
                                       
                    
                                    <div class="py-4">
                                        <div class="card  border-info">
                                        
                                            <div class="form-group py-4 px-4 text-light" style="background:#147fa3;">About This Job <span class="text-light">*</span></div>
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <textarea class="form-control btn-outline-light" style="height: 220px;" name="about_job" ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                    
                                    <div class="py-4">
                                        <div class="card  border-info">
                                        
                                            <div class="form-group py-4 px-4 text-light" style="background:#147fa3;">Trending Keywords <span class="text-light">*</span></div>
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group px-4">
                                                            <input class="form-control" placeholder="Type Keyword" style="height: 55px;" name="trending_key">
                                                            <p class=""><i class="fa fa-tag p-3 text-info"></i> Trending Keywords : ui designer, developer, seniorit company, design, call center</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div>
                    
                                    <div class="card  border-info">
                                        
                                        <div class="form-group py-4 px-4 text-light" style="background:#147fa3;">Address / Location </div>
                                        <div class="p-4 pb-5">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-group">Country <span class="text-danger">*</span></label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="country">
                                                            <option selected value="">Choose...</option>
                                                            <option>India</option>
                                                            <option>Nepal</option>
                                                            <option>America</option>
                                                            <option>Bangladesh</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-group">City <span class="text-danger">*</span></label>
                                                    <div class="form-group">
                                                        <select class="form-control" name="city" >
                                                            <option selected value="">Choose...</option>
                                                            <option>Delhi</option>
                                                            <option>Haryana</option>
                                                            <option>Uttar Pardesh</option>
                                                            <option>Bihar</option>
                                                            <option>Punjab</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                        
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label class="form-group">Full Address <span class="text-light">*</span></label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="full_address">
                                                    </div>
                                                </div>
                                            </div>
                        
                                            <div class="row px-4">
                                               <p class="alert text-success">Note - Blow details are autometic generate of your live location !!</p>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-group">Latitude</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="latitude"  value="{{$location_data->lat}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-group">Longitude</label>
                                                        <input type="text" class="form-control" name="longitude"  value="{{$location_data->lon}}" >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-group">TimeZone</label>
                                                        <input type="text" class="form-control" name="timezone"  value="{{$location_data->timezone}}" >
                                                    </div>
                                                </div>
                                            </div>
                
                                            <div class="row p-4">
                                                <div class="col-md-12 col-sm-12 col-12 p-0" style="background-color: #e2e2e2;">
                                                    <!-- <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 610px">
                                                        <iframe src="https://maps.google.com/maps?q=new%20delphi&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                        frameborder="0" style="border:0" allowfullscreen>
                                                        </iframe>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div> 

                                    
                                    </div>
                    
                                    <div class="row py-3">
                                        <div class="col-md-3">
                                            <input type="text" name="job_id" hidden >
                                            <input name="jobpost" type="submit" value="Post Job" class="btn ml-3 px-5 form-control text-light" style="background:#F26721"  onClick='generate()';>
                                        </div> 
                                      
                                    </div>
                                    @endif 
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
        </div>  
        
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>

        @include('layout.footer')
    </div>
  </main>


    <script type="text/javascript"> 
        // random-number-generate--------------------==
        function generate(){
        var my_num= Math.floor(Math.random() * 10000);
        document.generate_jobID.job_id.value=('Job_ID'+ my_num);
        }
        // random-number-generat-ende--------------------==
    </script>
        


    <script>
        // preview file upload**********************
        function PreviewFile() {
            // alert();
            pdffile=document.getElementById("uploadPDF").files[0];
            pdffile_url=URL.createObjectURL(pdffile);
            $('#viewer').attr('src',pdffile_url);
        }
        // preview file upload end**********************
    </script>

 

    <script>
        // preview image upload end**********************
        profile_img.onchange = evt => {
            const [file] = profile_img.files
            if (file) {
                blah.src = URL.createObjectURL(file)
            }
        }
        // preview image upload end**********************
    </script>

    <script>
           // validater form----------------------
$("#jobpost_form").validate({
    errorClass: "error fail-alert",
    validClass: "valid success-alert",
    rules:{
    "job_category":{
        required:true,
    },
    "job_title":{
        required:true,
    },
    "job_type":{
        required:true,
    },
    "working_hours":{
        required:true,
       
    },
    "salary":{
        required:true,
        
    },
    
    "experience":{
        required:true,
       
    },
    "about_job":{
        required:true,
      
    },
    "trending_key":{
        required:true,
    },
    "full_address":{
        required:true,
    
    },
    "country":{
        required:true,
    
    },
    "city":{
        required:true,
    }
   
},

    
    submitHandler: function(form) {
    document.getElementsByClassName("loader")[0].style.display = "block";
    SubmittingForm();
       
  
  }

});
    </script>
    
</body>
</html>