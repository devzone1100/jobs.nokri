<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

@include('layout.Company_sidemenu')


<main class="l-main">
    <div class="content-wrapper content-wrapper--with-bg"> 
        <div class="container-fluid" style="">
            <form action="{{url('profile_image_update')}}"  method="post" enctype="multipart/form-data">
                <div class="row my-3">
                
                    <div class="col-md-12 ">
                        <div class="card">
                            <div class="card-header px-5 py-4 text-light" style="background: #147fa3;">
                                Your Profile <span class="px-3"> ( Join &nbsp; - &nbsp; {{$data->created_at->format('D , d M y') }} )</span>
                                <i class="fa fa-edit float-right" style="font-size: 18px;" id="btn_enable" type="button"></i>
                            </div>
                            <div class="row px-5 pt-3">
                            @if(Session::has('useremail') || Cookie::has('useremail'))
                                <div class="col-md-2">
                                <img class="card-img "  id="blah" src="{{asset('/profile_images/'.$data->profile_image)}}"   style="border-radius: 20px; border: 2px solid #eee !important;  height: 70px; width: 70px;">
                                </div>
                                @csrf    
                                
                                <div class="col-md-2">
                                    <div class="card-text pt-3"> 
                                        <a href="#" type="image" class="btn btn-info text-light">Browse Image</a>
                                        <input type="file" name='pro_img' id="my_file" style="display: none;" onchange="readURL(this);" />  
                                    </div> 
                                </div>
                            </div>
                            <div class="row  px-5 pt-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">First Name</label>
                                        <input type="text" class="form-control" id="first_name" disabled  name="first_name" value="{{$data['Firstname']}}"  placeholder="Your first name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Last Name</label>
                                        <input type="text" class="form-control" id="last_name"  disabled name="last_name"  value="{{$data['Lastname']}}"  placeholder="Your last name">
                                    </div>  
                                </div>
                            </div>
                            <div class="row px-5 py-1">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" class="form-control"  name="email" disabled  value="{{$data['email']}}"  placeholder="Your Email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Phone No</label>
                                        <input type="text" class="form-control" id="phone" disabled name="phone" maxlength="10" value="{{$data['phone_no']}}"  placeholder="Your Phone no">
                                    </div>
                                </div>
                            </div>
                            <div class="row px-5 py-3">
                                <div class="col-md-4">
                                    <button type="submit" id="save" disabled class="btn btn-primary py-3 px-5">Save Change</button><span class="px-3 ">  Last Update &nbsp; - &nbsp; {{$data->updated_at->format('D , d M y') }} </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>   
            @endif 

        </div>
    </div>
</main>

<script> 
    $(document).ready(function() { 
      $("#btn_enable").click(function(){ 
      $("#first_name").prop("disabled", false);
      $("#last_name").prop("disabled", false);
      $("#phone").prop("disabled", false);
      $("#save").prop("disabled", false);
      }); 
    }); 
  </script>





<script>
    $("a[type='image']").click(function() {
          $("input[id='my_file']").click();
      });

      // show image after upload

      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah')
                      .attr('src', e.target.result)
                      .width(70)
                      .height(70);
                      
              };

              reader.readAsDataURL(input.files[0]);
          }
      }
  </script>
    
</body>
</html>