<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>
        #invoice{
    padding: 10px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 0px
}

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #3989c6
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #3989c6
}

.invoice main {
    padding-bottom: 50px
}

.invoice main .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th {
    padding: 15px;
    background: #ddd;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #3989c6;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: left;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

.invoice table .unit {
    background: #eee
}

.invoice table .total {
    background: #3989c6;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
    border-top: none
}

.invoice table tfoot tr:last-child td {
    color: #3989c6;
    font-size: 1.4em;
    border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    }

    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }

    .invoice>div:last-child {
        page-break-before: always
    }
}
    </style>
</head>
<body>
    
@include('layout.Company_sidemenu')   


<main class="l-main">
    <div class="">
            
     
        <div id="invoice" class="px-5">
            <div class="toolbar hidden-print">
                <div class="text-right">
                    <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
                </div>
                <hr>
            </div>
            <div class="invoice">
                <div style="min-width: 600px">
                    <header>
                        <div class="row">
                            <div class="col">
                                <a target="_blank">
                                  <div class="navbar-brand" style="display: inline-flex"><img src="images/log.png" alt="" height="50px"><span><h5 class="my-3" style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: #black; letter-spacing: 3px; font-weight: 600;">&nbsp;Jobs<span style="color: #F26721;">.</span>Nokri</h5></span></div>
                                </a>
                            </div>
                            <div class="col company-details">
                                <h2 class="name">
                                    <a target="_blank" class="text-info">
                                      Jobs.Nokri
                                    </a>
                                </h2>
                                <div>455 South Delhi, AZ 85004, India</div>
                                <div>(123) 456-789</div>
                                <div>Jobs.Nokri@gmail.com</div>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row contacts">
                            <div class="col invoice-to">
                                <div class="text-gray-light">INVOICE TO:</div>
                                <h2 class="to text-info">{{$data->Firstname}} {{$data->Lastname}}</h2>
                                <div class="address">796 Silver Harbour, {{$data->phone_no}}</div>
                                <div class="email"> <a>{{$data->email}}</a></div>
                            </div>
                            <div class="col invoice-details">
                                <h1 class="invoice-id">INVOICE 3-2-1</h1>
                                <!-- <div class="date">Date of Invoice: 01/10/2018</div>
                                <div class="date">Due Date: 30/10/2018</div> -->
                            </div>
                        </div>
                        <table border="0" cellspacing="0" cellpadding="0">
   
                            <thead style="background: #ddd !important;">
                                <tr >
                                    <th>ID</th>
                                    <th class="text-left">DATE</th>
                                    <th class="text-left">TRANSACTION-ID</th>
                                    <th class="text-left">STATUS</th>
                                    <th class="text-left">ORDER-ID</th>
                                    <th class="text-left">DURATION</th>
                                    <th class="text-left">AMOUNT</th>
                                    <th class="text-left">GST-TAX</th>
                                    <th class="text-left">TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                @foreach($paymentDetailsData as $d)
                                    <tr>
                                        <td class="no">{{$d->id}}</td>
                                        <td class="unit"> {{$d->created_at}}</td>
                                        <td class="unit">{{$d->TXNID}}</td>
                                        <td class="unit">{{$d->STATUS}}</td>
                                        <td class="unit">{{$d->ORDERID}}</td>
                                        <td class="qty unit">24 MONTH</td>
                                        <td class="qty unit">₹{{$d->TXNAMOUNT}}</td>
                                        <td class="qty unit" >₹0.00</td>
                                        <td class="total" >{{$d->TXNAMOUNT}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2">SUBTOTAL</td>
                                    <td>$5,200.00</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2">TAX 25%</td>
                                    <td>$1,300.00</td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2">GRAND TOTAL</td>
                                    <td>$6,500.00</td>
                                </tr>
                            </tfoot> -->
                        </table>
                        <!-- <div class="thanks">Thank you!</div> -->
                        <div class="notices">
                            <div>NOTICE:</div>
                            <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                        </div>
                    </main>
                    <footer>
                        Invoice was created on a computer and is valid without the signature and seal.
                    </footer>
                </div>
                <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
                <div></div>
            </div>
        </div>
    </div>
</main>       

<script>
     $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
</script>



</body>
</html>