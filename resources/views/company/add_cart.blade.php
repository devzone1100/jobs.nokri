<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Cart</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>

@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box
}

body {
    background-color: #eee
}

nav,
.wrapper {
    padding: 3px 50px
}

nav .logo a {
    color: #000;
    font-size: 1.2rem;
    font-weight: bold;
    text-decoration: none
}

nav div.ml-auto a {
    text-decoration: none;
    font-weight: 600;
    font-size: 0.8rem
}

header {
    padding: 20px 0px
}

header .active {
    font-weight: 700;
    position: relative
}

header .active .fa-check {
    position: absolute;
    left: 50%;
    bottom: -27px;
    background-color: #fff;
    font-size: 0.7rem;
    padding: 5px;
    border: 1px solid #008000;
    border-radius: 50%;
    color: #008000
}

.progress {
    height: 2px;
    background-color: #ccc
}

.progress div {
    display: flex;
    align-items: center;
    justify-content: center
}

.progress .progress-bar {
    width: 40%
}

#details {
    padding: 30px 50px;
    min-height: 300px
}

input {
    border: none;
    outline: none
}

.form-group .d-flex {
    border: 1px solid #ddd
}

.form-group .d-flex input {
    width: 95%
}

.form-group .d-flex:hover {
    color: #000;
    cursor: pointer;
    border: 1px solid #008000
}

select {
    width: 100%;
    padding: 8px 5px;
    border: 1px solid #ddd;
    border-radius: 5px
}

input[type="checkbox"]+label {
    font-size: 0.85rem;
    font-weight: 600
}

#address,
#cart,
#summary {
    padding: 20px 50px
}

#address .d-md-flex p.text-justify,
#register p.text-muted {
    margin: 0
}

#register {
    background-color: #d9ecf2
}

#register a {
    text-decoration: none;
    color: #333
}

#cart,
#summary {
    max-width: 500px
}

.h6 {
    font-size: 1.2rem;
    font-weight: 700
}

.h6 a {
    text-decoration: none;
    font-size: 1rem
}

.item img {
    object-fit: cover;
    border-radius: 5px
}

.item {
    position: relative
}

.number {
    position: absolute;
    font-weight: 800;
    color: #fff;
    background-color: #0033ff;
    padding-left: 7px;
    border-radius: 50%;
    border: 1px solid #fff;
    width: 25px;
    height: 25px;
    top: -5px;
    right: -5px
}

.display-5 {
    font-size: 1.2rem
}

#cart~p.text-muted {
    margin: 0;
    font-size: 0.9rem
}

tr.text-muted td {
    border: none
}

.fa-minus,
.fa-plus {
    font-size: 0.7rem
}

.table td {
    padding: 0.3rem
}

.btn.text-uppercase {
    border: 1px solid #333;
    font-weight: 600;
    border-radius: 0px
}

.btn.text-uppercase:hover {
    background-color: #333;
    color: #eee
}

.btn.text-white {
    background-color: #66cdaa;
    border-radius: 0px
}

.btn.text-white:hover {
    background-color: #3cb371
}

.wrapper .row+div.text-muted {
    font-size: 0.9rem
}

.mobile,
#mobile {
    display: none
}

.buttons {
    vertical-align: text-bottom
}

#register {
    width: 50%
}

@media(min-width:768px) and (max-width: 991px) {
    .progress .progress-bar {
        width: 33%
    }

    #cart,
    #summary {
        max-width: 100%
    }

    .wrapper div.h5.large,
    .wrapper .row+div.text-muted {
        display: none
    }

    .mobile.h5,
    #mobile {
        display: block
    }
}

@media(min-width: 576px) and (max-width: 767px) {
    .progress .progress-bar {
        width: 29%
    }

    #cart,
    #summary {
        max-width: 100%
    }

    .wrapper div.h5.large,
    .wrapper .row+div.text-muted {
        display: none
    }

    .mobile.h5,
    #mobile {
        display: block
    }

    .buttons {
        width: 100%
    }
}

@media(max-width: 575px) {
    .progress .progress-bar {
        width: 38%
    }

    #cart,
    #summary {
        max-width: 100%
    }

    nav,
    .wrapper {
        padding: 10px 30px
    }

    #register {
        width: 100%
    }
    }

    @media(max-width: 424px) {
        body {
            width: fit-content
        }
    }

    @media(max-width: 375px) {
        .progress .progress-bar {
            width: 35%
        }

        body {
            width: fit-content
        }
    }

    .logo__txt {
            font-size: 26px;
            line-height: 1;
            color: #fff;
            text-align: center;
            font-weight: 700;
    }
    .logo__txt span {
        color: #00EE00;
        position: absolute;
        margin-top: 20px;
        margin-left: -15px;

    }

    .logo__txt img {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border: 2px solid #fff;
    }
 

    </style>
</head>
<body>

<nav class="" style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
    <div class="d-flex align-items-center">
        <div class="logo"> <a href="{{url('home')}}" class="navbar-brand" style="display: inline-flex"><img src="images/log.png" alt="" height="50px"><span><h5 class="my-3" style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: #fff; letter-spacing: 3px; font-weight: 600;">&nbsp;Jobs<span style="color: #F26721;">.</span>Nokri</h5></span></a></div>
        <div class="ml-auto"> <a href="#" class="text-uppercase text-light"><div class="logo__txt"><img class="card-img " src="{{asset('/profile_images/'.$profile_data)}}"  style=""><span class="">●</span></div></a> </div>
    </div>
</nav>

<div class="wrapper">
    <div class="h5 large">Billing Details</div>
   
   
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-10 offset-lg-0 offset-md-2 offset-sm-1">
            <div class="mobile h5">Billing Address</div>
            <div id="details" class="bg-white rounded pb-5">
                <form>
                    <div class="form-group"> <label class="text-muted">Name</label>
                        <div class="d-flex jusify-content-start align-items-center rounded p-2"> <input type="text" value="{{$LoginData['Firstname']}}"> <span class="fa fa-check text-success pr-sm-2 pr-0"></span> </div>
                    </div>
                    <div class="form-group"> <label class="text-muted">Email</label>
                        <div class="d-flex jusify-content-start align-items-center rounded p-2"> <input type="email" value="{{$LoginData['email']}}"> <span class="fa fa-check text-success pr-sm-2 pr-0"></span> </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group"> <label>Phone</label>
                                <div class="d-flex jusify-content-start align-items-center rounded p-2"> <input type="text" value="{{$LoginData['phone_no']}}"> <span class="fa fa-check text-success pr-2"></span> </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group"> <label>Zip code</label>
                                <div class="d-flex jusify-content-start align-items-center rounded p-2"> <input type="text" value="Null"> <span class="fa fa-check text-success pr-2"></span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group"> <label>Address</label>
                                <div class="d-flex jusify-content-start align-items-center rounded p-2"> <input type="text" value="Null"> <span class="fa fa-check text-success pr-2"></span> </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group"> <label>State</label>
                                <div class="d-flex jusify-content-start align-items-center rounded p-2"> <input type="text" value="Null"> <span class="fa fa-check text-success pr-2"></span> </div>
                            </div>
                        </div>
                    </div> <label>Country</label> <select name="country" id="country">
                        <option value="usa">USA</option>
                        <option value="ind">INDIA</option>
                    </select>
                </form>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-8 col-sm-10 offset-lg-0 offset-md-2 offset-sm-1 pt-lg-0 pt-3">
            <div id="cart" class="bg-white rounded">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="h6">Cart Summary</div>
                   
                </div>
                <div class="d-flex jusitfy-content-between align-items-center pt-3 pb-2 border-bottom">
                    <div class="item pr-2"> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgHXuzyZdjv2jOm_Rry8jmwcDL8aDrytYUVRua7U2mQBaP5wyvSJ4qJAzx99oLxPIh-gA&usqp=CAU" alt="" width="80" height="80">
                        <!-- <div class="number">2</div> -->
                    </div>
                    <div class="d-flex flex-column px-3"> <b class="h5">{{$addcartDetails['plan_type']}}</b> <a href="#" class="p">PlanID-PS<small class="text-dark">{{$addcartDetails['plan_type']}}</small> </a> </div>
                    <div class="ml-auto"> <b class="h5">₹{{$addcartDetails['plan_prize']}}</b> </div>
                </div>
                <div class="my-3"> <input type="text" class="w-100 form-control text-center" placeholder="Bill Summary"> </div>
                <div class="d-flex align-items-center">
                    <div class="display-5">Subtotal</div>
                    <div class="ml-auto font-weight-bold">₹{{$addcartDetails['plan_prize']}}</div>
                </div>
                <div class="d-flex align-items-center py-2 border-bottom">
                    <div class="display-5">GST</div>
                    <div class="ml-auto font-weight-bold">₹0.0</div>
                </div>
                <div class="d-flex align-items-center py-2">
                    <div class="display-5">Total</div>
                    <div class="ml-auto d-flex">
                        <div class="text-primary text-uppercase px-3">Rupess</div>
                        <div class="font-weight-bold">₹{{$addcartDetails['plan_prize']}}</div>
                    </div>
                </div>
            </div>

            <div class="row pt-lg-3 pt-2 buttons mb-sm-0 mb-2">
                <div class="col-md-6">
                    <form role="form" method="post" action="{{url('paymentinsret')}}">
                        @csrf
                        
                        <!-- <label for=""><h6>Email</h6></label>  -->
                        <input type="hidden" name="company_email" value="rinkumar123@gmail.com" placeholder="Enter your email" class="form-control "> 
                        
                        <!-- <label for=""><h6>Job ID</h6></label>  -->
                        <input type="hidden" name="job_id" value="12345" placeholder="Enter your job id" required class="form-control "> 
                        
                        <!-- <label for=""><h6>Amount</h6></label> -->
                        <input type="hidden" name="amount" placeholder="Enter your amount" class="form-control " value="{{$addcartDetails['plan_prize']}}">
                            
                        <input type="hidden" name="admin_email"  value="karanveer2602@gmail.com">
                    
                        <input  type="submit" class="btn bg-success px-5 py-2 text-light" value="Continue to Checkout">
                    </form>
                    <!-- <a href="{{url('payment')}}" class="btn text-white ml-auto bg-success"> <span class="fa fa-lock"></span> Continue to Checkout </a> -->
                </div>
                
            </div>
            <div class="text-muted pt-3" id="mobile"> <span class="fa fa-lock"></span> Your information is save </div>
        </div>
    </div>
    <div class="text-muted"> <span class="fa fa-lock"></span> Your information is save </div>
</div>
    
  
</body>
</html>