<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <script src="{{asset('libs/conts.js')}}"></script>

   
    
    <style>

        /* toaster message show */
        #toast-container{position:fixed;z-index:999999;pointer-events:none}
        /* #toast-container>div{position:relative;pointer-events:auto;overflow:hidden;margin:0 800px 6px;padding:15px 15px 15px 50px;width:100%;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px;background-position:15px center;background-repeat:no-repeat;-moz-box-shadow:0 0 12px #999;-webkit-box-shadow:0 0 12px #999;box-shadow:0 0 12px #999;color:#FFF;opacity:.8;-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=80);filter:alpha(opacity=80)} */
        #toast-container>div{position: fixed;
        z-index: 999;
        height: 4em;
        width: 100%;
        overflow: show;
        margin: 0 auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        }
        /* toaster message show end */
       
        .login_class{
            float: left;
            width:100%;
            background-image: url(images/riban_27.jpg);
            height: 100%;
            background-position: center 0;
            background-size: cover;
            position: relative;
            min-height: 610px;
            text-align: center;
            padding-top: 60px;

        }
        .login_form h4 {
            font-size: 26px;
            text-transform: capitalize;
            padding-bottom: 30px;
        }
        .form-group {
            padding-top: 20px;
        }
        .form-control{
            border-radius: 0px;
            height: 40px;
        }
        .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 0px solid black;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .btn_eye {
            cursor: context-menu !important;
            border: 0px solid black;
            background: linear-gradient(to right, #fff 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .form-group a {
            text-decoration: none;
        }
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }

        /* tab */
        .nav-tabs{
            background: linear-gradient(to right, #61b1cc 30%, #F26721 70%) ;
        }
        .nav-link {
            color:#15695e !important;
        }

        /* validation password */

        input.error, textarea.error {
            border: 1px dashed red;
            font-weight: 300;
            color: red;
        }

        label.error.fail-alert {
        /* border: 1px dashed red;
        border-radius: 4px; */
        line-height: 1;
        padding: 2px 0 6px 6px;
        color:red;
        /* background: #ffe6eb; */
        }
        input.valid.success-alert {
        border: 2px solid #4CAF50;
        color: green;
        }

        /* validation password end */

         /* data inser loader ------------------------*/
         .loader {
        width: 100%;
        height: 500%;
        /* background: rgba(0, 0, 0, 0.5) !important; */
        display: none;
        position: absolute;
        z-index: 1000;
        }
        .loader-text {
        position: fixed;
        z-index: 999;
        height: 0em;
        width: 7em;
        overflow: show;
        margin: auto;
        top: 5%;
        left: 0;
        bottom: 0;
        right: 0;

        }

        .loading {

            border-radius: 50%;
            z-index: 999;
            height: 2px;
            width: 2px;
            position: fixed;
            margin: auto;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            box-shadow: 10px 0 0 2px #F26721, 7px 7px 0 2px #999, 0 10px 0 2px #999, -7px 7px 0 2px #999, -10px 0 0 2px #999, -7px -7px 0 2px #999, 0 -10px 0 2px #F26721, 7px -7px 0 2px #F26721;
            -webkit-animation: rotate 0.7s steps(8) infinite;
            -o-animation: rotate 0.7s steps(8) infinite;
            animation: rotate 0.7s steps(8) infinite;
  
        }

        @keyframes rotate {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    /* data inser loader End */    

    .social-btns{
        font-size: 22px;
        list-style: none;
    }

    .social-btns li{
        margin-bottom: 5px;
    }

    .social-btns li i{
        font-size: 22px;
    }



    .social-btns .btn-linkedin {
    background-color: #00629e;
    border-color: #044a75;
    color: #ffffff;
    min-width: 250px;
    }

    .social-btns .btn-linkedin:hover {
    color: #ffffff;
    background-color: #044a75;
    }

    .social-btns .btn-google {
    background-color: #df270b;
    border-color: #a01b03;
    color: #ffffff;
    min-width: 250px;
    }

    .social-btns .btn-google:hover {
    color: #ffffff;
    background-color: #a01b03;
    }


    </style>
</head>
<body>
    
        <div class="loader">
            <p class="text-success loader-text">&nbsp; Please wait....</p>
            <div class="loading"> 
            </div>
        </div>

        
        
    
    <div class="container-fluid text-light" style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
        <div class="row pt-2">
            <div class="col-md-6 col-sm-6 col-6 mx-auto pr-5">
                 <h5 class="text-center pr-5">Sign Up</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-6 pl-5">
                <h6 class="text-center pl-5"><a href="{{url('home')}}" class="text-light" style="text-decoration: none;">Home</a>   /  <a href="signup.html" class="text-light" style="text-decoration: none;">Sign Up</a></h6>  
           </div>
        </div>
    </div> 

    <div class="container py-4">
        <div class="row" style="border: 2px solid rgb(179, 177, 177);">
            <div class="col-md-6 login_class">
                <div>
                    <img src="images/log.png" alt=""><span class="m-3 " style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: black; letter-spacing: 3px; font-weight: 600; font-size: 20px;">Jobs<span style="color: #F26721;">.</span>Nokri</span>
                    <div class="mt-5 mr-5">
                        <ul class="social-btns center-block">
                            <li><button class="btn btn-linkedin"><i class="fa fa-linkedin pull-left" aria-hidden="true"></i>Sign in with Linkedin</button></li>
                            <li><button class="btn btn-google"><i class="fa fa-google-plus pull-left" aria-hidden="true"></i>Sign in with Google</button></li>
                        </ul>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6 mx-auto">
                <h4 class="text-center py-4">Sign Up</h4>
              
                <ul class="nav nav-tabs nav-justified"  id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"  aria-selected="true">As a User</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"  aria-selected="false">As a Company</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form method="post" action="{{url('registerinsret')}}" class="text-dark" id="signup_form">
                           
                            <hr>
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Enter First name*" name="first_name" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Enter Last name*" name="last_name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" placeholder="Enter Email*" name="email" class="form-control">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input  placeholder="Enter Phone no*" maxlength="10" name="phone" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group pt-3 password_parent">
                                        <input type="password" placeholder="Enter Password*" name="password" id="passwordUser"  class="form-control">
                                        <span class="input-group-append bg-light px-2">
                                            <i class="fa fa-eye mt-3" id="togglePasswordUser" style="cursor: pointer;" ></i>
                                        </span>

                                    </div>
                                    <p class="error-text3"></p>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group pt-3 confirm_parent">
                                        <input type="password" class="form-control" name="confirm" id="passconfirmUser"  placeholder="Conform Password*">
                                        <span class="input-group-append bg-light px-2">
                                            <i class="fa fa-eye mt-3" id="togglePasswordUserConfirm" style="cursor: pointer;" ></i>
                                        </span>
                                      
                                    </div> 
                                    <p class="error-text4"></p>

                                </div>
                            </div>
                           
                            <div class="row mt-1 px-2">
                                <div class="col-md-6">
                                    <div class="input-group gender-parent">
                                        <label for="usr">Gender :</label>
                                        <label class="radio inline px-2 mt-1"> 
                                            <input type="radio" name="gender" value="male">
                                            <span> Male </span> 
                                        </label>
                                        <label class="radio inline ml-2 mt-1"> 
                                            <input type="radio" name="gender" value="female">
                                            <span> Female </span> 
                                        </label>
                                    </div>
                                    <p class="error-text2"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 px-5">
                                    <div class="input-group mt-4">
                                        <label class="radio inline"> 
                                            <input type="checkbox" name="user_type" value="user">
                                            <span>I hereby agree to abide by the <a href="#"> Terms and Conditions.</a></span> 
                                        </label>
                                        <p class="error-text"></p>
                                    </div>
                                    <div class="form-group">
                                        <input name="register"   type="submit" class="btn_2 form-control text-light">
                                        
                                      
                                    </div>
                                    <div class="account text-center">
                                        <p>You have an acount ?<a href="{{url('login')}}" style="text-decoration: none;">&nbsp; Login <i class="fa fa-sign-in"></i></a></p>
                                    </div>
                                </div>
                            </div>
                          
                        </form>   
                    </div>
                    <div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                         <form method="post" action="{{url('registerinsret')}}" class="text-dark" id="signup_form_company">
                            <hr>
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" placeholder="Enter First name*" name="first_name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                        <input type="text" placeholder="Enter Last name*" name="last_name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" placeholder="Enter Email*" name="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                        <input  placeholder="Enter Phone no*" maxlength="10" name="phone" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group pt-3 password_parent">
                                        <input type="password" placeholder="Enter Password*" name="password" id="password"  class="form-control">
                                        <span class="input-group-append bg-light px-2">
                                            <i class="fa fa-eye mt-3" id="togglePassword" style="cursor: pointer;" ></i>
                                        </span>
                                    </div>
                                    <p class="error-text3"></p>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group pt-3 confirm_parent">
                                        <input type="password" class="form-control" id="confirm" name="confirm"  placeholder="Conform Password*">
                                        <span class="input-group-append bg-light px-2">
                                            <i class="fa fa-eye mt-3" id="togglePasswordConfirm" style="cursor: pointer;" ></i>
                                        </span>
                                    </div>
                                    <p class="error-text4"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 px-5 mx-auto">
                                    <div class="input-group mt-4 ">
                                        <label class="radio inline text-center"> 
                                            <input type="checkbox" name="user_type" value="company">
                                            <span>I hereby agree to abide by the <a href="#"> Terms and Conditions.</a></span> 
                                        </label>
                                        <p class="error-text"></p>
                                    </div>
                                    <div class="form-group">
                                        <input name="register"  type="submit" class="btn_2 form-control text-light">
                                    </div>
                                    <div class="account text-center">
                                        <p>You have an acount ?<a href="{{url('login')}}" style="text-decoration: none;">&nbsp; Login <i class="fa fa-sign-in"></i></a></p>
                                    </div>
                                </div>
                            </div>
                           
                        </form> 
                    </div>
                </div>
            </div>
           
        </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>

<script>

    @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch(type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;

            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
    @endif
</script>









<script>



// password eye-show Company-Form**************************

    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-low-vision');
    });

    const togglePasswordConfirm = document.querySelector('#togglePasswordConfirm');
    const pass = document.querySelector('#confirm');

    togglePasswordConfirm.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = pass.getAttribute('type') === 'password' ? 'text' : 'password';
    pass.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-low-vision');
    });
    
// password eye-show User-Form**************************

    const togglePasswordUser = document.querySelector('#togglePasswordUser');
    const passwordUser = document.querySelector('#passwordUser');

    togglePasswordUser.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = passwordUser.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordUser.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-low-vision');
    });

    const togglePasswordUserConfirm = document.querySelector('#togglePasswordUserConfirm');
    const passUserConfirm = document.querySelector('#passconfirmUser');

    togglePasswordUserConfirm.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = passUserConfirm.getAttribute('type') === 'password' ? 'text' : 'password';
    passUserConfirm.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-low-vision');
    });

    // validater form----------------------
$("#signup_form").validate({
    errorClass: "error fail-alert",
    validClass: "valid success-alert",
    rules:{
    "first_name":{
        required:true,
    },
    "last_name":{
        required:true,
    },
    "email":{
        required:true,
    },
    "password":{
        required:true,
        minlength: 8,
    },
    "user_type":{
        required:true,
        
    },
    
    "confirm":{
        required:true,
        // minlength: 8,
        equalTo: "#passwordUser"
    },
    "phone":{
        required:true,
        number: true,
        minlength: 10,
        maxlength:10
    },
    "gender":{
        required:true,
    }
   
},
errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
        error.appendTo($(element).parent().siblings('.error-text'));
    }else if(element.attr("type") == "radio"){
        error.appendTo($(element).parent().parent(".gender-parent").siblings('.error-text2'));  
    } 
    else if(element.attr("name") == "password"){
        error.appendTo($(element).parent(".password_parent").siblings('.error-text3'));  
    }
    else if(element.attr("name") == "confirm"){
        error.appendTo($(element).parent(".confirm_parent").siblings('.error-text4'));  
    }
     else {
        error.insertAfter(element);
    }
},
messages: {
        
        confirm: {
            equalTo: "Please enter the same Password."
        },

        user_type:{
            required: "Reqiure Checkbox"

        },
        gender: {
            required: "Choose gender"

        }

    },
 
    
    submitHandler: function(form) {
    document.getElementsByClassName("loader")[0].style.display = "block";
    SubmittingForm();
       
  
  }

});

// confirm_parent

$("#signup_form_company").validate({
    errorClass: "error fail-alert",
    validClass: "valid success-alert",
    rules:{
    "first_name":{
        required:true,
    },
    "last_name":{
        required:true,
    },
    "email":{
        required:true,
    },
    "password":{
        required:true,
        minlength: 8,
    },
    "user_type":{
        required:true,
    },
    
    "confirm":{
        required:true,
        minlength: 8,
        equalTo: "#password"
    },
    "phone":{
        required:true,
        number: true,
        minlength: 10,
        maxlength:10
    }
    
   
},
errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
        error.appendTo($(element).parent().siblings('.error-text'));
    } 
    else if(element.attr("name") == "password"){
        error.appendTo($(element).parent(".password_parent").siblings('.error-text3'));  
    }
    else if(element.attr("name") == "confirm"){
        error.appendTo($(element).parent(".confirm_parent").siblings('.error-text4'));  
    }
     else {
        error.insertAfter(element);
    }
},

submitHandler: function(form) {
    document.getElementsByClassName("loader")[0].style.display = "block";
    SubmittingForm();
       
  
  },

    messages: {
        
        confirm: {
            equalTo: "Please enter the same Password."
        },
        user_type:{
            required: "Reqiure Checkbox"

        },

    }
    
    

});

</script>


    
        
    
   

    
    
</body>
</html>