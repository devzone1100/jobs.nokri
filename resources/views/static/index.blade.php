<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">


<!-- 
    <link rel="stylesheet" href="{{asset('css3-preloader-transition-finish/css/normalize.css')}}">
	<link rel="stylesheet" href="{{asset('css3-preloader-transition-finish/css/main.css')}}">
	<script src="{{asset('js/vendor/modernizr-2.6.2.min.js')}}"></script> -->
    
 
    
    <style>
        h1,h2,h3,h4,h5,h6 {
            font-family:'Times New Roman', Times, serif;
        }
        .form-control{
            border-radius: 0px;
            height: 45px;
        }
        .home_bg{
            background-image: url("images/riban_0.jpg");
            background-position: center 0;
            background-size: cover;
            height: 100%;
            position:relative;
            border-bottom:1px solid #e2e2e2;
            
        }

        .box_home {
            background: #ffff;
            border: 1px solid #e2e2e2;
            text-align: center;
            padding: 20px 0px 10px 0px;
            position: relative;
          
            
        }
        .box_home_hov:hover{
            background: linear-gradient(to right, #147fa3 10%, #F26721 90%);
            border: 1px solid #e2e2e2;
            text-align: center;
            padding: 0px 3px 3px 3px;
        }
        .box_home h5 a {
            text-transform: uppercase;
            color: black;
            font-size: 17px;
            text-decoration: none;
        }
        .job_ctg {
            padding: 35px 20px 35px 20px;
            text-align: center;
            margin-bottom: 30px;
            border: 1px solid #147fa3;
            position: relative;
            overflow: hidden;
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg a{
            text-decoration: none !important;
        }
        .job_ctn{
            margin-top: 100px;
        }
        .job_ctg .hover-block:before {
            bottom: 71%;
            left: 50%;
            -webkit-transform: translate(-50%, -100%);
            transform: translate(-50%, -100%);
            background-color:rgba(255, 255, 255, 0.09);
            z-index: 10;
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg .hover-block:before, .job_ctg .hover-block:after {
            content: '';
            width: 600px;
            height: 600px;
            border-radius: 50%;
            position: absolute;
        }
        .job_ctg:hover {
            background: linear-gradient(to right, #147fa3 10%, #F26721 90%);
            border: 1px solid #F26721;
                -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg:hover:before {
            -webkit-transform: translate(-50%, 0%);
            transform: translate(-50%, 0%);
            transition-delay: .2s;
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg:hover i:before, .job_ctg:hover h3, .job_ctg:hover p{
            color:#fff;
                -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg:hover .hover-block:before {
            -webkit-transform: translate(-50%, 0%);
            transform: translate(-50%, 0%);
            transition-delay: .1s;
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg:hover .hover-block:after {
            -webkit-transform: translate(-50%, 0%);
            transform: translate(-50%, 0%);
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
        }
        .job_ctg i:before {
            color: #F26721;
            font-size: 28px;
            float:left; width:100%;
            padding-bottom:10px;
            -webkit-transition: all 0.5s;
            -o-transition: all 0.5s;
            -ms-transition: all 0.5s;
            -moz-transition: all 0.5s;
            transition: all 0.5s;
        }
        
        .job_ctg p{
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
            
        }
        .job_ctg h3{
            color: #147fa3;
            padding-top: 15px;
            font-size: 20px;
            text-transform:capitalize;
            position:relative;
            z-index:10;
            -webkit-transition: all 0.6s;
            -o-transition: all 0.6s;
            -ms-transition: all 0.6s;
            -moz-transition: all 0.6s;
            transition: all 0.6s;
            
        }
        .job_ctg a {
            text-decoration: none;
            color: #F26721;
        }

        .bg_grow {
            margin-top: 50px;
            background: #e2e2e2;
            padding: 100px 0;
        }
        .latest_job {
            border-bottom: 1px solid #e2e2e2;
        }
        .latest_job_tabs {
            float: right;
            display: inline-block;
            
        }
        .latest_job_tabs .nav {
            display: inline-block;
            border-bottom: 0;
        }
        .latest_job_tabs .nav li {
            font-size: 20px;
            float: left;
            border: 0;
            border-left: 0;
        }
        .counter_wrapper {
            width: 100%;
            float: left;
            background-image: url(images/riban_11.jpg);
            height: 100%;
            background-position: center 0;
            background-size: cover;
            position: relative;
            padding: 80px 0;
            padding-bottom: 60px;
            overflow: hidden;
        }
        .counter_overlay {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            background: #147fa3;
            background: -moz-linear-gradient(0deg, #147fa3 39%, #F26721 100%);
            background: -webkit-linear-gradient(0deg, #147fa3 39%, #F26721 100%);
            background: linear-gradient(0deg, #147fa3 39%, #F26721 100%);
        }
        
        .counter_mockup_design {
            position: relative;
            /* left: -70px; */
        }
        .counter_mockup_design img {
            text-align:center !important;
            position: relative;
            /* z-index: 1; */
            -webkit-animation: movebounce 3.9s linear infinite;
            animation: movebounce 3.9s linear infinite;
        }
        @keyframes movebounce {
            0% {
                transform: translateY(0px); }
            50% {
                transform: translateY(20px); }
            100% {
                transform: translateY(0px); } 
        }
        .counter_right_wrapper {
            padding-top: 55px;
        }
        .counter_box {
            margin-top: 30px;
            color: #fff;
        }
        .count-description span {
            font-size: 38px;
            font-weight: 600;
            
            
        }
        .count-description .con2 {
            font-size: 17px;            
        }
        .carousel-inner img {
            width: 100%;
            height: 100%;
           
           
            
        }
        .animation-circle-inverse {
            padding-right:80px;
        }
        .animation-circle-inverse i {
            background: #f18fb5;
            -webkit-box-shadow: 0 15px 30px 0 #ec3668;
            box-shadow: 0 15px 30px 0 #ec3668;
            position: absolute;
            height: 75px;
            width: 75px;
            top: 36%;
            /* margin: 0 auto; */
            -webkit-border-radius: 100%; 
            -moz-border-radius: 100%; 
            border-radius: 100%; 
            opacity: 0.3;
            -webkit-transform: scale(1.4);
            transform: scale(1.4);
            -webkit-animation: ripple1 3s linear infinite;
            animation: ripple1 3s linear infinite;
        }
        .animation-circle-inverse i:nth-child(2) {
            -webkit-animation: ripple2 3s linear infinite;
            animation: ripple2 3s linear infinite;
        }
        .animation-circle-inverse i:nth-child(3) {
            -webkit-animation: ripple3 3s linear infinite;
            animation: ripple3 3s linear infinite;
        }
        @keyframes ripple1 {
            0% {
                -webkit-transform: scale(5.5);
                transform: scale(5.5);
                opacity: 0.3
            }
            100% {
                -webkit-transform: scale(6.5);
                transform: scale(6.5);
                opacity: 0.0
            }
        }
        @keyframes ripple2 {
            0% {
                -webkit-transform: scale(3.5);
                transform: scale(3.5)
            }
            100% {
                -webkit-transform: scale(5.5);
                transform: scale(5.5)
            }
        }
        @keyframes ripple3 {
            0% {
                -webkit-transform: scale(1.5);
                transform: scale(1.5)
            }
            100% {
                -webkit-transform: scale(3.5);
                transform: scale(3.5)
            }
        }
        .btn_1 {
            border: 0px solid #F26721;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 0px solid #147fa3;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center; 
            color: #ffff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
   
       
        /* card hover effect */

        .box{
            background: linear-gradient(180deg, #e52d27 0%, #b31217 100%);
            font-family: 'Nanum Gothic', sans-serif;
            border-radius: 25px 0;
            position: relative;
            overflow: hidden;
            height: 350px;
        }
        .box:before,
        .box:after,
        .box .box-content:before,
        .box .box-content:after{
            content: "";
            background: #fff;
            width: 50%;
            height: 4px;
            transform: scaleX(0);
            position: absolute;
            top: 15px;
            left: 15px;
            z-index: 1;
            transition: all 600ms ease;
        }
        .box:after{
            top: auto;
            bottom: 15px;
            left: auto;
            right: 15px;
        }
        .box .box-content:before,
        .box .box-content:after{
            width: 4px;
            height: 50%;
            transform: scaleY(0);
        }
        .box .box-content:after{
            left: auto;
            right: 15px;
            top: auto;
            bottom: 15px;
        }
        .box:hover:before,
        .box:hover:after,
        .box:hover .box-content:before,
        .box:hover .box-content:after{
            transform: scale(1);
        }
        .box img{
            width: 100%;
            height: 350px;
            transform: scale3d(1.1, 1.1, 1);
            transition: all 0.25s linear;
        }
        .box:hover img{
            opacity: 0.25;
            transform: scale(1.25);
        }
        .box .inner-content{
            color: #fff;
            text-align: center;
            width: 70%;
            opacity: 0;
            transform: translateX(-50%) translateY(-50%);
            position: absolute;
            top: 70%;
            left: 50%;
            transition: all 600ms ease;
        }
        .box:hover .inner-content{
            opacity: 1;
            top: 50%;
        }
        .box .title{
            font-size: 20px;
            font-weight: 800;
            letter-spacing: 1px;
            text-transform: uppercase;
            margin: 0 0 3px;
        }
        .box .post{
            font-size: 14px;
            letter-spacing: 1px;
            text-transform: capitalize;
            margin: 0 0 12px;
            display: block;
        }
        .box .icon{
            padding: 0;
            margin: 0;
            list-style: none;
        }
        .box .icon li{
            display: inline-block;
            margin: 0 4px;
        }
        .box .icon li a{
            text-decoration: none;
            color: #fff;
            font-size: 18px;
            line-height: 32px;
            border: 2px solid #fff;
            border-radius: 10px 0 10px 0;
            display: block;
            transition: all 0.3s;
        }
        .box .icon li a:hover{
            color: #b31217;
            background: #fff;
            border-radius: 0 10px 0 10px;
        }
        @media only screen and (max-width:990px){
            .box { margin: 0 0 30px; }
        }
        /* card hover end */

        option{
            font-size: 18px;
            text-transform: capitalize;
            background: #dddcdc;
           
        }
        .home_bg .form-control{
            border-radius: 5px;
            
        }

        /* readmore */
        .moretext {
        display: none;
        }

        .nav_2 span{
            background-color: #F26721;
        }
        .navbar_2 .nav-item{
            padding-right: 25px;
        }

        

        /* counter number */

        #counter .counter-value {
            font-size: 35px;
            color: #ffff;
        }
        #counter p {
            font-size: 18px;
            color: #ffff;
        }
        .fake-div {
        width:100%;
        position:relative;
        }

        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6) !important;
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6) !important;
        }
        button{
            border:0px solid red !important;
            outline-color:white !important;
        }
        .btn{
            border:0px solid red !important;
            outline-color:white !important;
        }
        a {
            text-decoration:none !important;
        }

        .accordion .card-header:after {
            font-family: 'FontAwesome';  
            content: "\f068";
            float: right; 
            
            color:#F26721!important;
        }
        .accordion .card-header.collapsed:after {
            /* symbol for "collapsed" panels */
            content: "\f067";
            color:#147fa3 !important;
             
        }
        .accordion .card-header {
            background: #ffff !important;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
        }
        .accordion .card-header .card-title {
            color:#F26721!important;
        }


        /* latest-job----------------------------- */

        .no-gutters img {
            height: 78px !important;
            width: 150px !important;
        }

        .news {
        width: 160px;
        }

        .news-scroll a {
        text-decoration: none;
        }

        .dot {
        height: 6px;
        width: 6px;
        margin-left: 3px;
        margin-right: 3px;
        margin-top: 2px !important;
        background-color: rgb(207, 23, 23);
        border-radius: 50%;
        display: inline-block;
        }

        /* Custom */

        marquee a {
        color: black;
        }


        /* Choose-Plane---------------------------------------------- */
        .pricing-wrapper {
        color: #FFF;
        font-size: 62.5%;
        font-family: 'Roboto', Arial, Helvetica, Sans-serif, Verdana;
      }

      .pricing-wrapper ul {
        list-style-type: none;
      }

      .pricing-wrapper a {
        color: #e95846;
        text-decoration: none;
      }

      .pricing-table-title {
        text-transform: uppercase;
        font-weight: 700;
        font-size: 2.6em;
        color: #FFF;
        margin-top: 15px;
        text-align: left;
        margin-bottom: 25px;
        text-shadow: 0 1px 1px rgba(0,0,0,0.4);
      }

      .pricing-table-title a {
        font-size: 0.6em;
      }

      .clearfix:after {
        content: '';
        display: block;
        height: 0;
        width: 0;
        clear: both;
      }
      /** ========================
      * Contenedor
      ============================*/
      .pricing-wrapper {
        width: 960px;
        margin: 40px auto 0;
      }

      .pricing-table {
        margin: 0 10px;
        text-align: center;
        width: 300px;
        float: left;
        -webkit-box-shadow: 0 0 15px rgba(0,0,0,0.4);
        box-shadow: 0 0 15px rgba(0,0,0,0.4);
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
      }

      .pricing-table:hover {
        -webkit-transform: scale(1.06);
        -ms-transform: scale(1.06);
        -o-transform: scale(1.06);
        transform: scale(1.06);
      }

      .pricing-title {
        color: #FFF;
        background: #e95846;
        padding: 20px 0;
        font-size: 2em;
        text-transform: uppercase;
        text-shadow: 0 1px 1px rgba(0,0,0,0.4);
      }

      .pricing-table.recommended .pricing-title {
        background: #2db3cb;
      }

      .pricing-table.recommended .pricing-action {
        background: #2db3cb;
      }

      .pricing-table .price {
        background: #403e3d;
        font-size: 3.4em;
        font-weight: 700;
        padding: 20px 0;
        text-shadow: 0 1px 1px rgba(0,0,0,0.4);
      }

      .pricing-table .price sup {
        font-size: 0.4em;
        position: relative;
        left: 5px;
      }

      .table-list {
        background: #FFF;
        color: #403d3a;
      }

      .table-list li {
        font-size: 1.4em;
        font-weight: 700;
        padding: 12px 8px;
      }

      .table-list li:before {
        content: "\f00c";
        font-family: 'FontAwesome';
        color: #3fab91;
        display: inline-block;
        position: relative;
        right: 5px;
        font-size: 16px;
      } 

      .table-list li span {
        font-weight: 400;
      }

      .table-list li span.unlimited {
        color: #FFF;
        background: #e95846;
        font-size: 0.9em;
        padding: 5px 7px;
        display: inline-block;
        -webkit-border-radius: 38px;
        -moz-border-radius: 38px;
        border-radius: 38px;
      }


      .table-list li:nth-child(2n) {
        background: #F0F0F0;
      }

      .table-buy {
        background: #FFF;
        padding: 15px;
        text-align: left;
        overflow: hidden;
      }

      .table-buy p {
        float: left;
        color: #37353a;
        font-weight: 700;
        font-size: 2.4em;
      }

      .table-buy p sup {
        font-size: 0.5em;
        position: relative;
        left: 5px;
      }

      .table-buy .pricing-action {
        float: right;
        color: #FFF;
        background: #e95846;
        padding: 10px 16px;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        font-weight: 700;
        font-size: 1.4em;
        text-shadow: 0 1px 1px rgba(0,0,0,0.4);
        -webkit-transition: all 0.25s ease;
        -o-transition: all 0.25s ease;
        transition: all 0.25s ease;
      }

      .table-buy .pricing-action:hover {
        background: #cf4f3e;
      }

      .recommended .table-buy .pricing-action:hover {
        background: #228799;  
      }

      /** ================
      * Responsive
      ===================*/
      @media only screen and (min-width: 768px) and (max-width: 959px) {
        .pricing-wrapper {
          width: 768px;
        }

        .pricing-table {
          width: 236px;
        }
        
        .table-list li {
          font-size: 1.3em;
        }

      }

      @media only screen and (max-width: 767px) {
        .pricing-wrapper {
          width: 420px;
        }

        .pricing-table {
          display: block;
          float: none;
          margin: 0 0 20px 0;
          width: 100%;
        }
      }

      @media only screen and (max-width: 479px) {
        .pricing-wrapper {
          width: 300px;
        }
      } 

      * {
        margin: 0;
        padding: 0;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
      }

      /* choose-plan-end------------------------------------------------------ */

 
        

        
     
        
    </style>

</head>
<body>

    <div id="loader-wrapper">
        <div id="loader"></div>

        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>

    </div>

    <div id="company-page" >
        <section class="home_bg">
            @include('layout.navbar')
        
            <div class="container-fluid">
                <div class="row ">
                   
                    <div class="col-md-12 col-sm-12 col-12 mx-auto px-5" style="margin-top:230px; margin-bottom:360px;">
                        <div class="row mx-auto p-5">
                            <div class="col-md-3 col-12 col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="name" placeholder="Keyword e.g. (Job Title, Description, Tags)">
                                </div>
                            </div>
                            <div class="col-md-3 col-12 col-sm-12">
                                <div class="form-group">
                                    <i class=""></i>
                                    <select class="form-control">
                                        <option>Category</option>
                                        <option>Real estate</option>
                                        <option>electronics</option>
                                        <option>marketing</option>
                                        <option>education</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-12 col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" editable="true" searchable="Search and add here...">
                                        <option>Select location</option>
                                        <option>california</option>
                                        <option>los velas</option>
                                        <option>noida</option>
                                        <option>chicago</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-12 col-sm-12">
                                <div class="" style="">
                                    <button class=" btn_1 form-control  text-light  py-1 btn-outline-light" ><i class="fa fa-search" style="font-size: 18px;">&nbsp;&nbsp;Search</i></button>
                                </div>
                            </div>

                        </div>   
                    </div>

                    
                </div>
            </div>
        </section> 

        <div class="container">
            <div class="row mt-5">
                <div class="col-md-2  col-sm-4 col-12  mx-auto">
                    <div class="box_home_hov">
                        <div class="box_home">
                            <h5><a href="#">laravel</a></h5>
                            <img src="images/riban_4.jpg" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-md-2  col-sm-4 col-12  mx-auto">
                    <div class="box_home_hov">
                        <div class="box_home">
                            <h5><a href="#">laravel</a></h5>
                            <img src="images/riban_5.jpg" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-md-2  col-sm-4 col-12  mx-auto">
                    <div class="box_home_hov">
                        <div class="box_home">
                            <h5><a href="#">laravel</a></h5>
                            <img src="images/riban_6.jpg" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-md-2  col-sm-4 col-12  mx-auto">
                    <div class="box_home_hov">
                        <div class="box_home">
                            <h5><a href="#">laravel</a></h5>
                            <img src="images/riban_7.jpg" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-md-2  col-sm-4 col-12  mx-auto">
                    <div class="box_home_hov">
                        <div class="box_home">
                            <h5><a href="#">laravel</a></h5>
                            <img src="images/riban_8.jpg" alt="img">
                        </div>
                    </div>
                </div>
                <div class="col-md-2  col-sm-4 col-12  mx-auto">
                    <div class="box_home_hov">
                        <div class="box_home">
                            <h5><a href="#">laravel</a></h5>
                            <img src="images/riban_7.jpg" alt="img">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container job_ctn">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12 pb-4">
                    <div class="jb_heading_wraper text-center pb-5">
                        <h3>Browse Jobs By <span style="color: #F26721;">Category</span></h3>
                        <p class="text-secondary">Your next level Product developemnt company assets</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="job_ctg">
                        <a href="">
                            <div class="hover-block"></div>
                            <i class="fa fa-code"></i>
                            <h3>developer</h3>
                            <p>(1450 jobs)</p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="job_ctg">
                        <a href="">
                            <div class="hover-block"></div>
                            <i class="fa fa-laptop"></i>
                            <h3>technology</h3>
                            <p>(4525 jobs)</p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="job_ctg ">
                        <a href="">
                            <div class="hover-block"></div>
                            <i class="fa fa-university"></i>
                            <h3>accounting</h3>
                            <p>(214 jobs)</p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="job_ctg">
                        <a href="">
                            <div class="hover-block"></div>
                            <i class="fa fa-google-plus"></i>
                            <h3>Google+</h3>
                            <p>(4572 jobs)</p>

                        </a>
                    </div>
                </div>
                <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12 pd5">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">

                            <div class="job_ctg ">
                                <a href="">
                                    <div class="hover-block"></div>
                                    <i class="fa fa-building"></i>
                                    <h3>goverment</h3>
                                    <p>(2414 jobs)</p>

                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">

                            <div class="job_ctg ">
                                <a href="">
                                    <div class="hover-block"></div>
                                    <i class="fa fa-phone"></i>
                                    <h3>media &amp; news</h3>
                                    <p>(2142 jobs)</p>

                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">

                            <div class="job_ctg ">
                                <a href="">
                                    <div class="hover-block"></div>
                                    <i class="fa fa-linkedin"></i>
                                    <h3>linkdin</h3>
                                    <p>(2342 jobs)</p>

                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="show-more-content">
                        <div class="row px-5">
                            <div class="col-md-4 col-sm-12 col-12">
                                <div class="job_ctg ">
                                    <a href="">
                                        <div class="hover-block"></div>
                                        <i class="fa fa-home"></i>
                                        <h3>goverment</h3>
                                        <p>(2414 jobs)</p>
    
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-12">
                                <div class="job_ctg ">
                                    <a href="">
                                        <div class="hover-block"></div>
                                        <i class="fa fa-laptop"></i>
                                        <h3>media &amp; news</h3>
                                        <p>(2142 jobs)</p>
    
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-12">
                                <div class="job_ctg ">
                                    <a href="">
                                        <div class="hover-block"></div>
                                        <i class="fa fa-tree"></i>
                                        <h3>media &amp; news</h3>
                                        <p>(2142 jobs)</p>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>            
                <div id="show-more" class="form-group px-5 mx-auto m-5">
                    <a href="javascript:void(0)" class=" btn_2 px-5 py-2"><i class="fa fa-arrow-right"></i> View More</a>
                </div>
            </div>
        </div>
       
        <div class="container-fluid px-5 bg_grow">
            <div class="row px-5">
                <div class=" col-md-6 col-12 col-sm-12 mx-auto">
                    <h3>Grow next level <span style="color: #F26721;">business</span></h3>
                    <p class="text-secondary">#1 MOST trusted digital marketplace company</p>

                    <p style="text-align: justify;">What do all consultants need? In short, trust. This is achieved with professional presentation and the ability to communicate clearly with and potential clients. Whether you are an accountant.
                    <br>
                    <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusd tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="my-2">
                        <button class=" btn_1 btn btn-lg text-light ">discover more</button>
                    </div>
                </div>
                <div class="col-md-6 col-12 col-sm-12 mx-auto ">
                    <img src="images/riban_10.jpg" class="img-responsive" alt="img" style="width: 100%; height:300px;">
                </div>
            </div>
        </div>
            
        <main class="my-5">
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-md-10 mx-auto" style="border: 1px solid #F26721;">
                        <div class="d-flex justify-content-between align-items-center breaking-news bg-white">
                            <div class="d-flex flex-row flex-grow-1 flex-fill justify-content-center bg-danger text-white px-1 py-2 news"><span class="d-flex align-items-center">Recent Post</span></div>
                            <marquee class="news-scroll" behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                                <span class="dot"></span>
                                <a href="#">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </a>
                                <span class="dot"></span>
                                <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </a>
                                <span class="dot"></span>
                                <a href="#">Duis aute irure dolor in reprehenderit in voluptate velit esse </a>
                            </marquee>
                        </div>
                    </div>
                </div>
            </div>
        </main>    

        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-10 mx-auto ">
                    <div class="row viewmore">
                        @php  
                        $i=0;
                        @endphp
                        @foreach($data as $d) 
                        
                        <div class="col-md-12 col-sm-12 col-12 my-1">
                            <div class="card py-0 card-body job-list">
                                <div class="media align-items-center align-items-lg-start text-center text-lg-left flex-column flex-lg-row">
                                    <div class="mr-4 mt-2 no-gutters"><img class="card-img pl-0 pr-2" src="{{asset('company_logos/'.$d['company_logo'])}}"> </div>
                                    <a href="#" class="position-absolute px-3 text-white" style="bottom:10px; left: 20px; background-color: #F26721;"><small>Post : {{$data[$i]->created_at->format('d-M-Y') }} </small></a>
                                    <div class="media-body mt-1 media-search">
                                        <p class="media-title font-weight-semibold mb-0 mt-1" style="color: #147fa3;"> <span href="#" data-abc="true" style="text-transform: uppercase !important; color: #F26721;">{{$data[$i]["Job_Title"]}} </span> ( {{$data[$i]["Experience"]}} )</p>
                                        <ul class="list-inline list-inline-dotted mb-0">
                                            <li class="list-inline-item mb-0"><a href="#" class="text-dark" data-abc="true">{{$data[$i++]["Salary"]}}</a></li>
                                        </ul>
                
                                        <p class="mb-0">Company Hire Developer </p>
                                        <ul class="list-inline list-inline-dotted mt-2">
                                            <li class="list-inline-item">All items from</li>
                                            <li class="list-inline-item"><a  href= "{{url('details_job_post' ,$d['id'])}}" class=""  data-abc="true" style="color:#F26721;"> <i class="fa fa-share"></i> Details</a></li>
                                        </ul>
                                    </div>
                                    <div class="my-2 ml-lg-3 text-center">
                                        <div class="text-warning"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                                        <div class="text-muted">1985 reviews</div> <a href="{{url('details_job_post' ,$d['id'])}}" class="btn mt-2 text-white" style="background:#147fa3;"><i class="fa fa-share mr-2"></i>Apply</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        @endforeach    
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="fake-div">
        </div>
        <div class="counter_wrapper jb_cover my-5">
            <div class="counter_overlay"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-12 text-left">
                        <div class="counter_mockup_design text-center">
                            <div class="animation-circle-inverse"><i></i><i></i><i></i></div>
                            <img src="images/riban_12.jpg" class="img-responsive text-center pr-5" alt="img" width=470px; >
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-12 text-center ">
                        
                        <h1 class="text-light">Some Statistical Facts</h1>
                        <div class="d-flex my-4">
                            <div class="col-md-4">
                                <div id="counter">
                                    <div class="counter-value" data-count="2500">0</div>
                                    <p>Applied jobs</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="counter">
                                    <div class="counter-value" data-count="550">0</div>
                                    <p class="">ShortList</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="counter">
                                    <div class="counter-value" data-count="3200">0</div>
                                    <p class="">Post Total Jobs</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="fake-div">
        </div>
        
        <div class="container-fluid" style="">
            <div class="row p-5">
                <div class="col-md-6 col-sm-12 col-12 p-5">
                    <h2 class="">Top Hiring <span style="color: #F26721;">Companies</span></h2>
                    <p class="" style="text-align: justify;">What do all consultants need? In short, trust. This is achieved with professional presentation and the ability to communicate. Clearly with existing and potential clients.ll consultants need? In short, trust. This is achieved with Whether you Whether you are an accountant What do all consultants need? In short, trust. This is achieved with professional presentation and the ability to communicate. Clearly with existing and potential clients.ll consultants need? In short, trust. This is achieved with Whether you Whether you are an accountant.</p>
                    <div class="mt-4">
                        <button class=" btn_1 text-light btn-md px-5 py-2 " href="#myCarousel" data-slide="prev"><i class="fa fa-arrow-left"></i></button>
                        <button class=" btn_1 text-light btn-md px-5 py-2 " href="#myCarousel" data-slide="next"><i class="fa fa-arrow-right"></i></button> 
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-12 px-5">
                    <div class="container-fluid">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- The slideshow -->
                            <div class="carousel-inner">
                                <div class="carousel-item active" style="height: 100%;">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <img
                                                src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                                class="card-img-top"
                                                alt="..."
                                                />
                                                <div class="card-body text-center">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn_2 form-control">Button</a>
                                                </div>
                                            </div>
                                        </div>
                            
                                        <div class="col-md-6 d-lg-block">
                                            <div class="card">
                                                <img
                                                src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                                class="card-img-top"
                                                alt="..."
                                                />
                                                <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn_2 form-control">Button</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item" style="height: 100%;">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <img
                                                src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                                class="card-img-top"
                                                alt="..."
                                                />
                                                <div class="card-body text-center">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn_2 form-control">Button</a>
                                                </div>
                                            </div>
                                        </div>
                            
                                        <div class="col-md-6  d-lg-block">
                                            <div class="card">
                                                <img
                                                src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                                class="card-img-top"
                                                alt="..."
                                                />
                                                <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn_2 form-control">Button</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item" style="height: 100%;">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-12">
                                            <div class="card">
                                                <img
                                                src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                                class="card-img-top"
                                                alt="..."
                                                />
                                                <div class="card-body text-center">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn_2 form-control">Button</a>
                                                </div>
                                            </div>
                                        </div>
                            
                                        <div class="col-md-6  d-lg-block">
                                            <div class="card">
                                                <img
                                                src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                                class="card-img-top"
                                                alt="..."
                                                />
                                                <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn_2 form-control">Button</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div> 
        
        <div class="container-fluid py-5" style="background:  #e2e2e2;">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="pb-2 text-capitalize font-weight-bold">Choose Pricing <span style="color: #F26721;">Plans</span></h2>
                    <p class="pb-4 text-secondary">Your next level Product developemnt company assets</p>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    
                        <div class="pricing-wrapper clearfix">
                            <!-- Titulo -->

                            @php  
                            $i=0;
                            @endphp
                            @foreach($plandata as $d) 
                            <div class="pricing-table">
                                <h3 class="pricing-title">{{$plandata[$i]["plan_type"]}}</h3>
                                <div class="price">{{$plandata[$i]["plan_prize"]}}<sup>/ mes</sup></div>
                                <!-- Lista de Caracteristicas / Propiedades -->
                                <ul class="table-list">
                                    <li>{{$plandata[$i]["number_post"]}}<span> Post</span></li>
                                    <li>{{$plandata[$i]["duration_Time"]}}<span> Duration Time</span></li>
                                    <li>{{$plandata[$i]["plan_prize"]}}<span> plan_prize </span></li>
                                    <li>{{$plandata[$i]["featured"]}}  <span class="unlimited"> Featured</span></li>
                                    <li>Cuentas de correo <span class="unlimited"> ilimitadas</span></li>
                                    <li>CPanel <span>incluido</span></li>
                                </ul>
                                <!-- Contratar / Comprar -->
                                <div class="table-buy">
                                    <p>{{$plandata[$i++]["plan_prize"]}}<sup>/ mes</sup></p>
                                    <a href="{{url('add_cart' ,$d['id'])}}" class="pricing-action ">Add Cart</a>
                                </div>
                            </div>
                            @endforeach  

                        </div>
                    </div>
                </div>
            </div>
            
            <!-- <div class="container">
                <div class="row">
                  <div class="col-md-4 my-4">
                    <div class="p-4 py-5 text-center">
                      <b class="text-uppercase text-secondary">Startup</b>
                      <h1 class="font-weight-bold my-4" style="color: #F26721;">$24.5</h1>
                      <p class="text-secondary">5 Jobs Posting</p>
                      <p class="text-secondary">2 Featured Jobs</p>
                      <p class="text-secondary">10 Days Duration</p>
                      <p class="text-secondary">24/7 Support</p>
            
                      <a href="#" class="btn px-5 py-2 text-white mt-4" style="border-radius: 30px; background-color: #147fa3;">Get Started</a>
                    </div>
                  </div>
                  <div class="col-md-4 my-4">
                    <div class="p-4 py-5 text-center bg-light">
                      <b class="text-uppercase text-secondary">Recommended</b>
                      <h1 class="font-weight-bold my-4" style="color: #F26721;">$49.5</h1>
                      <p class="text-secondary">5 Jobs Posting</p>
                      <p class="text-secondary">5 Featured Jobs</p>
                      <p class="text-secondary">15 Days Duration</p>
                      <p class="text-secondary">24/7 Support</p>
            
                      <a href="#" class="btn px-5 py-2 text-white mt-4" style="border-radius: 30px; background-color: #147fa3;">Get Started</a>
                    </div>
                  </div>
                  <div class="col-md-4 my-4">
                    <div class="p-4 py-5 text-center">
                      <b class="text-uppercase text-secondary">Ebterprise</b>
                      <h1 class="font-weight-bold my-4" style="color: #F26721">$199.5</h1>
                      <p class="text-secondary">Unlimited Domain</p>
                      <p class="text-secondary">8 Featured Jobs</p>
                      <p class="text-secondary">25 Days Duration</p>
                      <p class="text-secondary">24/7 Support</p>
            
                      <a href="#" class="btn px-5 py-2 text-white mt-4" style="border-radius: 30px; background-color: #147fa3;">Get Started</a>
                    </div>
                  </div>
                </div>
            </div> -->

        </div>

        <div class="container-fluid p-5">
            <div class="row px-5">
                <div class="col-md-6 col-sm-12 col-12">
                    <img src="images/riban_17.jpg" alt="img" style="width: 100%;"> 
                </div>
                <div class="col-md-6 col-sm-12 col-12">
                    
                    <h3 class="">Our Job <span style="color: #F26721;">Review</span></h3>
                    <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt </p>
          
                    <div id="myCarousel_2" class="carousel slide" data-ride="carousel">
                    
                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active" style="width: 100%;">
                            <img src="https://mdbootstrap.com/img/new/slides/041.jpg" alt="Los Angeles"  height="400px">
                            </div>
                            <div class="carousel-item">
                            <img src="https://mdbootstrap.com/img/new/slides/042.jpg" alt="Chicago"  height="400px">
                            </div>
                            <div class="carousel-item">
                            <img src="https://mdbootstrap.com/img/new/slides/043.jpg" alt="New York"  height="400px">
                            </div>
                        </div>
                    </div>

                    <div class="my-2">
                        <button class=" btn_1 text-light btn-md px-5 py-2 " href="#myCarousel_2" data-slide="prev"><i class="fa fa-arrow-left"></i></button>
                        <button class=" btn_1 text-light btn-md px-5 py-2 " href="#myCarousel_2" data-slide="next"><i class="fa fa-arrow-right"></i></button> 
                    </div>
                        
                   
                </div>
            </div>
        </div>

        <div class="container-fluid  my-5 text-center" style="background:  #e2e2e2; padding: 120px;">
            <h2 class="pb-2">From Our <span style="color: #F26721;">Blog</span></h2>
            <p class="pb-5 text-secondary">Your next level Product developemnt company assets</hp> 
            <div class="row pt-3">
                <div class="col-md-4 col-sm-12 col-12  cardo">
                    
                    <div class="card p-4 " style=" background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
                        <h3 class="card-title text-center text-light">Request Call Back</h3>
                        <div class="card-body text-center">
                            <p class="card-title text-light py-2">What do all consultants need? In short  with professional and the ability to communicate. and the ability to communicate.</p>
                            <div class="card-title pt-2">
                                <div class="form-group">
                                    <input class="form-control text-center" type="text" placeholder="How Can we help you?">
                                </div>
                            </div>
                            <div class="card-title pt-2">
                                <div class="form-group">
                                    <input class="form-control text-center" type="text" placeholder="Your name">
                                </div>
                            </div>
                            <div class="card-title pt-2">
                                <div class="form-group">
                                    <input class="form-control text-center" type="text" placeholder="Email">
                                </div>
                            </div>
                            <div class="card-title pt-2">
                                <div class="form-group">
                                    <button  class="btn_1 btn form-control text-center" style="background: rgb(248, 167, 15)"><i class="fa fa-send"></i></button>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="images/riban_18.jpg" alt="Card image cap" style="height: 200px;">
                        <div class="card-body px-3 text-left">
                            <h5>FEB 19, 2021</h5>
                        <p class="card-text text-secondary">Want To Be An Ace ? Try 
                            <br>Travelling The World</p>
                        </div>
                    </div>
                    <h5 style="padding-top: 20px; text-align: center;">Frequently Asked Question?</h5>
                    <div id="accordion" class="accordion mt-3">
                        <div class="card mb-0">
                            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <a class="card-title">
                                Is there any auto-renew subscription?
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordion" >
                                <div class="card-body">This is Photoshop's version of LoremProin gravida nibh vel velit auctor Ipsum. Proin gravida nibh vel velit auctor aliquet....
                                </div>
                            </div>
                            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                <a class="card-title">
                                How many sites can I use my themes on?
                                </a>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion" >
                                <div class="card-body">This is Photoshop's version of LoremProin gravida nibh vel velit auctor Ipsum. Proin gravida nibh vel velit auctor aliquet....
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-md-4 col-sm-12 col-12 cardo">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top" src="images/riban_19.jpg" alt="Card image cap" style="height: 200px;">
                        <div class="card-body px-3 text-left">
                            <h5>MAR 19, 2021</h5>
                            <p class="card-text text-secondary">Hey Seeker, It’s Time To Try 
                            <br>Travelling The World</p>
                        </div>
                    </div>
                    <h5 style="margin-top: 20px; text-align: center;">Frequently Asked Question?</h5>
                    <div id="accordion" class="accordion mt-3">
                        <div class="card mb-0">
                            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                <a class="card-title">
                                Is there any auto-renew subscription?
                                </a>
                            </div>
                            <div id="collapseFour" class="collapse" data-parent="#accordion" >
                                <div class="card-body">This is Photoshop's version of LoremProin gravida nibh vel velit auctor Ipsum. Proin gravida nibh vel velit auctor aliquet....
                                </div>
                            </div>
                            <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                <a class="card-title">
                                How many sites can I use my themes on?
                                </a>
                            </div>
                            <div id="collapseFive" class="collapse" data-parent="#accordion" >
                                <div class="card-body">This is Photoshop's version of LoremProin gravida nibh vel velit auctor Ipsum. Proin gravida nibh vel velit auctor aliquet....
                                </div>
                            </div>
                        
                        </div>
                    </div> 
                </div>

            </div>
        </div>

        <div class="container-fluid py-5">
            <div class="row mt-5">
                <div class="col-md-6 mx-auto text-left px-5">
                    <h3>Looking For A <span style="color: #F26721;">Job</span></h3>
                    <p class="text-secondary">Your next level Product developemnt company assets</p>

                </div>

                <div class="col-md-2 mx-auto text-right px-0">
                    <div class="form-group px-5">
                        <button href="#" class="btn_2 px-5 py-2  text-light" style="text-decoration: none;">Submit</button>
                    </div> 
                </div>
                
            </div>
            <div class="" style="height: 2px; background:  #a3a2a2;"></div> 
        </div>

        
       

        <!--------------------------------- footer-------------------------->

        @include('layout.footer')

        <!-- end footer-------------------------------------------------- -->
    
    </div> 

    <script src="{{asset('css3-preloader-transition-finish/js/main.js')}}"></script>

    <script>



     // for home***************************
        $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
        $(e.target)
            .prev()
            .find("i:last-child")
            .toggleClass("fa-minus fa-plus");
        });

        $("#accordion_2").on("hide.bs.collapse show.bs.collapse", e => {
        $(e.target)
            .prev()
            .find("i:last-child")
            .toggleClass("fa-minus fa-plus");
        });

        // readmore
        $('#show-more-content').hide();

        $('#show-more').click(function(){
            $('#show-more-content').show(300);
            $('#show-less').show();
            $('#show-more').hide();
        });

        $('#show-less').click(function(){
            $('#show-more-content').hide(150);
            $('#show-more').show();
            $(this).hide();
        });
        
        // couter number***************************
        var a = 0;
        $(window).scroll(function() {

        var oTop = $('#counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
            $('.counter-value').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                countNum: countTo
                },

                {

                duration: 2000,
                easing: 'swing',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }

                });
            });
            a = 1;
        }

    });


    </script>    

   
    


  



    
    
</body>
</html>