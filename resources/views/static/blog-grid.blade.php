<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BlogList</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
   

    <style>
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }

        /* pagination------------------ */
       

        .wrapper {
            margin-top: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .page-link {
            font-size: 20px;
            position: relative;
            display: block;
            color: #147fa3 !important;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #F26721 !important
        }

        .page-link:hover {
            z-index: 2;
            color: #fff !important;
            background-color: #F26721;
            border-color: #147fa3;
        }

        .page-link:focus {
            z-index: 3;
            outline: 0;
            box-shadow: none
        }
    </style>

</head>
<body>

    <section style="background:#808080;">

         @include('layout.navbar')

        <div class="container py-5">
            <div class="text-center m-5">
            <h3 class="text-capitalize font-weight-bold mt-5" style="color: white;">Our Latest <span style="color:#F26721">Blogs</span></h3>
            </div>
            <div class="row ">
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/1153369/pexels-photo-1153369.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/545032/pexels-photo-545032.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/310983/pexels-photo-310983.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/1153369/pexels-photo-1153369.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/545032/pexels-photo-545032.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/310983/pexels-photo-310983.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/1153369/pexels-photo-1153369.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/545032/pexels-photo-545032.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12 p-4">
                    <div class="border">
                        <div class="position-relative w-100" style="height: 250px;background-image: url(https://images.pexels.com/photos/310983/pexels-photo-310983.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500); background-size: cover; background-position: center;">
                            <div class="position-absolute bg-dark" style="opacity: .3; top: 0; left:0; right: 0; bottom: 0;"></div>
                            <div class="position-absolute text-white d-flex flex-column justify-content-center align-items-center rounded-circle" style="top:10px; right:10px; width: 50px; height: 50px; background-color: #F26721;">
                            <small>27</small>
                            <small><b>MAR</b></small>
                            </div>
                            <a href="#" class="position-absolute px-3 py-1 text-white" style="bottom:10px; left: 10px; background-color: #147fa3;"><small>PHOTOS</small></a>
                        </div>
                        <div class="px-3 pt-4 pb-3">
                            <a href="#" class="d-inline-block"><h4 class="text-dark" style="font-weight: 600; font-size: 1.1rem;">Lorem ipsum dolor sit amet.</h4></a>
                            <p class="tex-secondary">asperiores dolore explicabo aut excepturi aliquam nam?</p>
                            <div class="d-flex mt-4">
                            <div class="d-flex align-items-center mr-4">
                                <small class="mt-1" style="color: #F26721"><i class="fa fa-calendar"></i>&nbsp;&nbsp; min ago</small>
                            </div>
                            <div class="d-flex align-items-center">
                                <small class="mt-1" style="color: #147fa3;"><i class="fa fa-user"></i>&nbsp;&nbsp;comments</small>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item"><a class="page-link" href="#">6</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </section>
   

    @include('layout.footer')
      
</body>
</html>