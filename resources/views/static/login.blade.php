<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}" ></script>
    <link rel="stylesheet" type="text/css" href="{{asset('font/flaticon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
    
    <style>

         /* toaster message show */
         #toast-container{position:fixed;z-index:999999;pointer-events:none}

         
       
        #toast-container>div{position: fixed;
        z-index: 999;
        height: 4em;
        width: 100%;
        overflow: show;
        margin: 0 auto;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        }
        /* toaster message show end */

        .login_class{
            float: left;
            width:100%;
            background-image: url(images/riban_27.jpg);
            height: 100%;
            background-position: center 0;
            background-size: cover;
            position: relative;
            min-height: 600px;
            text-align: center;
            padding-top: 60px;

        }
        .login_form h4 {
            font-size: 26px;
            text-transform: capitalize;
            padding-bottom: 30px;
        }
        .form-group {
            padding-top: 15px;
        }
        .form-control{
            border-radius: 0px;
            height: 45px;
        }
        .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 0px solid black;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }
        .form-group a {
            text-decoration: none;
        }
        @media (prefers-reduced-motion: reduce) {
        .form-control {
            transition: none;
        }
    }


         /* data inser loader ------------------------*/
         .loader {
        width: 100%;
        height: 500%;
        /* background: rgba(0, 0, 0, 0.5) !important; */
        display: none;
        position: absolute;
        z-index: 1000;
        }
        .loader-text {
        position: fixed;
        z-index: 999;
        height: 0em;
        width: 15em;
        overflow: show;
        margin: auto;
        top: 5%;
        left: 0;
        bottom: 0;
        right: 0;

        }

        .loading {

            border-radius: 50%;
            z-index: 999;
            height: 2px;
            width: 2px;
            position: fixed;
            margin: auto;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            box-shadow: 10px 0 0 2px #F26721, 7px 7px 0 2px #999, 0 10px 0 2px #999, -7px 7px 0 2px #999, -10px 0 0 2px #999, -7px -7px 0 2px #999, 0 -10px 0 2px #F26721, 7px -7px 0 2px #F26721;
            -webkit-animation: rotate 0.7s steps(8) infinite;
            -o-animation: rotate 0.7s steps(8) infinite;
            animation: rotate 0.7s steps(8) infinite;
  
        }

        @keyframes rotate {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    /* data inser loader End */     


    .form-control:focus {
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
    }
    .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
    }



        .social-btns{
        font-size: 22px;
        list-style: none;
    }

    .social-btns li{
        margin-bottom: 5px;
    }

    .social-btns li i{
        font-size: 22px;
    }



    .social-btns .btn-linkedin {
    background-color: #00629e;
    border-color: #044a75;
    color: #ffffff;
    min-width: 250px;
    }

    .social-btns .btn-linkedin:hover {
    color: #ffffff;
    background-color: #044a75;
    }

    .social-btns .btn-google {
    background-color: #df270b;
    border-color: #a01b03;
    color: #ffffff;
    min-width: 250px;
    }

    .social-btns .btn-google:hover {
    color: #ffffff;
    background-color: #a01b03;
    }





</style>
</head>
<body>

        <div class="loader">
            <p class="text-success loader-text"> Login Successfully Please wait....</p>
            <div class="loading"> 
            </div>
        </div>
    
    <div class="container-fluid text-light" style="background: linear-gradient(to right, #147fa3 10%, #F26721 90%);">
        <div class="row pt-2">
            <div class="col-md-6 col-sm-6 col-6 mx-auto pr-5">
                 <h5 class="text-center pr-5">Login</h5>
            </div>
            <div class="col-md-6 col-sm-6 col-6 pl-5">
                <h6 class="text-center pl-5"><a href="{{url('home')}}" class="text-light" style="text-decoration: none;">Home</a>   /  <a href="{{url('login')}}" class="text-light" style="text-decoration: none;">Login</a></h6>  
           </div>
        </div>
    </div> 

    

    <div class="container pt-5" style="">
        <div class="row"> 
            <div class="col-md-12 mx-auto">
                @if(Session::has('error'))
                    <div class="alert alert-danger text-center">{{Session::get('error')}}</div>
                @endif
                @if(Session::has('message2'))
                    <div class="text-center">
                        <div class="alert alert-success text-center">{{Session::get('message2')}}</div>
                    </div>
                @endif
            </div>
               
        </div>
        <div class="row" style="border: 2px solid rgb(179, 177, 177);">
            <div class="col-md-6 login_class">
                <div>
                    <img src="images/log.png" alt=""><span class="m-3 " style="font-family: Verdana, Geneva, Tahoma, sans-serif; color: black; letter-spacing: 3px; font-weight: 600; font-size: 20px;">Jobs<span style="color: rgb(242, 103, 33);">.</span>Nokri</span>
                    <div class="mt-5 mr-5">
                        <ul class="social-btns center-block">
                            <li><button class="btn btn-linkedin"><i class="fa fa-linkedin pull-left" aria-hidden="true"></i>Sign in with Linkedin</button></li>
                            <li><button class="btn btn-google"><i class="fa fa-google-plus pull-left" aria-hidden="true"></i>Sign in with Google</button></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mx-auto ">
            <!-- @if(Session::has('error'))
                        <div class="alert alert-danger text-center">{{Session::get('error')}}</div>
                @endif -->
                <p id="loginMsg" class="text-center"></p>
                <div class="login_form text-center p-5">
                    <h4>login</h4>
                    @csrf
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Email Address*">
                            <input type="hidden"  id="redirectUrl" value="{{ $_GET['redirectUrl']??'' }}" >
                        </div>
                        
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" placeholder="Password*">
                        </div>
                        <div class="row py-3">
                            <div class="col-md-6">
                                <input type="checkbox" class="" id="remember_me"><span>&nbsp;&nbsp;Remember me</span><br>
                            </div>
                            
                            <div class="col-md-6">
                                <!-- <a href="{{url('forgot')}}" class="forget_password p-2" style="text-decoration: none">
                                    Forgot Password
                                </a> -->
                                <a href="#" class="forget_password p-2" style="text-decoration: none" data-toggle="modal" data-target="#myModal">
                                Forgot Password
                                </a>
                            </div>    
                            
                            <!-- The Modal -->
                            <div class="modal" id="myModal">
                                <div class="modal-dialog">
                                <div class="modal-content">
                             
                                
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                    <h6 class="modal-title">Recovery Password</h6>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    
                                    <!-- Modal body -->
                                    <div class="card login-form">
                                        <div class="card-body">  
                                            <div class="card-text">
                                                <form method="post" action="{{url('forget')}}">
                                                @csrf
                                                    <div class="form-group">
                                                        <div><img src="images/gmail.png" height="60px" width="60px" alt=""></div>
                                                        <!-- <div><i class="fa fa-envelope text-danger" style="font-size:45px"></i></div> -->
                                                        <label for="exampleInputEmail1">Enter your email address and we will send you a link to reset your password.</label>
                                                        <input type="email" name="email" class="form-control" placeholder="Enter your email address" required>
                                                    </div>

                                                    <button type="submit" name="recoverypassword" class="btn btn-primary btn-block" > <strong>Send</strong></button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group px-5">
                            <input type="submit" onclick="loginCheck()" class="btn_2 form-control text-light" value=Login>
                            
                        </div>
                        <div class="account">
                            <p>Don’t have an acount ?<a href="{{url('signup')}}" style="text-decoration: none;">&nbsp; Sign up<i class="fa fa-user ml-1"></i></a></p>
                        </div>

                        <div class="mt-5">
                           
                            @include('layout.social-module')

                        </div>
                    </form>  
                </div>
            </div>
        </div>
    </div>

 


    <script>

        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;
                
                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script>
   
  

    <script src="{{asset('libs/login.js')}}"></script>
        
    
   

    
    
</body>
</html>