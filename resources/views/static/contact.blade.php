<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    

    <style>
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .form-control{
            border-radius: 0px;
            height: 45px;
        }
        .ctn_ico {
            color: #F26721;
            font-size: 70px !important;
        }
        
        .input-container {
        display: -ms-flexbox; /* IE10 */
        display: flex;
        width: 100%;
        margin-bottom: 15px;
        }

        .icon {
        padding: 10px;
        background: linear-gradient(to right, #13bef7 10%, #147fa3 90%);
        color: white;
        min-width: 50px;
        text-align: center;
        }

        .input-field {
        width: 100%;
        padding: 10px;
        outline: none;
        }

        .input-field:focus {
        border: 2px solid dodgerblue;
        }

        /* Set a style for the submit button */
        .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#147fa3 50%, #F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px;
            text-decoration: none;
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none !important;
        }
        .contact_bg{
            background-image: url("images/riban_20.jpg");
            background-position: center 0;
            background-size: cover;
            height: 100%;
            position:relative;
            border-bottom:1px solid #e2e2e2;
            
        }
        .map-container-6{
            overflow:hidden;
            padding-bottom:56.25%;
            position:relative;
            height:0;
        }
        .map-container-6 iframe{
            left:0;
            top:0;
            height:100%;
            width:100%;
            position:absolute;
        }

        /* contact message form */
        /*------------- Form ---------------*/

        .nb-form {
            position: fixed;
            z-index: 9999;
            width: 300px;
            background: #FFF;
            left: 20px;
            bottom: -367px;
            transition: all .8s cubic-bezier(.22, .67, .43, 1.22) .2s;
            border-radius: 10px 10px 0 0;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14),0 1px 5px 0 rgba(0,0,0,0.12),0 3px 1px -2px rgba(0,0,0,0.2);
        }
        .nb-form:hover {
            bottom: 0px;
        }

        /*-- User Icon --*/

        .nb-form .user-icon {
            position: absolute;
            top: -49px;
            right: 12px;
            display: block;
            width: 58px;
            margin: 20px auto 15px;
            border-radius: 100%;
        }

        /*-- Title --*/

        .nb-form .title {
            background: linear-gradient(to right, #13bef7 10%, #147fa3 90%);
            font-size: 16px !important;
            padding: 20px 18px !important;
            color: #fff !important;
            border-radius: 10px 10px 0 0;
        }

        /*-- Text --*/

        .nb-form p {
            font-size: 13px;
            margin: 0;
            padding: 15px;
            color: #666;
        }

        .nb-form p.message {
            margin-left: 7px;
        }

        /*-- Form Elements --*/

        .nb-form form {
            padding: 0 15px 15px 15px;
        }

        .nb-form input,
        .nb-form textarea {
            font-family: Tahoma, Geneva, sans-serif;
            font-size: 12px;
            width: 254px;
            max-width: 254px;
            margin-bottom: 10px;
            margin-left: 7px;
            padding: 6px;
            border: none;
            border-radius: 4px;
            color: #999;
            border-bottom: 1px solid #f0f0f0;
        }

        .nb-form input:focus,
        .nb-form textarea:focus {
            outline: none;
            box-shadow: none;
        }

        .nb-form input[type='submit'] {
            display: block;
            width: 120px;
            margin: 0 auto;
            padding: 0 20px;
            height: 40px;
            line-height: 40px;
            border-radius: 20px;
            cursor: pointer;
            transition: all .4s ease;
            color: #fff !important;
            border: none;
        }

        .nb-form input[type='submit']:hover {
            box-shadow: 0 3px 3px 0 rgba(0,0,0,0.07),0 1px 7px 0 rgba(0,0,0,0.02),0 3px 1px -1px rgba(0,0,0,0.1);
        }

        .nb-form textarea {
            min-height: 110px;
        }


        .nb-form ::-webkit-input-placeholder {
            color: #ccb0b0;
        }

        .nb-form ::-moz-placeholder{
            color: #ccb0b0;
        }

        .nb-form :-ms-input-placeholder {
            color: #ccb0b0;
        }

        .nb-form :-moz-placeholder {
            color: #ccb0b0;
        }

        .nb-form input[type='submit'] {
            background: #F26721;
        }

        @media screen and (max-width: 676px) {
            .nb-form:hover .user-icon {
                display: none;
            }

            .nb-form .message {
                display: none;
            }

            .nb-form form {
                padding-top: 15px;
            }
            .nb-form{
                right: 50%;
                bottom: -320px;
                left: 50%;
                transform: translateX(-50%);
            }
        }

        .home_bg{
            background-image: url("images/riban_43.jpg");
            background-position: center 0;
            background-size: cover;
            height: 100%;
            position:relative;
            border-bottom:1px solid #e2e2e2;
            
        }

       
        
        

    </style>
</head>
<body>

   <section class="home_bg" >

    @include('layout.navbar')

    <div class="container py-5 text-center" style="padding-bottom :120px !important ;">
        <h4 class="pb-2 pt-5">Contact With <span style="color: #F26721;">Us</span></h4>
        <p class="pb-5 text-light">Your next level Product developemnt company assets</p> 
        <div class="row">
            <div class="col-md-4 col-sm-12 col-12 cardo">
                <div class="card" style="width: 100%;">
                    <h4 class="card-title text-center py-3">Contact Us</h4>
                    <div class="card-img-top"  alt="Card image cap">
                        <div class="row py-5">
                            <div class="col-md-12">
                                <i class="fa fa-phone-square ctn_ico"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center py-5">
                        <h6 class="card-title text-secondary">+1800-148-423</h6>
                        <h6 class="card-title text-secondary">+9175-148-124</h6>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-12 cardo">
                <div class="card" style="width: 100%;">
                    <h4 class="card-title text-center py-3">Email</h4>
                    <div class="card-img-top"  alt="Card image cap">
                        <div class="row py-5">
                            <div class="col-md-12">
                                <i class="fa fa-envelope-square ctn_ico"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center py-5">
                        <h6 class="card-title text-secondary">jbdesks@example.com</h6>
                        <h6 class="card-title text-secondary">support@example.com</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12 cardo">
                <div class="card" style="width: 100%;">
                    <h4 class="card-title text-center py-3">Location</h4>
                    <div class="card-img-top"  alt="Card image cap">
                        <div class="row py-5">
                            <div class="col-md-12">
                                <i class="fa fa-map-marker ctn_ico"></i>
                            </div>
                        </div>
                    </div>
                    <div class="card-body text-center py-5">
                        <h6 class="card-title text-secondary">51-Delhi ,india</h6>
                        <h6 class="card-title text-secondary">52B-New Delhi,India</h6>
                    </div>
                </div>
            </div> 
        </div> 

    </div>

   </section>
  

    <div class="container-fluid py-5">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-12 px-0" style="background-color: #e2e2e2;">
                <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 530px">
                    <iframe src="https://maps.google.com/maps?q=new%20delphi&t=&z=13&ie=UTF8&iwloc=&output=embed"
                      frameborder="0" style="border:0" allowfullscreen>
                    </iframe>
                </div>
            </div>
            
            <div class="col-md-6 col-sm-12 col-12 contact_bg">
                <div class="row pl-5 pt-5">
                    <div class="col-md-8">
                        <h3>Get In <span style="color: #F26721;">Touch</span></h3>
                        <p class="text-secondary">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt</p>
                    </div>
                </div>
                <form>
                    <div class="row pl-5">
                        <div class="col-md-5 ">
                            <div class="input-container">
                                <i class="fa fa-user icon pt-3"></i>
                                <input class="input-field" type="text" placeholder="Name*">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="input-container">
                                <i class="fa fa-envelope icon pt-3"></i>
                                <input class="input-field" type="text" placeholder="Email*">
                            </div>
                        </div>
                    </div>
                    <div class="row pl-5 py-3">
                        <div class="col-md-10">
                            <div class="input-container">
                                <i class="fa fa-comment icon pt-3"></i>
                                <textarea class="input-field" type="text" placeholder="Message*" style="height: 200px;"></textarea>
                            </div>
                            <div class="pb-4">
                                <a href="#" class="btn btn-secondary px-5 py-2" style="text-decoration:none;">Send</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- message send -->
    <div class="container">
        <div class="row">
            <!-- Form -->
            <div class="nb-form">
                <p class="title py-4">Send Message<i class="fa fa-telegram pull-left" style="font-size: 40px;"></i></p>
                <img src="https://lh3.googleusercontent.com/-LvTWzTOL4c0/V2yhfueroyI/AAAAAAAAGZM/Ebwt4EO4YlIc03tw8wVsGrgoOFGgAsu4wCEw/w140-h140-p/43bf8578-86b8-4c1c-86a6-a556af8fba13" alt="" class="user-icon">
                <p class="message">This is an awesome example of sticky contact form on right bottom of the page</p>
            
                <form>
                <input type="text" name="cpname" placeholder="Name:" required>
                <input type="email" name="cpemail" placeholder="Email:" required>
                <input type="tel" name="cpphone" placeholder="Phone:" required>
                <textarea name="cpmessage" placeholder="Message:" required></textarea>
                <input type="submit" value="Send message">
                </form>
            </div>
        </div>
    </div>

    
  

    @include('layout.footer')

    
    
</body>
</html>