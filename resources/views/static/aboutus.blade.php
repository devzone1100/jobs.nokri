<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AboutUs</title>
    <link rel="shortcut icon" href="{{asset('images/log.png')}}" type="image/img">
    
   
</head>
    <style>
        .form-control:focus {
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn:focus{
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(247, 245, 245, 0.6);
        }
        .btn_1 {
            border: 0px solid black;
            background: linear-gradient(to right,#F26721 50%, #147fa3 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center;
            font-size: 16px;
            line-height: 30px; 
            color: #fff !important;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_1:hover {
        background-position: left bottom;
        }
        .btn_1 a {
            text-decoration: none;
        }
        .btn_2 {
            border: 1px solid black;
            background: linear-gradient(to right, #147fa3 50% ,#F26721 50%);
            background-size: 200% 100%;
            background-position: right bottom;
            transition: all .5s ease-out;
            text-align: center; 
            color: #ffff;
            transition: all .6s ease-out;
            border-radius:0px
            
        }
        .btn_2:hover {
        background-position: left bottom;
        }

        .para,.para1 {
            text-align: justify;
        }

        #more{
            display:none;
        }
        .interinner {
        padding: 50px 15px 60px;
        border-radius: 30px;
        transition: .5s ease;
        }

        /* meet our */
        .intro {
        font-size: 16px;
        max-width: 500px;
        margin: 0 auto
        }

        .intro p {
            margin-bottom: 0
        }

        .people {
            padding: 50px 0;
            cursor: pointer
        }

        .item {
            margin-bottom: 30px
        }

        .item .box {
            border-radius: 50%;
            text-align: center;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            height: 280px;
            position: relative;
            overflow: hidden
        }

        .item .cover {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(128, 125, 122, 0.75);
            transition: opacity 0.15s ease-in;
            opacity: 0;
            padding-top: 80px;
            color: #fff;
            text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.15)
        }

        .item:hover .cover {
            opacity: 1
        }

        .item .name {
            font-weight: bold;
            margin-bottom: 8px
        }

        .item .title {
            text-transform: uppercase;
            font-weight: bold;
            color: #bbd8fb;
            letter-spacing: 2px;
            font-size: 13px;
            margin-bottom: 20px
        }

        .social {
            font-size: 35px;
            color: #44a2f6;
            
        }
        

        .social a {
            color: inherit;
            margin: 0 10px;
            display: inline-block;
            opacity: 0.7
        }

        .social a:hover {
            opacity: 1
        }

  

       
    </style>
</head>
<body>
     

    <section style="background:#808080;" >

        @include('layout.navbar')
        <div class="container-fluid px-5" style="padding-top:120px !important ; padding-bottom:120px !important ;">
            <div class="row p-5">
                <div class="col-md-6">
                    <div id="myCarousel_2" class="carousel slide" data-ride="carousel">
                        
                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active" style="width: 100%;">
                            <img src="https://mdbootstrap.com/img/new/slides/041.jpg" alt="Los Angeles" width="1100" height="400px">
                            </div>
                            <div class="carousel-item">
                            <img src="https://mdbootstrap.com/img/new/slides/042.jpg" alt="Chicago" width="1100" height="400px">
                            </div>
                            <div class="carousel-item">
                            <img src="https://mdbootstrap.com/img/new/slides/043.jpg" alt="New York" width="1100" height="400px">
                            </div>
                        </div>
                    </div>
                        
                </div>
                <div class="col-md-6">
                    <h3>Company <span class="text text-danger">Overview</span></h3>
                    <p class="para text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum. Maecenas nisl est, Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet. Nunc sagittis dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc sed do tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p class="para1 text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum Donec vitae dui eget tellus gravida venenatis<span id="dots">.........</span><span id="more">vitae dui eget tellus gravida venenatis.Nunc sagittis dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc sed.</span></p>
                    <div class="mt-4">
                        <button class="btn_1 text-light btn-md px-5 py-2 " href="#myCarousel_2" data-slide="prev"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn_1 text-light btn-md px-5 py-2 " href="#myCarousel_2" data-slide="next"><i class="fa fa-arrow-right"></i></button> 
                    </div>
                </div>
            </div>
        </div>

    </section>   
    

    <section class="about-us p-5 " id="about-us">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-md-6 p-5">
                    <h2 style="color: #F26721">Know More About Us</h2>
                    <hr>
                    <p class="para">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore etae magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <button type="button" class="btn btn_1">Let's Know More</button>
        
                </div>
                <div class="col-md-6 mt-5">
                    <img src="http://themebubble.com/demo/marketingpro/wp-content/uploads/2016/10/seo-slide.png "alt="" style="width: 100%;">
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid px-5">
        <div class="row p-5">
            <div class="col-md-12 p-5">
                <h3 class="text text-center my-5">WHY CHOOSE <span class="text text-danger">US ?</span></h3>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                <div class="container-fluid px-5">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active" style="height: 100%;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body text-center">
                                            <h5 class="card-title">Card title 1</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-3 d-none d-lg-block">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body">
                                            <h5 class="card-title">Card title 2</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body text-center">
                                            <h5 class="card-title">Card title 1-a</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-3 d-none d-lg-block">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body">
                                            <h5 class="card-title">Card title 2-a</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item" style="height: 100%;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body text-center">
                                            <h5 class="card-title">Card title 3</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-3 d-none d-lg-block">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body">
                                            <h5 class="card-title">Card title 4</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body text-center">
                                            <h5 class="card-title">Card title 3-a</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-3 d-none d-lg-block">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body">
                                            <h5 class="card-title">Card title 4-a</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item" style="height: 100%;">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body text-center">
                                            <h5 class="card-title">Card title 5</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-3 d-none d-lg-block">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body">
                                            <h5 class="card-title">Card title 6</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/181.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body text-center">
                                            <h5 class="card-title">Card title 5-a</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                        
                                    <div class="col-md-3 d-none d-lg-block">
                                        <div class="card">
                                            <img
                                            src="https://mdbootstrap.com/img/new/standard/nature/182.jpg"
                                            class="card-img-top"
                                            alt="..."
                                            />
                                            <div class="card-body">
                                            <h5 class="card-title">Card title 6-a</h5>
                                            <p class="card-text">
                                                Some quick example text to build on the card title and make up the bulk
                                                of the card's content.
                                            </p>
                                            <a href="#!" class="btn btn_2 form-control">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-md-12 text-center">
                <button class="btn_1 text-light btn-md px-5 py-2 " href="#myCarousel" data-slide="prev"><i class="fa fa-arrow-left"></i></button>
                <button class="btn_1 text-light btn-md px-5 py-2 " href="#myCarousel" data-slide="next"><i class="fa fa-arrow-right"></i></button> 
            </div>
        </div>
    </div>
  
    <div class="team-grid">
        <div class="container-fluid" style="margin-top: 100px;">
            <div class="my-5">
                <h2 class="text-center">Meet Our Team</h2>
                <p class="text-center">A Bootstrap 4 example layout with an Meet our team section and team members</p>
            </div>
            <div class="row people d-flex justify-content-center">
                <div class="col-md-4 col-lg-3 item">
                    <div class="box" style="background-image:url('images/riban_40.jpg')">
                        <div class="cover">
                            <h3 class="name">Tim Cook</h3>
                            <p class="title">Android Developer</p>
                            <div class="social"><a href="#"><i class="fa fa-facebook-official"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 item">
                    <div class="box" style="background-image:url('images/riban_38.jpg')">
                        <div class="cover">
                            <h3 class="name">Michael Clarke</h3>
                            <p class="title">Cricker</p>
                            <div class="social"><a href="#"><i class="fa fa-facebook-official"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3 item">
                    <div class="box" style="background-image:url('images/riban_39.jpg')">
                        <div class="cover">
                            <h3 class="name">Carlos vento</h3>
                            <p class="title">Business Analyst</p>
                            <div class="social"><a href="#"><i class="fa fa-facebook-official"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a id="button_up" style="text-decoration: none;"></a>

    @include('layout.footer')

    

    


<script>
    function fun1 () {
        var dots = document.getElementById("dots");
        var mt = document.getElementById("more")
        var bt = document.getElementById("btn");
        if(dots.style.display === "none"){
            dots.style.display = "inline";
            mt.style.display ="none";
            bt.innerHTML = "Read More"
        }
        else{
            dots.style.display = "none";
            mt.style.display ="inline";
            bt.innerHTML = "Read Less"

        }
    }

 

</script>
</body>
</html>