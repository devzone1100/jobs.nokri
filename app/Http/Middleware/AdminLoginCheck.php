<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Cookie;                                                

class AdminLoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $session_Adminemail = Session::get('Adminemail');
        $cookie_Adminemail = Cookie::get('Adminemail');
        if((!$session_Adminemail) && (!$cookie_Adminemail)){

            $notification = array(
                'message' => 'Need to login Enter Dasbhoard, Please login !',
                'alert-type' => 'error'
            );
            return redirect('Admin-Login')->with($notification);  
        }
        return $next($request);


    }
}
