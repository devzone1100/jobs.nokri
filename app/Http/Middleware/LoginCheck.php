<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
// use Request;
use Session;
use Cookie;                                                

class LoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   
        // print_r(Session::get('useremail')); 
        // print_r(Cookie::get('useremail')); die;
       
        $session_email = Session::get('useremail');
        $cookie_email = Cookie::get('useremail');
        if( empty($session_email) && empty($cookie_email) ){

            $notification = array(
                'message' => 'Need to login Enter Page, Please login !',
                'alert-type' => 'error'
            );

            if($checkEmail['UserType'] = 'company'){
                $redirect_url = $request->url();
                return redirect('login?redirectUrl='.$redirect_url)->with($notification);

            }
           
            
            return redirect('login')->with($notification);  

            // $redirect_url = $request->url();
            // return redirect('login?redirectUrl='.$redirect_url)->with($notification);
            // return redirect('login')->with($notification);  
        }
        return $next($request);


    }
}
