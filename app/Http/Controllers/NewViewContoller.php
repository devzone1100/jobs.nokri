<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\RegisterModel;
use App\Models\AdminUserModel;
use App\Models\JobPostModel;
use App\Models\AppliedUser;
use App\Models\DetailsPayment;
use App\Models\AddplansModel;
use App\Models\VerifyEmail;
use App\Models\ResetPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Session;
use Cookie;
use File;




class NewViewContoller extends Controller
{


        // **************************************************    USER-CODE  ********************************************************************
        // ****************************************************************************************************************************************



        //***************** Sign-Up-Code *******************************************
 

        public function registersave(Request $request)
        {
            $validator = Validator::make($request->all(),[
                // 'email'=> 'required|unique:register_models',
                'password'=> 'required',
                'first_name'=> 'required',
                'last_name'=> 'required',
                'phone'=> 'required',
                'user_type'=> 'required',
            ]);
            if($validator->fails()){
            return $this->send_error_message($validator->errors()->first());
            }
        
            # check user if match with database user
            $register = RegisterModel::where('email', $request->email)->get();

            # check if email is more than 1
            if(sizeof($register) > 0){
                # tell user not to duplicate same email

                $notification = array(
                    'message' => 'This user already signed up !',
                    'alert-type' => 'error'
                );
                return redirect('signup')->with($notification);   
            }
            
            $register = new RegisterModel();
            $register->Firstname = $request->first_name;
            $register->Lastname = $request->last_name;           
            $register->email = $request->email;
            $register->phone_no = $request->phone;
            // $register->password = md5($request->password);
            $register->password = password_hash($request->password, PASSWORD_DEFAULT);
            if($request->user_type == 'user')
            {
                
                $register->gender = $request->gender;
            }
            $register->UserType = $request->user_type;
            $register->save();

            // $insertQuery = RegisterModel::create($data);
            // echo $insertQuery;

            // Verification Section
            $ve = new VerifyEmail();
            $token = rand(5,10).time();
            $ve->email= $request->email;
            $ve->token = $token;
            $ve->save();

            // mail send **********

            $mail_array = array('email'=>$request->email,'token'=>$token);
            Mail::send('email.register_mail',$mail_array,function($m) use ($mail_array){
                $m->to($mail_array['email'])->subject('Registration Done');
                $m->from('xyz@gmail.com');

            });

            $resp['status']=true;
            $resp['message']='Data inserted Successfully';
            // return $this->send_success_message('Data inserted Successfully');

            $notification = array(
                'message' => 'Your Account Created Successfully , Check Your Mail For Verification',
                'alert-type' => 'success'
            );
            return redirect('signup')->with($notification); 
    
        }
  
        //***************** Email-Verification-Code ********************************
    
        public function verify_email($token)
        {
            
            $getVerificationModalData = VerifyEmail::where('token',$token)->first();
            if($getVerificationModalData){
                $updateRegisterStatus = RegisterModel::where('email',$getVerificationModalData->email)->update(['status'=>1]);
                if($updateRegisterStatus){
                    VerifyEmail::where('token',$token)->delete();
                    $notification = array(
                        'message' => 'Verification done successfully , Now You Can Login !',
                        'alert-type' => 'success'
                    );
                    return redirect('login')->with($notification); 

                }
                $notification = array(
                    'message' => 'Verification not done successfully',
                    'alert-type' => 'error'
                );
                return redirect('signup')->with($notification);

            
            }
            
            
        }

        //****************** Email-Verification-End **********************************


        //****************** Forget-Password-Code*************************************

        public function forget(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'email'=>'required',]);
                if($validator->fails())
                {
                    $notification = array(
                        'message' => 'This email is not Register , Please enter a valid email !!',
                        'alert-type' => 'error'
                    );
                    return redirect('login')->with($notification); 
                   
                    
                }
            $checkEmail = RegisterModel::where('email',$request->email)->first();
            if($checkEmail){
                $ve = new ResetPassword();
                $token = rand(5,10).time();
                $ve->email = $request->email;
                $ve->token = $token;
                $ve->save();
                $mail_array = array('email'=>$request->email,'token'=>$token);
                    Mail::send('email.reset_password',$mail_array,function($m) use ($mail_array){
                        $m->to($mail_array['email'])->subject('Password Reset');
                        $m->from('xyz@gmail.com');
                    });
                    $notification = array(
                        'message' => 'Mail Send Successfully , Check Your Mail received Reset Password Link !!',
                        'alert-type' => 'success'
                    );
                    return redirect('login')->with($notification); 
                  return false;
            }
            $notification = array(
                'message' => 'This email is not Register , Please enter a valid email !!',
                'alert-type' => 'error'
            );
            return redirect('login')->with($notification); 
           
        }

        public function reset_password($id)
        {
            $checkToken = ResetPassword::where('token',$id)->first();
            if(!$checkToken){
            return '<h1 style="text-align:center;margin-top:20%;color:red;font-family:sans-serif";>Oops.. Link Will be Expired !! Please Resend .. </h1>';
                return redirect('login');
            }
            return view('forgetpassword')->with('token',$id);
        }

        public function forget_pass(Request $request)
        {
        
            $checkToken = ResetPassword::where('token',$request->passwordToken)->first();

            if(!$checkToken){
                $notification = array(
                    'message' => 'Link expire. Please try again with new Link !!',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification); 
            
            }else{
        
                $updatePassword = RegisterModel::where('email',$checkToken->email)->update(['password'=>Hash::make($request->password)]);
                ResetPassword::where('token',$request->passwordToken)->delete();
                    
                $notification = array(
                    'message' => 'Password Reset Successfully , Now You Can Login !!',
                    'alert-type' => 'success'
                );
                return redirect('login')->with($notification); 
                return false;
            }
        }

        //****************** Forget-Password-Code-End ********************************



        //****************** Edit-Update-Delete-Code *********************************

        public function editDetailsregister($id) 
        {
            $getParticularData = RegisterModel::where('id',$id)->first();
            if ($getParticularData) {
            
                return view('Admin.edit_page_register')->with('editDetails',$getParticularData);

                session()->flash('message2', 'Data Edit Successfully');
                return redirect('registerData');
            
            }
            
            session()->flash('error', 'Details are not in database');
            return redirect('registerData');
            
        }

        public function updateFormregister(Request $request)
        {
        
            $getParticularData = RegisterModel::where('id',$request->id_)->first();
            $getParticularData->Firstname = $request->first_name;
            $getParticularData->Lastname = $request->last_name;
            $getParticularData->email = $request->email;
            // $getParticularData->phone_no = $request->phone;
            $getParticularData->password = $request->password;
            // $getParticularData->gender = $request->gender;
            $getParticularData->status = $request->status;
            $getParticularData->save();
            
        
            session()->flash('message2', 'Data Update Successfully');
            return redirect('registerData');
        }

        public function deleteDetailsregister($id)
        {
            $getParticularData = RegisterModel::where('id',$id)->first();
            if ($getParticularData->delete()) {
            
                $success = true;
                $message = "User deleted successfully";
                
            } else {
                $success = true;
                $message = "User not found";
                return redirect('registerData');
            }

            return response()->json([
                'success' => $success,
                'message' => $message,
            ]);
        }

        //****************** Edit-Update-Delete-Code-End ****************************



        
        // **********Login-ViewDashboard-ProfileImage-Logout-Code***********************

        public function index()
        {
            return view('login');
        }

        public function viewPage(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'email'=> 'required|unique:register_models',
                'password'=> 'required',
            ]);
            if($validator->fails()){
            return $this->send_error_message($validator->errors()->first());
            }
            $pass = $request->password;
            $data  = $request->all();
            unset($data['_token']);
            unset($data['password']);
            $data['password'] = Hash::make($pass);
        
            // file upload
            if($request->hasFile('profile_img')){
                $image = $request->profile_img;
                $extension = $image->getClientOriginalExtension();
                $newName = rand().time().'.'.$extension;
                $destination = public_path('profile_images');
                $image->move($destination,$newName);
                unset($data['profile_img']);
                $data['profile_image'] = $newName;
            }
        
        
        }

        public function newGetData()
        {
            
            $getData = RegisterModel::all();
            return view('Admin.registerData')->with('data',$getData); 
            // return view('signup');
        }

        public function Login_(Request $request)
        {
            
            $checkEmail = RegisterModel::where('email',$request->email)->first();
            if($checkEmail){
                // check verification status
                if($checkEmail->status != 1){
                    return $this->send_error_message('Email is not verified , Please Check Your Mail For Verification'); 
                }
                // password check
                $checkPassword = Hash::check($request->password,$checkEmail->password);
                if($checkPassword){

                    
        
                
                    if($request->remember_me == 1){
                        // Cookie
                    Cookie::queue(Cookie::make('useremail',$checkEmail->email,20));
                    Cookie::queue(Cookie::make('userid',$checkEmail->id,20));
                    Cookie::queue(Cookie::make('UserType',$checkEmail->UserType,20));
                    }
                    // else{
                    Session::put('useremail',$checkEmail->email);
                    Session::put('userid',$checkEmail->id);
                    Session::put('UserType',$checkEmail->UserType);
                    Session::put('profile_image',$checkEmail->profile_image);

                    // }
    
                    
                        $data=array(
                            "status" => "true",
                            "data" => $checkEmail['UserType'],
                            "message" => "Login Successfull",
                            "redirectUrl"=> $request->redirectUrl ??""
                        );
                        return $data;


                    // return $this->send_success_message('Login Successfull');

                }
                return $this->send_error_message('Incorrect Password');
            }
            return $this->send_error_message('Email is not in our records');
        
        }

        // company-dashboard-----------------------------

        public function dashboardView()
        {
        
            if(Session::get('UserType') !== 'company')
            {
                $notification = array(
                    'message' => ' Need Login First Enter Dashboard !',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification);
            }   

            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();
 
            
            // echo Session::get('userid'); die;
            $count = JobPostModel::where('company_id', Session::get('userid'))->count();
            $apllicationcount = AppliedUser::where('company_email', Session::get('useremail'))->count();

            // $paymentDetails = DetailsPayment::all();
            $paymentDetails = DetailsPayment::where('USERID', Session::get('userid'))->first();



            $alldata_arr=array(
                'data'=>$getLoginUserData,
                'countdata' =>  $count,
                'countapplieduser' =>  $apllicationcount,
                'paymentDetailsData' =>  $paymentDetails,
                'profile_data' =>  Session::get('profile_image'),

            );
            return view('company.company-dashboard', $alldata_arr);

        
        }

        // company-dashboard-end----------------------------


        // transaction-invoice-------------------------------------------------------------------

        public function transactionInvoice(Request $request)
        {
        
            if(Session::get('UserType') !== 'company')
            {
                $notification = array(
                    'message' => ' Need Login First Enter Dashboard !',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification);
            }   

            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();
 
            
            // echo Session::get('userid'); die;

            // $paymentDetails = DetailsPayment::all();
            // $paymentDetails = DetailsPayment::where('USERID', Session::get('userid'))->first();
        

            $value = $request->session()->get('useremail');

            $paymentDetails = DB::table('details_payments')
                ->where('USEREMAIL', $value)
                ->get();


            $alldata_arr=array(
              
                'data'=>$getLoginUserData,
                'paymentDetailsData' =>  $paymentDetails,
                'profile_data' =>  Session::get('profile_image'),

            );
            return view('company.transation-invoice', $alldata_arr);
            
        
        }

        // transaction-invoice-End------------------------------------------------------------------



        // edit-company-profile---------------------------------------

        public function editCompanyProfile()
        {
        
            if(Session::get('UserType') !== 'company')
            {
                $notification = array(
                    'message' => ' Need Login First Enter Dashboard !',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification);
            }   

            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();
 
            // echo Session::get('userid'); die;
            $count = JobPostModel::where('company_id', Session::get('userid'))->count();
            $apllicationcount = AppliedUser::where('company_email', Session::get('useremail'))->count();

            $alldata_arr=array(
                'data'=>$getLoginUserData,
                'countdata' =>  $count,
                'countapplieduser' =>  $apllicationcount,
                'profile_data' =>  Session::get('profile_image'),

            );
            return view('company.edit-company-profile', $alldata_arr);

        
        }

        

        // edit-company-profile-end--------------------------------------



        // user-dashboard--------------------------------------------------------

        public function userdashboardView()
        {
            if(Session::get('UserType') !== 'user')
            {
                $notification = array(
                    'message' => ' Need Login First Enter Dashboard !',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification);
            }
            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();

            $applyjobsData = AppliedUser::where('applied_useremail', Session::get('useremail'))->count();
            


            $all_data_arr=array(
                
                'data'=>$getLoginUserData,
                'apply_jobs_data' =>  $applyjobsData,
                'profile_data' =>  Session::get('profile_image'), 

            );

            return view('user.user-dashboard', $all_data_arr);   
        }

        // user-dashboard-end--------------------------------------------------------



        // applied-user_Data--------------------------------------------------------

        public function userAppliedJob(Request $request)
        {
            if(Session::get('UserType') !== 'user')
            {
                $notification = array(
                    'message' => ' Need Login First Enter Dashboard !',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification);
            }
            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();

            $applyjobsData = AppliedUser::all();

            $value = $request->session()->get('useremail');

            $users = DB::table('applied_users')
                ->where('applied_useremail', $value)
                ->get();
            


            $all_data_arr=array(
                'allappliedjob'=> $users,
                'data'=>$getLoginUserData,
                'apply_jobs_data' =>  $applyjobsData,
                'profile_data' =>  Session::get('profile_image'), 

            );

            return view('user.user-applied-job', $all_data_arr);   
        }

        // applied-user_Data-End-------------------------------------------------------



        // edit_profile_Data--------------------------------------------------------

        public function edit_profile_Data()
        {
            if(Session::get('UserType') !== 'user')
            {
                $notification = array(
                    'message' => ' Need Login First Enter Dashboard !',
                    'alert-type' => 'error'
                );
                return redirect('login')->with($notification);
            }
            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();

            $applyjobsData = AppliedUser::where('applied_useremail', Session::get('useremail'))->count();
            


            $all_data_arr=array(
                
                'data'=>$getLoginUserData,
                'apply_jobs_data' =>  $applyjobsData,
                'profile_data' =>  Session::get('profile_image'), 

            );

            return view('user.edit-user-profile', $all_data_arr);   
        }

        // edit_profile_Data------------------------------------------------ 

        public function profile_image_update(Request $request)
        {
            // print_r(Session::get('useremail')); die;
                
            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();
            // file exists code
            $update_data=array();
            if($request->hasFile('pro_img')){
                $exists_image = public_path("/profile_images/".$getLoginUserData->profile_image);
                if(File::exists($exists_image)){
                    File::delete($exists_image);
                }
                $new_image = $request->file('pro_img');
                $new_name = rand().time().'.'.$new_image->getClientOriginalExtension();
                $new_image->move(public_path('/profile_images/'),$new_name);
                $update_data['profile_image']=$new_name;
            }
            $update_data['Firstname']= $request['first_name'];
            $update_data['Lastname']= $request['last_name'];
            $update_data['phone_no']= $request['phone'];

            $updateQuery = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->update($update_data);

            $notification = array(
                'message' => 'Profile Details Change Successfully',
                'alert-type' => 'success'
            );
            // return redirect('company-dashboard')->with($notification);
            return redirect('edit-company-profile')->with($notification);
        }

        public function profile_image_update_User(Request $request)
        {
            // print_r(Session::get('useremail')); die;
                
            $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();
            // file exists code
            $update_data=array();
            if($request->hasFile('pro_img')){
                $exists_image = public_path("/profile_images/".$getLoginUserData->profile_image);
                if(File::exists($exists_image)){
                    File::delete($exists_image);
                }
                $new_image = $request->file('pro_img');
                $new_name = rand().time().'.'.$new_image->getClientOriginalExtension();
                $new_image->move(public_path('/profile_images/'),$new_name);
                $update_data['profile_image']=$new_name;
            }
            $update_data['Firstname']= $request['first_name'];
            $update_data['Lastname']= $request['last_name'];
            $update_data['phone_no']= $request['phone'];

            

            $updateQuery = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->update($update_data);

            $notification = array(
                'message' => 'Profile Details Change Successfully',
                'alert-type' => 'success'
            );
            // return redirect('user-dashboard')->with($notification);
            return redirect('edit-user-profile')->with($notification);
        }

        public function viewProfile()
        {
            return redirect('user-dashboard');
        }

        public function logout()
        {
            // session--------------------
            Session::flush();
            
            // cookie --------------------
            Cookie::queue(Cookie::forget('useremail'));
            Cookie::queue(Cookie::forget('userid'));
            Cookie::queue(Cookie::forget('usertype'));
        
            return redirect('login');
        }

        // **********Login-ViewDashboard-ProfileImage-Logout-Code-End********************

         // job-post-form****************************************

           public function jobpostformView( Request $request)
           {
           
               if(Session::get('UserType') !== 'company')
               {
                   $notification = array(
                       'message' => ' Need Login First Enter job-post !',
                       'alert-type' => 'error'
                   );
                   
                   $redirect_url = $request->url();
                   return redirect('login?redirectUrl='.$redirect_url)->with($notification);
                   // return redirect('login')->with($notification);
               }
   
               $response= Http::get('http://ip-api.com/json/169.149.224.130');
               $location_data=json_decode($response);
   
               $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();
               
               
               $data_arr=array(
                   'data'=>$getLoginUserData,
                   'location_data' =>  $location_data,
                   'profile_data' =>  Session::get('profile_image'),
   
               );
               // return view('company.job-post')->with('data',$getLoginUserData);
               return view('company.job-post', $data_arr);
   
           }
   
        // job-post-form-end****************************************


        // ****************************Sweet-Alert-Code***********************************

        public function send_error_message($message)
        {
            $resp = array();
            $resp['status']=false;
            $resp['message']=$message;
            return $resp;   
        }
        public function send_success_message($message,$data=null)
        {
            $resp = array();
            $resp['status']=true;
            $resp['message']=$message;
            if($data != null){
                $resp['data']=$data;
            }
            return $resp;   
        }

        // ****************************Sweet-Alert-Code-End***********************************



}

  


