<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\AdminUserModel;
use App\Models\RegisterModel;
use App\Models\JobPostModel;
use App\Models\AddplansModel;
use App\Models\AppliedUser;
use App\Models\DetailsPayment;
use App\Models\VerifyEmail;
use Illuminate\Support\Facades\DB;
use App\Models\ResetPassword;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Session;
use Cookie;
use File;



class AdminController extends Controller
{
   
        public function AdminAddPlansInsert(Request $request)
        {
            
            $addplandata = new AddplansModel();
            $addplandata->plan_type = $request->plan_type;
            $addplandata->number_post = $request->number_post;
            $addplandata->duration_Time = $request->duration_Time;
            $addplandata->plan_prize = $request->plan_prize;
            $addplandata->featured = $request->featured;

            // echo "<pre>";
            // print_r($request->all());
            // echo "</pre>";

            $addplandata->save();

            $notification = array(
                'message' => ' Plan Add Done !',
                'alert-type' => 'success'
            );
            
            return redirect('add-plans')->with($notification);
            
        }

        public function addplanData()
        {
            $getData = AddplansModel::all();
            return view('Admin.addplan_Data')->with('data',$getData); 
           
            
        }

        public function addCartView($id, Request $request) {

            $getParticularData = AddplansModel::where('id',$id)->first();

            if(Session::get('UserType') !== 'company')
            {
                $notification = array(
                    'message' => ' Need Login First Then you Use Plan !',
                    'alert-type' => 'error'
                );
                
                $redirect_url = $request->url();
                return redirect('login?redirectUrl='.$redirect_url)->with($notification);
                // return redirect('login')->with($notification);
            } 

            if ($getParticularData) {

                $registerData = RegisterModel::all();
                $getLoginUserData = RegisterModel::where('email',Session::get('useremail'))->orwhere('email',Cookie::get('useremail'))->first();

                $alldata_arr=array(
                    'LoginData' => $getLoginUserData,
                    'register_data' =>  $registerData,
                    'addcartDetails' =>  $getParticularData,
                    'profile_data' =>  Session::get('profile_image'),
                );

                return view('company.add_cart',$alldata_arr);
            }
         
        }

        // ****************************************Select-plans*************************************

            public function SelectPlans ()
            {
                $getData = AddplansModel::all();
                $registerData = RegisterModel::all();
               
                
                $alldata_arr=array(
                    'plandata'=>$getData,
                    'register_data' =>  $registerData,
                    
                    'profile_data' =>  Session::get('profile_image'),
                );
                // print_r(); die;
                return view('company.select_plans', $alldata_arr);


            }


        // ****************************************Select-plans-end*************************************
 
        // **************************************************  ADMIN-CODE  ************************************************************************
        // ****************************************************************************************************************************************

        public function Adminregistersave(Request $request)
        {
            $validator = Validator::make($request->all(),[
                'email'=> 'required',
                'password'=> 'required',
                
            ]);
            if($validator->fails()){
               return $this->send_error_message($validator->errors()->first());
            }
           
    
             # check user if match with database user
             $register = AdminUserModel::where('email', $request->email)->get();
    
            # check if email is more than 1
            if(sizeof($register) > 0){
                # tell user not to duplicate same email
    
                $notification = array(
                    'message' => 'This user already signed up !',
                    'alert-type' => 'error'
                );
    
                return redirect('Adminsignup')->with($notification); 
                
            }
            
            $register = new AdminUserModel();         
            $register->email = $request->email;
            $register->password = $request->password;
          
            $register->save();
    
            // Verification Section
            $ve = new VerifyEmail();
            $token = rand(5,10).time();
            $ve->email= $request->email;
            $ve->token = $token;
            $ve->save();
    
            $resp['status']=true;
            $resp['message']='Data inserted Successfully';
            // return $this->send_success_message('Data inserted Successfully');
    
            $notification = array(
                'message' => 'Your Account Created Successfully , Check Your Mail For Verification',
                'alert-type' => 'success'
            );
            return redirect('Adminsignup')->with($notification); 
     
        }
    
     

        public function AdminLogin_(Request $request)
        {
            
            $checkEmail = AdminUserModel::where('email',$request->email)->first();
            if($checkEmail){
                // check verification status
                
                // password check
                $checkPassword = AdminUserModel::where('password',$request->password)->first();
                if($checkPassword){

                    if($request->remember_me == 1){
                        // Cookie
                    Cookie::queue(Cookie::make('Adminemail',$checkEmail->email,20));
                    Cookie::queue(Cookie::make('Adminid',$checkEmail->id,20));
                 
                    }else{
                    Session::put('Adminemail',$checkEmail->email);
                    Session::put('Adminid',$checkEmail->id);
             
                    }
      
                    return $this->send_success_message('Login Successfull');
    
                }
                return $this->send_error_message('Incorrect Password');
            }
            return $this->send_error_message('Email is not in our records');
           
        }

        public function AdmindashboardView()
        {
            if(Session::has('Adminemail')){
                $getLoginAdminData = AdminUserModel::where('email',Session::get('Adminemail'))->orwhere('email',Cookie::get('Adminemail'))->first();   
            }

            $registerData = RegisterModel::count();
            $user_registerData = RegisterModel::where('UserType','user')->count();
            $company_registerData = RegisterModel::where('UserType','company')->count();
            $job_postData = JobPostModel::count();
            $applied_userData = AppliedUser::count();
            $Payment_Data = DetailsPayment::count();


            // $count = JobPostModel::where('company_id', Session::get('userid'))->count();


            $alldata_arr=array(
                'data'=>$getLoginAdminData,
                'register_data' =>  $registerData,
                'User_register_data' =>  $user_registerData,
                'Company_register_data' =>  $company_registerData,
                'job_post_data' =>  $job_postData,
                'applied_user_data' =>  $applied_userData,
                'paymentData' =>  $Payment_Data,


            );
            return view('Admin.Admin-dashboard', $alldata_arr);

            // return view('Admin.Admin-dashboard')->with('data',$getLoginAdminData);
        }
    
        public function Adminlogout()
        {
            // session--------------------
            Session::flush();
            
             // cookie --------------------
            Cookie::queue(Cookie::forget('Adminemail'));
            Cookie::queue(Cookie::forget('Adminid'));
            Cookie::queue(Cookie::forget('Admintype'));
          
            return redirect('Admin-Login');
        }

        
        public function AppliedUserData()
        {
            $getData = AppliedUser::all();

            $all_data = array(
                'data' => $getData,
                
            );  

            return view('Admin.applied-user-data' ,$all_data); 
            
        
        }

        public function editAppliedUserData($id) {
            $getParticularData = AppliedUser::where('id',$id)->first();
            if ($getParticularData) {
                return view('Admin.edit_applied_user_data')->with('editDetails',$getParticularData);
            }
            session()->flash('error', 'Details are not in database');
            return redirect('edit_applied_user_data');
        }
    
        public function updateFormAppliedUserData(Request $request){
           
            // echo "<pre>";
            // print_r($request->all());
            // echo "</pre>";
            // die();
            $getParticularData = AppliedUser::where('id',$request->id_)->first();
            $getParticularData->company_email = $request->company_email;
            $getParticularData->job_id = $request->job_id;
            $getParticularData->job_title = $request->job_title;
            $getParticularData->applied_useremail = $request->applied_useremail;
            $getParticularData->applied_firstname = $request->applied_firstname;
            $getParticularData->applied_lastname = $request->applied_lastname;
            $getParticularData->applied_phone = $request->applied_phone;
            $getParticularData->save();
            session()->flash('message2', 'Data Update Successfully');
            return redirect('applied-user-data');
        }
    

        public function deleteDetailsAppliedUserData($id)
        {
            
    
            $getParticularData = AppliedUser::where('id',$id)->first();
            if ($getParticularData->delete()) {
                // $getParticularData->delete();
                $success = true;
                $message = "User deleted successfully";
             
                
            } else {
                $success = true;
                $message = "User not found";
                return redirect('jobpostData');
            }
    
            return response()->json([
                'success' => $success,
                'message' => $message,
            ]);
        }
    
    
       
        // **************************************************  ADMIN-CODE-END  ********************************************************************
        // ****************************************************************************************************************************************

      

        // ****************************Sweet-Alert-Code***********************************

        public function send_error_message($message)
        {
            $resp = array();
            $resp['status']=false;
            $resp['message']=$message;
            return $resp;   
        }
        public function send_success_message($message,$data=null)
        {
            $resp = array();
            $resp['status']=true;
            $resp['message']=$message;
            if($data != null){
                $resp['data']=$data;
            }
            return $resp;   
        }

        // ****************************Sweet-Alert-Code-End***********************************

}

  


