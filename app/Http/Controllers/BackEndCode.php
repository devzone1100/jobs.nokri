<?php

namespace App\Http\Controllers;
use App\Models\Payment;
use Illuminate\Http\Request;

class BackEndCode extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    // payment***************

    public function paymentsave(Request $request)
    {
        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        
        $paymentdata = new Payment();
        $paymentdata->email = $request->email;
        $paymentdata->Transation_ID = $request->transation;
        $paymentdata->User_ID = $request->user_id;
        $paymentdata->Prize = $request->prize;
        $paymentdata->Job_ID = $request->job_id;
        $paymentdata->save();

        $notification = array(
            'message' => ' Payment Done !',
            'alert-type' => 'success'
        );
        return redirect('payment')->with($notification);
        
        // session()->flash('message2','Payment Done');
        // return redirect('payment');
        

    }
    public function editDetails($id) {
        $getParticularData = Payment::where('id',$id)->first();
        if ($getParticularData) {
            return view('Admin.edit_page')->with('editDetails',$getParticularData);
        }
        
        session()->flash('error', 'Details are not in database');
        return redirect('paymentData');
    }
    public function updateForm(Request $request){
       
        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        // die();
        $getParticularData = Payment::where('id',$request->id_)->first();
        $getParticularData->email = $request->email;
        $getParticularData->Transation_ID = $request->transation;
        $getParticularData->User_ID = $request->user_id;
        $getParticularData->Prize = $request->prize;
        $getParticularData->Job_ID = $request->job_id;
        $getParticularData->save();

        // $notification = array(
        //     'message' => ' Data Update Successfully !',
        //     'alert-type' => 'success'
        // );
        // return redirect('paymentData')->with($notification);

        session()->flash('message2', 'Data Update Successfully');
        return redirect('paymentData');
    }

    public function deleteDetails($id)
    {
        // $getParticularData = Payment::where('id',$id)->first();
        // if($getParticularData){
        //     $getParticularData->delete();
        //     session()->flash('message2', 'Data delete Successfully');
        // return redirect('paymentData');
        // }

        $getParticularData = Payment::where('id',$id)->first();
        if ($getParticularData->delete()) {
            // $getParticularData->delete();
            $success = true;
            $message = "User deleted successfully";
         
            
        } else {
            $success = true;
            $message = "User not found";
            return redirect('paymentData');
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
