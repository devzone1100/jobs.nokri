<?php

namespace App\Http\Controllers;

use App\Models\JobPostModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\RegisterModel;
use App\Models\AppliedUser;
use App\Models\AddplansModel;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Session;
use Cookie;




use File;


class PostJobBackEnd extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // http://ip-api.com/json/169.149.224.130

    public function jobpostsave(Request $request)
    {

      
   

        $jobpost = new JobPostModel();
        $jobpost->job_id = $request->job_id;
        $jobpost->company_email = $request->company_email;
        $jobpost->company_id = $request->company_id;
        $jobpost->Job_Category = $request->job_category;
        $jobpost->Job_Title = $request->job_title;
        $jobpost->Job_Type = $request->job_type;
        $jobpost->Working_Hours = $request->working_hours;
        $jobpost->Salary = $request->salary;
        $jobpost->Experience = $request->experience;
        $jobpost->about_job = $request->about_job;
        $jobpost->trending_key = $request->trending_key;
        $jobpost->country = $request->country;
        $jobpost->city = $request->city;
        $jobpost->full_address = $request->full_address;
        $jobpost->latitude = $request->latitude;
        $jobpost->longitude = $request->longitude;
        $jobpost->timezone = $request->timezone;
        

        // image-upload---------------------------------

          if($request->hasFile('company_logo')){
            
            $image = $request->company_logo;
            $extension = $image->getClientOriginalExtension();
            $newName = rand().time().'.'.$extension;
            $destination = public_path('company_logos'); // folder path and name
            $image->move($destination,$newName); // upload
            unset($jobpost['company_logo']); // delete the request key
            $jobpost['company_logo'] = $newName; // insert image name into request

        }

        // image-upload-End--------------------------------


        // pdf file upload---------------------------

        if($request->file('job_letter')) 
        {
            $file = $request->file('job_letter');
            $filename = time() . '.' . $request->file('job_letter')->extension();
            $filePath = public_path('job_letters');
            $file->move($filePath, $filename);
            $jobpost['job_letter'] = $filename; // insert 

        }

        // pdf file upload-End-----------------------

        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        // die();

     
     
        $jobpost->save();

        $notification = array(
            'message' => 'Your Job Post Successfully !!',
            'alert-type' => 'success'
        );
        
        return redirect('job-post')->with($notification);

    }
    
    public function jobpostFormShow ()
    {
        $getData = JobPostModel::all();
        return view('Admin.job_post_data')->with('data',$getData); 
        // return view('post');
    }

  

    public function manageJobs (Request $request)
    {
        $getData = JobPostModel::all();

        $value = $request->session()->get('useremail');

        $users = DB::table('job_post_models')
            ->where('company_email', $value)
            ->get();

        $all_data = array(

            'data' => $users,
            'profile_data' =>  Session::get('profile_image'),

        );
            
        return view('company.manage-job', $all_data); 
        // return view('post');
    }

    public function editDetailsManageJobs($id) {
        $getParticularData = JobPostModel::where('id',$id)->first();
        if ($getParticularData) {
            return view('company.edit_manage_jobs')->with('editDetails',$getParticularData);
        }
        session()->flash('error', 'Details are not in database');
        return redirect('manage-job');
    }

    public function updateFormManageJobs(Request $request){
       
        $getParticularData = JobPostModel::where('id',$request->id_)->first();
        $getParticularData->Job_Category = $request->job_category;
        $getParticularData->Job_Title = $request->job_title;
        $getParticularData->Job_Type = $request->job_type;
        $getParticularData->Working_Hours = $request->working_hours;
        $getParticularData->Salary = $request->salary;
        $getParticularData->Experience = $request->experience;
        $getParticularData->Job_Letter = $request->job_letter;
        $getParticularData->save();
        session()->flash('message2', 'Data Update Successfully');
        return redirect('manage-job');
    }

    public function deleteMangeJobs($id)
    {
        

        $getParticularData = JobPostModel::where('id',$id)->first();
        if ($getParticularData->delete()) {
            // $getParticularData->delete();
            $success = true;
            $message = "User deleted successfully";
         
            
        } else {
            $success = true;
            $message = "User not found";
            return redirect('manage-job');
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


    public function companypageview ()
    {
       
        $alldata_arr=array(
            'profile_data' =>  Session::get('profile_image'),
        );
        return view('company.company-page', $alldata_arr);

    }

  

    public function jobpostlistShow ()
    {
        $getData = JobPostModel::all();

        $registerData = RegisterModel::all();
        

        $alldata_arr=array(
            'data'=>$getData,
            'register_data' =>  $registerData,
            'profile_data' =>  Session::get('profile_image'),
        );
        // print_r(); die;
        return view('user.job-listing-grid', $alldata_arr);

        // return view('user.job-listing-grid')->with('data',$getData); 

    }

    public function LetestjobpostlistShow ()
    {
        // $getData = JobPostModel::all();
        $getData = JobPostModel::latest()->take(5)->get();

        $registerData = RegisterModel::all();
        // $plangetData = AddplansModel::all();
        $plangetData = AddplansModel::latest()->take(3)->get();



        $alldata_arr=array(
            'data'=>$getData,
            'plandata'=>$plangetData,
            'register_data' =>  $registerData,
        );
        return view('static.index', $alldata_arr);

      

    }

    public function editDetailsjobpost($id) {
        $getParticularData = JobPostModel::where('id',$id)->first();
        if ($getParticularData) {
            return view('Admin.edit_page_jobpost')->with('editDetails',$getParticularData);
        }
        session()->flash('error', 'Details are not in database');
        return redirect('jobpostData');
    }

    public function updateFormjobpost(Request $request){
       
        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        // die();
        $getParticularData = JobPostModel::where('id',$request->id_)->first();
        $getParticularData->Job_Category = $request->job_category;
        $getParticularData->Job_Title = $request->job_title;
        $getParticularData->Job_Type = $request->job_type;
        $getParticularData->Working_Hours = $request->working_hours;
        $getParticularData->Salary = $request->salary;
        $getParticularData->Experience = $request->experience;
        $getParticularData->Job_Letter = $request->job_letter;
        $getParticularData->save();
        session()->flash('message2', 'Data Update Successfully');
        return redirect('jobpostData');
    }

    public function deleteDetailsjobpost($id)
    {
        

        $getParticularData = JobPostModel::where('id',$id)->first();
        if ($getParticularData->delete()) {
            // $getParticularData->delete();
            $success = true;
            $message = "User deleted successfully";
         
            
        } else {
            $success = true;
            $message = "User not found";
            return redirect('jobpostData');
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

// job-post-details--------------------------------------------

    public function detailsjobpost($id, Request $request) {

        $getParticularData = JobPostModel::where('id',$id)->first();
        $value = $request->session()->get('useremail');

        $users = DB::table('register_models')
            ->where('email', $value)
            ->get();
            
            if(Session::get('UserType') !== 'user')
            {
                $notification = array(
                    'message' => ' Need Login First Then you can Apply !',
                    'alert-type' => 'error'
                );
                
                $redirect_url = $request->url();
                return redirect('login?redirectUrl='.$redirect_url)->with($notification);
                // return redirect('login')->with($notification);
            }    


        // echo $value; die;
        if ($getParticularData) {

            $registerData = RegisterModel::all();

            $alldata_arr=array(
                'data'=>$getParticularData,
                'register_data' =>  $users[0],
                'profile_data' =>  Session::get('profile_image'),
               
            );
             return view('user.details_job_post', $alldata_arr);

            // return view('user.details_job_post')->with('data',$getParticularData);
        }    
    }

// job-post-details--------------------------------------------


// applied-user-data-------------------------------------------
    public function applieduser(Request $request)
    {

  
        $applieduser = new AppliedUser();
        $applieduser->job_id = $request->job_id;
        $applieduser->company_id = $request->company_id;
        $applieduser->company_email = $request->company_email;
        $applieduser->job_title = $request->job_title;
        $applieduser->applied_useremail = $request->applied_useremail;
        $applieduser->applied_userid = $request->applied_userid;
        $applieduser->applied_firstname = $request->applied_firstname;
        $applieduser->applied_lastname = $request->applied_lastname;
        $applieduser->applied_phone = $request->applied_phone;

        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        // die();

        $users = DB::table('applied_users')
            ->where('job_id', $request->job_id)
            ->where('applied_userid', $request->applied_userid)
            ->get();
            // print_r($users[0]); die;
            // // echo ($users[0]->company_id !== NULL);
            // die;
            // echo isset($users[0]); die;

            if( isset($users[0]) && $users[0] !== NULL )
            {
                
                echo "alert('You are already applied for this job');";

                $notification = array(
                    'message' => 'You are already Applied for this job !!',
                    'alert-type' => 'error'
                );
                return redirect('job-listing-grid')->with($notification);
                
            }
            else{
                
                $mail_array = array('company_email'=>$request->company_email);
                Mail::send('email.applied_user',$mail_array,function($m) use ($mail_array){
                    $m->to($mail_array['company_email'])->subject('Job Application Request');
                    $m->from('xyz@gmail.com');
        
                });

                $applieduser->save();
        
                $notification = array(
                    'message' => 'You are succesfully Applied for this job !!',
                    'alert-type' => 'success'
                );
                return redirect('job-listing-grid')->with($notification);
            }
   
       
    } 
    
// applied-user-data-end-------------------------------------------


// manage-allpied-user-data-------------------------------------------


    public function manageapplieduser (Request $request)
    {   
        if(Session::get('UserType') == 'user')
        {
            $notification = array(
                'message' => ' Need Login First !',
                'alert-type' => 'error'
            );
            
            $redirect_url = $request->url();
            return redirect('login?redirectUrl='.$redirect_url)->with($notification);
            // return redirect('login')->with($notification);
        } 

        $getData = AppliedUser::all();
        $value = $request->session()->get('useremail');

        $users = DB::table('applied_users')
            ->where('company_email', $value)
            ->get();

        $all_data = array(
            'data' => $users,
            'profile_data' =>  Session::get('profile_image'),
        );    
            
        // return view('company.manage-application')->with('data',$users); 
        return view('company.manage-application' ,$all_data); 
        // return view('post');
    }

    public function editmanageapplication($id) {
        $getParticularData = AppliedUser::where('id',$id)->first();
        if ($getParticularData) {
            return view('company.edit_manage_application')->with('editDetails',$getParticularData);
        }
        session()->flash('error', 'Details are not in database');
        return redirect('manage-application');
    }

    public function updateFormAppliedUser(Request $request){
       
        // echo "<pre>";
        // print_r($request->all());
        // echo "</pre>";
        // die();
        $getParticularData = AppliedUser::where('id',$request->id_)->first();
        $getParticularData->company_email = $request->company_email;
        $getParticularData->job_id = $request->job_id;
        $getParticularData->job_title = $request->job_title;
        $getParticularData->applied_useremail = $request->applied_useremail;
        $getParticularData->applied_firstname = $request->applied_firstname;
        $getParticularData->applied_lastname = $request->applied_lastname;
        $getParticularData->applied_phone = $request->applied_phone;
        $getParticularData->save();
        session()->flash('message2', 'Data Update Successfully');
        return redirect('manage-application');
    }





    public function deleteDetailsAppliedUser($id)
    {
        

        $getParticularData = AppliedUser::where('id',$id)->first();
        if ($getParticularData->delete()) {
            // $getParticularData->delete();
            $success = true;
            $message = "User deleted successfully";
         
            
        } else {
            $success = true;
            $message = "User not found";
            return redirect('jobpostData');
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }



// manage-allpied-user-data-end-------------------------------------------


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
