<?php

namespace App\Http\Controllers;

use App\Models\RegisterModel;
use Illuminate\Http\Request;

class MyController extends Controller
{
    public function registerFormShow()
    {
        $getData = RegisterModel::all();
        return view('register_data')->with('data',$getData); 
      
        
    }
}
