<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPostModel extends Model
{
    use HasFactory;

    public $fillable = ['job_id','company_email','company_id','Job_Category','Job_Title','Job_Type','Working_Hours','Salary','Experience','company_logo','job_letter','about_job','trending_key','country','city','full_address','latitude','longitude','timezone'];
}
