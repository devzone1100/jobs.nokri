<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddplansModel extends Model
{
    use HasFactory;
    public $fillable = ['plan_type','number_post','duration_Time','plan_prize','featured'];

}
