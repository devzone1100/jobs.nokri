<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppliedUser extends Model
{
    use HasFactory;
    public $fillable = ['job_id','company_id','company_email','job_title','applied_useremail','applied_userid','applied_firstname','applied_lastname','applied_phone'];

}
