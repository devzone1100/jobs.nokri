<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailsPayment extends Model
{
    use HasFactory;
    public $fillable = ['USERID','USEREMAIL','ORDERID','MID','TXNID','TXNAMOUNT','PAYMENTMODE','CURRENCY','TXNDATE','STATUS','RESPCODE','RESPMSG','GATEWAYNAME','BANKTXNID','BANKNAME','CHECKSUMHASH'];

}
