<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegisterModel extends Model
{
    use HasFactory;
    
    public $fillable = ['Firstname','Lastname','email','phone_no','password','gender','UserType','profile_image'];
}
