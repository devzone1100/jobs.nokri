<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    // public $fillable = ['email','Transation_ID','User_ID','Prize','Job_ID'];
    public $fillable = ['company_email','job_id','amount','purchase_date','payment_order_id'];

}
