loginCheck = ()=>{
    let email = $('#email').val()
    let password = $('#password').val()
    let redirectUrl = $('#redirectUrl').val()
    let remember_me = 0
    let token = $('input[name="_token"]').val()
    if(email == '' || password == ''){
        // $('#loginMsg').html('All fields are required')
        // $('#loginMsg').css('color','red')
        Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red"> All fields are required <span>',
          })
         
        return false;
    }
    if ($('#remember_me').is(':checked')){
        remember_me = 1
        
        
    }
    let url = "loginCheck";
    let formData = new FormData()
    formData.append('_token',token);
    formData.append('email',email)
    formData.append('password',password)
    formData.append('redirectUrl',redirectUrl)
    formData.append('remember_me',remember_me)
    let xhr = new XMLHttpRequest
    xhr.open('POST',url)
    xhr.send(formData)
    xhr.onload = function(){
        let Obj = JSON.parse(xhr.responseText);
        var st = Obj.status;
        var con = Obj.data;
        var me = Obj.message;
        let url = Obj.redirectUrl;

        // console.log(url,con);
        // return;
        if(!st){
        //     $('#loginMsg').html(me)
        //    $('#loginMsg').css('color','red')
        Swal.fire({
            position: 'top-center', 
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
            return false;
        }
        document.getElementsByClassName("loader")[0].style.display = "block";
        // $('#loginMsg').html(me)
        // $('#loginMsg').css('color','green')
        // Swal.fire({
        //     position: 'top-center',
        //     icon: 'success',
        //     title: me,
        //     showConfirmButton: false,
        //     timer: 2000
        //   })
        if(con == 'user')
        {
            setTimeout(() => {
                // console.log(url);
                // console.log(url.length);
                // return;
                if( url.length > 0 ){

                    window.location.href = url;
                }else{
                    
                    // window.location.href = url;
                    window.location.href = 'user-dashboard'
                }
                
            }, 2000); 
        }
        else{

            setTimeout(() => {
                // console.log(url);
                // console.log(url.length);
                // return;
                if( url.length > 0 ){

                    window.location.href = url;
                }else{
                    
                    // window.location.href = url;
                    window.location.href = 'company-dashboard'
                }
                
            }, 2000); 

        }
       
    }
}