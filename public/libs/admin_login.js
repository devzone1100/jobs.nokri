AdminloginCheck = ()=>{
    let email = $('#email').val()
    let password = $('#password').val()
    let remember_me = 0
    let token = $('input[name="_token"]').val()
    if(email == '' || password == ''){
   
        Swal.fire({
            position: 'top-center',
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red"> All fields are required <span>',
          })
         
        return false;
    }
    if ($('#remember_me').is(':checked')){
        remember_me = 1
        
    }
    let url = "AdminloginCheck";
    let formData = new FormData()
    formData.append('_token',token);
    formData.append('email',email)
    formData.append('password',password)
    formData.append('remember_me',remember_me)
    let xhr = new XMLHttpRequest
    xhr.open('POST',url)
    xhr.send(formData)
    xhr.onload = function(){
        let Obj = JSON.parse(xhr.responseText);
        var st = Obj.status;
        var me = Obj.message;
        if(!st){
     
        Swal.fire({
            position: 'top-center', 
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<span style="color:red">'+me+'<span>',
          })
            return false;
        }
        document.getElementsByClassName("loader")[0].style.display = "block";
    
        // Swal.fire({
        //     position: 'top-center',
        //     icon: 'success',
        //     title: me,
        //     showConfirmButton: false,
        //     timer: 2000
        //   })
      
        setTimeout(() => {
            window.location.href = 'Admin-dashboard'
        }, 2000); 
  
       
    }
}